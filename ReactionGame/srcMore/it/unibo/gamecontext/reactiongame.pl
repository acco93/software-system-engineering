%====================================================================================
% Context gamecontext  SYSTEM-configuration: file it.unibo.gamecontext.reactiongame.pl 
%====================================================================================
context(gamecontext, "localhost",  "TCP", "7778" ).  		 
%%% -------------------------------------------
qactor( userinterface , gamecontext, "it.unibo.userinterface.MsgHandle_Userinterface"   ). %%store msgs 
qactor( userinterface_ctrl , gamecontext, "it.unibo.userinterface.Userinterface"   ). %%control-driven 
qactor( gamelogic , gamecontext, "it.unibo.gamelogic.MsgHandle_Gamelogic"   ). %%store msgs 
qactor( gamelogic_ctrl , gamecontext, "it.unibo.gamelogic.Gamelogic"   ). %%control-driven 
qactor( stopper , gamecontext, "it.unibo.stopper.MsgHandle_Stopper"   ). %%store msgs 
qactor( stopper_ctrl , gamecontext, "it.unibo.stopper.Stopper"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

