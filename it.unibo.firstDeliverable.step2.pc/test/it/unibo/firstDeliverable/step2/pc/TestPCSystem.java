package it.unibo.firstDeliverable.step2.pc;


import static org.junit.Assert.*;

import org.junit.Test;

import it.unibo.bls.highLevel.interfaces.IBLSControl;
import it.unibo.firstDeliverable.step2.pc.BCLS;
import it.unibo.firstDeliverable.step2.pc.Knowledge;


public class TestPCSystem {

	@Test
	public void testSystemNoInterruption() throws Exception{
		BCLS sys = BCLS.createTheSystem();
		IBLSControl controller = sys.getController();
		controller.update(null, Knowledge.blink);
		delay(Knowledge.MAXTIME);
		delay(1000);
		controller.update(null, Knowledge.blink);
		delay(500);
		controller.update(null, Knowledge.stop);
		delay(500);
		assertTrue("off", ! sys.getLed().isOn() );
	}
	
	@Test
	public void testSystemInterruption() throws Exception{
		BCLS sys = BCLS.createTheSystem();
		IBLSControl controller = sys.getController();
		controller.update(null, Knowledge.blink);
		controller.update(null, Knowledge.blink);
		delay(1000);
		controller.update(null, Knowledge.stop);
		delay(100);
		controller.update(null, Knowledge.stop);
		assertTrue("off", ! sys.getLed().isOn() );
	}
	

	
	protected void delay( int time ){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
 			e.printStackTrace();
		} 		
 	}
}
