package it.unibo.firstDeliverable.step2.pc;

import java.util.concurrent.Callable;

import it.unibo.action.AsynchTimedAction;
import it.unibo.bls.highLevel.interfaces.IDevLed;
import it.unibo.is.interfaces.IOutputEnvView;

/*
 * NEW CONCEPT: 
 */
public class BlinkTimed extends AsynchTimedAction  {
private IDevLed led;

	public BlinkTimed(String name,IOutputEnvView outEnvView,int maxduration,IDevLed led) {
		super(name, outEnvView, maxduration);
		this.led = led;
	}
 
  	@Override
	protected String getApplicationResult() { 		
 		return "actionDone(NAME,T)".replace("NAME", name).replace("T", ""+durationMillis);
	}
	@Override
	protected Callable<String> getActionBodyAsCallable() {
 		return new Callable<String>(){
			@Override
			public String call() throws Exception {
				for(int i=1; i<=1000000;i++){
					led.turnOn();
					Thread.sleep(500);
	 				led.turnOff();
	 				Thread.sleep(500);
				}
				return "done";
			}		
		};
	}
	@Override
	public void suspendAction(){
		led.turnOff();
		super.suspendAction();
   	}		
}

