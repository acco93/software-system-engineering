package it.unibo.firstDeliverable.step2.pc;


import java.util.Observable;
import java.util.concurrent.Future;

import it.unibo.bls.highLevel.interfaces.IBLSControl;
import it.unibo.bls.highLevel.interfaces.IDevLed;
import it.unibo.is.interfaces.IOutputEnvView;


public class ApplicationLogic implements IBLSControl {

	private IDevLed led;
	private IOutputEnvView out;
	
  	protected BlinkTimed blinkAction = null;
 	protected Future<String> future;
	
	public ApplicationLogic(IOutputEnvView outEnvView) {
		this.out = outEnvView;
	}
	
	@Override
	public void setLed(IDevLed led) {
		this.led = led;

	}

	@Override
	public void update(Observable source, Object input) {
		if( input.equals(Knowledge.blink)){
			if( future != null )  handleCmdWhileAction(input);
				if(  blinkAction == null ){
		 			out.addOutput("BlsBlinkControl input= " + input + " tout=" + Knowledge.MAXTIME );
					blinkAction = new BlinkTimed("blink", out, Knowledge.MAXTIME, led);
					future = blinkAction.execASynch();
				}
			}else if( input.equals(Knowledge.stop)){
				if(  blinkAction != null ){
					blinkAction.suspendAction();
					out.addOutput("" + blinkAction.getResultRep());
	   				blinkAction = null;
	   				future      = null;
			}
			}
	 	}		

 protected void handleCmdWhileAction(Object input){
	if( ! future.isDone() ){
		//A Blink action is working
		out.addOutput("BlsBlinkControl input=" + input + " while action working "  );
	}
	else{
		//A Blink action was working and it is now terminated. 
		out.addOutput("BlsBlinkControl input=" + input + " previous action: " + blinkAction.getResultRep() );
		blinkAction = null ;
		future      = null;
	}   	 
 }
	

}
