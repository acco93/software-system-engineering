%====================================================================================
% Context qauiContext  SYSTEM-configuration: file it.unibo.qauiContext.qaui.pl 
%====================================================================================
context(qauicontext, "localhost",  "TCP", "8889" ).  		 
%%% -------------------------------------------
qactor( userinterface , qauicontext, "it.unibo.userinterface.MsgHandle_Userinterface"   ). %%store msgs 
qactor( userinterface_ctrl , qauicontext, "it.unibo.userinterface.Userinterface"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

