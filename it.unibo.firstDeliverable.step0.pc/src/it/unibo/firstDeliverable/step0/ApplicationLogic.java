package it.unibo.firstDeliverable.step0;

import java.util.Observable;

import it.unibo.bls.highLevel.interfaces.IBLSControl;
import it.unibo.bls.highLevel.interfaces.IDevLed;
import it.unibo.is.interfaces.IOutputEnvView;


public class ApplicationLogic implements IBLSControl {

	private IDevLed led;
	private IOutputEnvView out;

	public ApplicationLogic(IOutputEnvView outEnvView) {
		this.out = outEnvView;
	}
	
	@Override
	public void setLed(IDevLed led) {
		this.led = led;

	}

	@Override
	public void update(Observable o, Object arg) {
		if(this.led.isOn()){
			out.addOutput("Turning off the led");
			this.led.turnOff();
		} else {
			out.addOutput("Turning on the led");
			this.led.turnOn();
		}

	}

	

}
