package it.unibo.firstDeliverable.step0.pc;

import java.util.Observable;

import it.unibo.is.interfaces.IObserver;

public class ButtonObserverTest implements IObserver {

	private String value;

	@Override
	public void update(Observable o, Object state) {
		 System.out.println("ButtonObserverTest update " + state);		
		 value = (String) state;
	}

	public String getValue() {
		return value;
	}
	
	
}
