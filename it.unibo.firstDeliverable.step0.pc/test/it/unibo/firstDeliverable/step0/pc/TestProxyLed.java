package it.unibo.firstDeliverable.step0.pc;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;

import org.junit.Test;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.bls.highLevel.interfaces.IDevLed.LedColor;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.buttonLedSystem.proxy.DeviceLedPassiveProxy;
import it.unibo.firstDeliverable.step0.BCLS;
import it.unibo.is.interfaces.IBasicEnvAwt;

public class TestProxyLed {

	@Test
	public void testLed(){
		try {
		IBasicEnvAwt env = new EnvFrame("Test the proxy led", Color.white, Color.BLACK);
		env.init();
 		ILed led = new DeviceLedPassiveProxy("ledGreen", LedColor.GREEN, BCLS.hostAddress, BCLS.portNumber, env.getOutputEnvView());
 		assertTrue("testLed", ! led.isOn());
 		led.doSwitch();
   		assertTrue("testLed",led.isOn());
		} catch (Exception e) {
			fail("");
  		}
	}	
}
