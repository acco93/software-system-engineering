package it.unibo.firstDeliverable.step0.pc;

import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.Test;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.bls.lowLevel.interfaces.IButton;
import it.unibo.bls.lowLevel.interfaces.IDeviceButtonImpl;
import it.unibo.buttonLedSystem.gui.BLSGuiFactory;
import it.unibo.is.interfaces.IBasicEnvAwt;

public class TestButton {

	@Test
	public void testButton(){
		IBasicEnvAwt env = new EnvFrame("DevButtonGui", Color.white, Color.BLACK);
		env.init();
 		IButton btn = BLSGuiFactory.createGuiButton("button", env.getOutputEnvView() ,   new String[] { "Switch led" }  );
 		ButtonObserverTest obs = new ButtonObserverTest();
 		btn.addObserver(obs);
 		btn.high();
 		// the button was low => there is a button state change
  		assertTrue("",obs.getValue().equals(IDeviceButtonImpl.repSwitch));
 		btn.high();
 		// the button was already high => no state change!
  		assertTrue("",obs.getValue().equals(IDeviceButtonImpl.repHigh));
 		btn.low();
  		assertTrue("",obs.getValue().equals(IDeviceButtonImpl.repSwitch));
 		btn.low();
  		assertTrue("",obs.getValue().equals(IDeviceButtonImpl.repLow));

	}
}
