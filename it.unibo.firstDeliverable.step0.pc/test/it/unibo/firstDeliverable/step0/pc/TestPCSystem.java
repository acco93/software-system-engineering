package it.unibo.firstDeliverable.step0.pc;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.bls.lowLevel.interfaces.IButton;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.firstDeliverable.step0.BCLS;
import it.unibo.is.interfaces.IBasicEnvAwt;

public class TestPCSystem {

	@Test
	public void testSystem(){
		try {
			IBasicEnvAwt env = new EnvFrame("BCLS");
			env.init();
			BCLS sys = BCLS.createTheSystem();
			IButton concreteButton = sys.getConcreteButton();
			ILed    concreteLed    = sys.getConcreteLed();
			
			concreteButton.high();
			assertTrue("testSystem",concreteLed.isOn());
			concreteButton.high();
			assertTrue("testSystem", ! concreteLed.isOn());
			concreteButton.low();
			assertTrue("testSystem",concreteLed.isOn());
			concreteButton.high();
			assertTrue("testSystem", ! concreteLed.isOn());
		} catch (Exception e) {
			fail("");
  		}
	}
}
