%====================================================================================
% Context gamecontext  SYSTEM-configuration: file it.unibo.gamecontext.reactiongame.pl 
%====================================================================================
context(gamecontext, "localhost",  "TCP", "7778" ).  		 
%%% -------------------------------------------
qactor( userinterface , gamecontext, "it.unibo.userinterface.MsgHandle_Userinterface"   ). %%store msgs 
qactor( userinterface_ctrl , gamecontext, "it.unibo.userinterface.Userinterface"   ). %%control-driven 
qactor( ledflash , gamecontext, "it.unibo.ledflash.MsgHandle_Ledflash"   ). %%store msgs 
qactor( ledflash_ctrl , gamecontext, "it.unibo.ledflash.Ledflash"   ). %%control-driven 
qactor( led1 , gamecontext, "it.unibo.led1.MsgHandle_Led1"   ). %%store msgs 
qactor( led1_ctrl , gamecontext, "it.unibo.led1.Led1"   ). %%control-driven 
qactor( led2 , gamecontext, "it.unibo.led2.MsgHandle_Led2"   ). %%store msgs 
qactor( led2_ctrl , gamecontext, "it.unibo.led2.Led2"   ). %%control-driven 
qactor( led3 , gamecontext, "it.unibo.led3.MsgHandle_Led3"   ). %%store msgs 
qactor( led3_ctrl , gamecontext, "it.unibo.led3.Led3"   ). %%control-driven 
qactor( randomizer , gamecontext, "it.unibo.randomizer.MsgHandle_Randomizer"   ). %%store msgs 
qactor( randomizer_ctrl , gamecontext, "it.unibo.randomizer.Randomizer"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(eventhandler,gamecontext,"it.unibo.gamecontext.Eventhandler","start").  
eventhandler(ledonhandler,gamecontext,"it.unibo.gamecontext.Ledonhandler","led").  
%%% -------------------------------------------

