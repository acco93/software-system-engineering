\select@language {english}
\contentsline {title}{An introduction to the usage of QActors}{1}{chapter.1}
\authcount {1}
\contentsline {author}{Antonio Natali}{1}{chapter.1}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1.1}
\contentsline {subsection}{\numberline {1.1}QActor API }{4}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.2}Actions }{4}{subsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Logical actions.}{4}{subsubsection.1.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}The actor's \texttt {WorldTheory}.}{4}{subsubsection.1.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Physical actions.}{5}{subsubsection.1.1.2.3}
\contentsline {subsubsection}{\numberline {1.2.4}Application actions. }{5}{subsubsection.1.1.2.4}
\contentsline {subsubsection}{\numberline {1.2.5}PlanActions.}{5}{subsubsection.1.1.2.5}
\contentsline {subsection}{\numberline {1.3}Plans}{6}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.4}Messages and (reactive) message}{6}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.5}Events and event-driven/event-based behaviour }{6}{subsection.1.1.5}
\contentsline {subsection}{\numberline {1.6}Actor programs as plans}{7}{subsection.1.1.6}
\contentsline {subsubsection}{\numberline {1.6.1}Actor programs as finite state machines (FSM)}{7}{subsubsection.1.1.6.1}
\contentsline {section}{\numberline {2}The \texttt {qa} language/metamodel.}{9}{section.1.2}
\contentsline {subsection}{\numberline {2.1}Example}{9}{subsection.1.2.1}
\contentsline {subsection}{\numberline {2.2}Workflow}{9}{subsection.1.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Application designer and System designer.}{10}{subsubsection.1.2.2.1}
\contentsline {subsection}{\numberline {2.3}QActor knowledge}{10}{subsection.1.2.3}
\contentsline {subsection}{\numberline {2.4}QActor software factory}{10}{subsection.1.2.4}
\contentsline {subsection}{\numberline {2.5}Contexts }{11}{subsection.1.2.5}
\contentsline {subsection}{\numberline {2.6}Messages}{11}{subsection.1.2.6}
\contentsline {subsubsection}{\numberline {2.6.1}PHead. }{11}{subsubsection.1.2.6.1}
\contentsline {subsection}{\numberline {2.7}Send actions}{12}{subsection.1.2.7}
\contentsline {subsubsection}{\numberline {2.7.1}Operation that sends a \textit {dispatch}}{12}{subsubsection.1.2.7.1}
\contentsline {subsubsection}{\numberline {2.7.2}forward implementation (see Subsection~\ref {ssec:sendImpl})}{12}{subsubsection.1.2.7.2}
\contentsline {subsubsection}{\numberline {2.7.3}Operation that sends a \textit {request}}{12}{subsubsection.1.2.7.3}
\contentsline {subsubsection}{\numberline {2.7.4}demand implementation (see Subsection~\ref {ssec:sendImpl})}{12}{subsubsection.1.2.7.4}
\contentsline {subsection}{\numberline {2.8}Send action implementation}{12}{subsection.1.2.8}
\contentsline {subsection}{\numberline {2.9}Receive actions}{12}{subsection.1.2.9}
\contentsline {subsubsection}{\numberline {2.9.1}Generic receive with optional message specification}{13}{subsubsection.1.2.9.1}
\contentsline {subsubsection}{\numberline {2.9.2}Receive a message with a specified structure}{13}{subsubsection.1.2.9.2}
\contentsline {subsubsection}{\numberline {2.9.3}Select a message and execute}{13}{subsubsection.1.2.9.3}
\contentsline {subsection}{\numberline {2.10}Receive implementation}{13}{subsection.1.2.10}
\contentsline {subsection}{\numberline {2.11}Events and event-based behaviour }{14}{subsection.1.2.11}
\contentsline {subsubsection}{\numberline {2.11.1}Emit action}{14}{subsubsection.1.2.11.1}
\contentsline {subsubsection}{\numberline {2.11.2}emit implementation}{14}{subsubsection.1.2.11.2}
\contentsline {subsubsection}{\numberline {2.11.3}Sense action. }{14}{subsubsection.1.2.11.3}
\contentsline {subsubsection}{\numberline {2.11.4}sense implementation}{14}{subsubsection.1.2.11.4}
\contentsline {subsubsection}{\numberline {2.11.5}OnEvent receive actions. }{14}{subsubsection.1.2.11.5}
\contentsline {subsection}{\numberline {2.12}Event handlers and event-driven behaviour }{15}{subsection.1.2.12}
\contentsline {section}{\numberline {3}Human interaction with a Qactor}{16}{section.1.3}
\contentsline {section}{\numberline {4}About actions}{17}{section.1.4}
\contentsline {subsection}{\numberline {4.1}Basic actions}{17}{subsection.1.4.1}
\contentsline {subsection}{\numberline {4.2}Timed actions}{17}{subsection.1.4.2}
\contentsline {subsection}{\numberline {4.3}Time out}{17}{subsection.1.4.3}
\contentsline {subsection}{\numberline {4.4}Asynchronous actions}{18}{subsection.1.4.4}
\contentsline {subsection}{\numberline {4.5}Reactive actions}{18}{subsection.1.4.5}
\contentsline {subsection}{\numberline {4.6}Guarded actions}{18}{subsection.1.4.6}
\contentsline {section}{\numberline {5}User-defined actions in \textsf {Prolog}}{20}{section.1.5}
\contentsline {subsubsection}{\numberline {5.0.1}Examples of unification}{20}{subsubsection.1.5.0.1}
\contentsline {subsection}{\numberline {5.1}The \texttt {solve} operation.}{20}{subsection.1.5.1}
\contentsline {subsection}{\numberline {5.2}Loading and using a user-defined theory}{20}{subsection.1.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}The initialization directive.}{21}{subsubsection.1.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}On backtracking.}{22}{subsubsection.1.5.2.2}
\contentsline {subsection}{\numberline {5.3}Using the actor in \textsf {Prolog}{} rules }{22}{subsection.1.5.3}
\contentsline {subsection}{\numberline {5.4}The operator actorOp}{23}{subsection.1.5.4}
\contentsline {subsection}{\numberline {5.5}Rules at model level}{24}{subsection.1.5.5}
\contentsline {subsection}{\numberline {5.6}From \textsf {Prolog}{} to \textsf {Java}{} again}{24}{subsection.1.5.6}
\contentsline {subsubsection}{\numberline {5.6.1}Guards as problem-solving operation.}{25}{subsubsection.1.5.6.1}
\contentsline {subsubsection}{\numberline {5.6.2}The user-defined \texttt {select/1} operation.}{25}{subsubsection.1.5.6.2}
\contentsline {subsection}{\numberline {5.7}Workflow}{26}{subsection.1.5.7}
\contentsline {subsection}{\numberline {5.8}Examples of problem solving with \textsf {tuProlog}{} }{26}{subsection.1.5.8}
\contentsline {subsubsection}{\numberline {5.8.1}configuration.}{28}{subsubsection.1.5.8.1}
\contentsline {subsubsection}{\numberline {5.8.2}family.}{29}{subsubsection.1.5.8.2}
\contentsline {subsubsection}{\numberline {5.8.3}accessData.}{29}{subsubsection.1.5.8.3}
\contentsline {subsubsection}{\numberline {5.8.4}output.}{29}{subsubsection.1.5.8.4}
