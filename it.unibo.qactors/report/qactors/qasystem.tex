\section{Overcoming static configurations}
In the \qa{} metamodel, a software system is made of a collections of \textit{Actors}, each included in a computational node called \textit{Context}. A \textit{Context} can be viewed as a \textit{subsystem} with a unique name and a unique IP address (host,port).

In several applications, the set of \textit{Contexts} and actors that compose the system can be statically fixed, but there are other cases in which a system must dynamically change its configuration. In particular:

\begin{enumerate}
\item we should allow an actor to dynamically create other actors (actor instances) within its \textit{Context};
\item we should allow the dynamic introduction of new \textit{Contexts} within a given system, together with the possibility to support the interaction among all the actors, both statically defined and dynamically created.
\end{enumerate}


\section{Dynamic creation of actors within a Context}
Let us introduce the model of a very simple actor that receives and prints 2 messages and sends a reply to the sender;
 
\lstinputlisting[language=java,caption={ \texttt{agentprototype.qa} }, firstline=1  ]{../../../it.unibo.qactor.dynamic/src/agentprototype.qa}

This actor has the 'static' name \texttt{agentprototype}, but it refers to its own name with the built-in rule \texttt{actorobj/1}. The generated code provides (in the directory \texttt{src}) the class \texttt{Agentprototype} that defines the following constructor:

\begin{Verbatim}[fontsize=\scriptsize, frame=single , label=Constrcutor in class \textit{it.unibo.agentprototype.Agentprototype}]
public Agentprototype(String actorId, ActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
...
} 
\end{Verbatim}

This constructor can be used to build new instances in a dynamical way, by using an actor-creation rule already defined in the built-in actor's \texttt{WorldTheory}:

\subsection{An actor-creation rule}
\lstinputlisting[language=ddr,caption={ \texttt{createActor rule (in \texttt{WorldTheory})} }, firstline=33,lastline=41]{../../../it.unibo.qactor.dynamic/pivotTheory.pl}

The rule calls the constructor of the prototype given in the argument \texttt{PrototypeClass} by using the same context and the same output view of the creator's agent. Thus each new instance is always creted in the context of the creator agent.

\subsection{An actor-instance creator}
For example, the following actor \texttt{agentcreator} dynamically generates two instances of \texttt{agentprototype} and sends to each instance a messages, waiting for a reply:

\lstinputlisting[language=ddr,caption={ \texttt{agentcreation.qa} }, firstline=1]{../../../it.unibo.qactor.dynamic/src/agentcreation.qa}


The guard 
\texttt{[!? newName(agentprototype,Name,N)]} 
in Plan \texttt{createInstance} makes reference to the built-in \texttt{newname} rule for instance-names creation, that stores in the actor's knowledge base a fact \texttt{instance/3}:

\lstinputlisting[language=ddr,caption={ instance fact }, firstline=63,lastline=63]{../../../it.unibo.qactor.dynamic/pivotTheory.pl}


\subsection{The name-creation rule}
The \texttt{newname/3} name creation rule is defined in the \textit{WorldTheory} as follows:

\lstinputlisting[language=ddr,caption={ \texttt{newName rule (in \texttt{WorldTheory})} }, firstline=47,lastline=60]{../../../it.unibo.qactor.dynamic/pivotTheory.pl}

\section{Dynamic addition of Contexts}

In this section we give an example of a dynamic system that starts from a single actor (and thus from a single node) called \texttt{pivot}. Other actors can interact with \texttt{pivot} by asking it to perform two basic operations:

\begin{itemize}
\item \textit{\textbf{register(NickName,Name)}}: the actor of name \texttt{Name} informs \texttt{pivot} of its existence by providing a \textit{NickName} that can be used by other actors to find the actor;
\item \textit{\textbf{getactor(NickName)}}: the actor makes a query to \texttt{pivot} in order to obtain the (name of the) actor that corresponds to the given \texttt{NickName}. The answer given by \texttt{pivot} can be used as destination in message-passing operations.
\end{itemize}

With these two basic operations, we can extends a systems with new actors working in different nodes and we can allow an actor to interact with another one, just knowing its nickname. 

\subsection{A first dynamic actor}
In the following example:

\begin{enumerate}
\item an actor (\texttt{agent1}) registers itself to the \texttt{pivot} with the nickname \texttt{zorro};
\item \texttt{agent1} makes a request to  \texttt{pivot} for an actor with nickname \texttt{batman};
\item  \texttt{agent1} forwards a \textit{dispatch} to the actor given in the answer to its request.
\end{enumerate}

\lstinputlisting[language=ddr,caption={ \texttt{agent1.qa} }, firstline=1 , lastline=46]{../../../it.unibo.qactor.dynamic/src/agent1.qa}

Of course, this kind of system can work only if all the dynamic agents share a common knowledge about the messages that can exchange among them.

 
\subsection{A second dynamic actor}
Each dynamic agent starts from a system composed of two actors (and contexts): itself and \texttt{pivot}. 

Thus, the structural part of the model of the '\texttt{batman}' agent is quite similar to that of  \texttt{agent1} above; as regards the behaviour, we suppose here that '\texttt{batman}' first registers itself to the \texttt{pivot} and then waits for messages:

\lstinputlisting[language=ddr,caption={ \texttt{agent2.qa} }, firstline=1 , lastline=46]{../../../it.unibo.qactor.dynamic/src/agent2.qa}

Of course, \texttt{agent2} must run before \texttt{agent1} since \texttt{agent1} must find it already registered in the system.
 
\section{The pivot }
Le us introduce now a possible model for the \texttt{pivot} :
 

\lstinputlisting[language=ddr,caption={ \texttt{pivot.qa} }, firstline=1 , lastline=44]{../../../it.unibo.qactor.dynamic.pivot/src/pivot.qa}

\subsection{The pivot application theory}
Several operations of the \texttt{pivot} actor are implemented as \tuprolog{} rules by means of the user-defined \textit{pivotTheory}:

\lstinputlisting[language=java,caption={ \texttt{pivotTheory.pl} }, firstline=1 ,lastline=27 ]{../../../it.unibo.qactor.dynamic/pivotTheory.pl}
 
\subsection{The pivot at start-up} 

\begin{Verbatim}[fontsize=\scriptsize, frame=single , label=Startup]
sudo crontab -u root -e

@reboot cd /home/pi/nat/pivot && screen sh -c './pivot.sh ; read'

#!/bin/bash
echo hello pivot
sudo java -jar  MainCtxPivot.jar 
exec bash
\end{Verbatim}	



%%\begin{Verbatim}[fontsize=\scriptsize, frame=single , label=Arduino code site]
%%  
%%\end{Verbatim}

 
%%% \lstinputlisting[language=java,caption={ \texttt{DeviceButtonArduinoQa.java} }, firstline=1 , lastline=24]{../../src/it/unibo/devices/qa/DeviceButtonArduinoQa.java}

\medskip 
\scriptsize
\framebox[15cm]{ %
\begin{minipage}{140mm}
\end{minipage}}
\normalsize
\medskip 