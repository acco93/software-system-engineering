package it.unibo.qactors.qa.systems;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import it.unibo.ctxEx5_Receiver.MainCtxEx5_Receiver;
import it.unibo.ctxEx5_Sender.MainCtxEx5_Sender;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
 
public class TestQaSenderReceiver {
   
  QActorContext ctxSender, ctxReceiver;

  @Before
	public void setUp() throws Exception{
		//R1 (sender)
		activateCtxSender( );
		//R1 (receiver)
		activateCtxReceiver();
	}
 
	 @After
	 public void terminate(){
		ctxSender.terminateQActorSystem();
		ctxReceiver.terminateQActorSystem();
	 }

	 @Test
	public void testSenderReceiver() {  
  		System.out.println("====== testSenderReceiver ==============="  );
		try {
			Thread.sleep(6000);  //wait for the end
 			QActor qareceiver   = //we work in the same JVM => QactorUtils static is shared
 					QActorUtils.getQActor("ex5_qareceiver_ctrl"); 
			assertTrue("ctx qareceiver",   qareceiver != null);
 			assertTrue("ctx sender port",   ctxSender.getCtxPort()==8037);
 			assertTrue("ctx receiver port", ctxReceiver.getCtxPort()==8047);			
			SolveInfo sol = qareceiver.solveGoal("getVal(inputInfoCounter,V)");
 			System.out.println( "InputInfoCounter=" + sol.getVarValue("V") );
 			Term vt = sol.getVarValue("V");
 			int v = Integer.parseInt(vt.toString());
			assertTrue("testSenderReceiver", v==1);
			sol = qareceiver.solveGoal("numOfEvents(N)");
 			System.out.println( "numOfEvents=" + sol.getVarValue("N") );
 			vt = sol.getVarValue("N");
 			v = Integer.parseInt(vt.toString());
 			 
 			sol = qareceiver.solveGoal("getVal( inputInfoCounter, V )");
 			vt = sol.getVarValue("V");
 			v = v + Integer.parseInt(vt.toString());
 			System.out.println( "numOfEvents=" + v );
			assertTrue("testSenderReceiver", v==5);
 		} catch (Exception e) {
			fail("testSenderReceiver " + e.getMessage() );
		}		
	}
 	
	protected void activateCtxSender( ){
		new Thread(){
			public void run(){
				try {
					 ctxSender = MainCtxEx5_Sender.initTheContext(); 
				} catch (Exception e) {
 					fail("activteCtxSender");
				}			
			}
		}.start();
	}
	
	protected void activateCtxReceiver( ){
		new Thread(){
			public void run(){
				try {
					ctxReceiver = MainCtxEx5_Receiver.initTheContext(); 
				} catch (Exception e) {
					fail("activteCtxReceiver");
				}			
			}
		}.start();
	}

}
