package it.unibo.qactors.qa.systems;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import it.unibo.ctxEx4.MainCtxEx4;
import it.unibo.qactors.QActorContext;


public class TestEventPerceiver {
	private QActorContext ctx;
	
	@Before
	public void setUp() throws Exception{
		ctx = MainCtxEx4.initTheContext();
 	}
 	
	 @After
	 public void terminate(){
   		ctx.terminateQActorSystem();
	 }

	@Test
	public void run() { 
	 		try {
				Thread.sleep(3000) ; 
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

}
