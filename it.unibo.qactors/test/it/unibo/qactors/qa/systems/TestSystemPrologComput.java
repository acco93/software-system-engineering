package it.unibo.qactors.qa.systems;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import it.unibo.ctxEx9.MainCtxEx9;
import it.unibo.qactors.QActorContext;


public class TestSystemPrologComput {
	private QActorContext ctx;
	
	@Before
	public void setUp() throws Exception{
		ctx = MainCtxEx9.initTheContext();
 	}
	@After
	public void terminate(){
		ctx.terminateQActorSystem();
	}
	@Test
	public void run() { 
	 		try {
				Thread.sleep(6000) ;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
}
