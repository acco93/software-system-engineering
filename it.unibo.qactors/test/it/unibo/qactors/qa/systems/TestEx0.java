package it.unibo.qactors.qa.systems;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import it.unibo.ctxEx0.MainCtxEx0;
import it.unibo.qactors.QActorContext;


public class TestEx0 {
	private QActorContext ctx;
	
	@Before
	public void setUp() throws Exception{
		ctx = MainCtxEx0.initTheContext();
// 		ctx = QActorContext.initQActorSystem(
//				"ctxex0", "./srcMore/it/unibo/ctxEx0/ex0.pl", 
//				"./srcMore/it/unibo/ctxEx0/sysRules.pl", SituatedSysKb.standardOutEnvView);
	}
	 @After
	 public void terminate(){
		 ctx.terminateQActorSystem();
	 }
	@Test
	public void run() { 
	 		try {
				Thread.sleep(2000) ;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
 
}
