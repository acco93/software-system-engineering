package it.unibo.qactors.systems;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import it.unibo.qactors.QActorContext;
import it.unibo.system.SituatedSysKb;

public class TestQActorContextAkka {
 
private String ctxName = "ctxMockAkka";
private String ctxOtherName = "ctxOther";
 
  	@Test
	public void emptyKb() {
		try {
 			QActorContext.initQActorSystem(
					"ctxtest", "./test/it/unibo/qactors/systems/emptySystemKb.pl", 
					"./test/it/unibo/qactors/systems/sysRules.pl", 
					SituatedSysKb.standardOutEnvView);
	 		fail("emptyKbAkkaTest "   );
		} catch (Exception e) {
			System.out.println("            *** emptyKb " +  e.getMessage());
			assertTrue("emptyKbAkkaTest " , e.getMessage() != null );
		}
		
	}
	
 	@Test
	public void outEnvViewNull() {
		try {
 			QActorContext ctx = QActorContext.initQActorSystem(
 					"ctxNoActors", "./test/it/unibo/qactors/systems/noactorctx.pl", 
 					"./test/it/unibo/qactors/systems/sysRules.pl", 
 					null);
			assertTrue("outEnvViewNukk " , ctx.getOutputEnvView() != null);
		} catch (Exception e) {
	 		fail("outEnvViewNull "   );
		}		
	}

 	@Test
	public void actorTest() { //TODO CHECK
		try {
			activteCtx(ctxName);
			activteCtx(ctxOtherName);
 	 		Thread.sleep(4000);
		} catch (Exception e) {
			fail("actorTest " + e.getMessage() );
		}		
	}

	protected void activteCtx(final String ctx){
		new Thread(){
			public void run(){
				try {
					System.out.println("            *** activteCtx " +  ctx );
					QActorContext.initQActorSystem(
							ctx, "./test/it/unibo/qactors/systems/mockAkkaSystemKb.pl", 
							"./test/it/unibo/qactors/systems/sysRules.pl", 
							SituatedSysKb.standardOutEnvView);
				} catch (Exception e) {
 					e.printStackTrace();
				}			
			}
		}.start();
	}
}
