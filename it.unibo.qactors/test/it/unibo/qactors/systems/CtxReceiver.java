package it.unibo.qactors.systems;
 
import java.awt.Color;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.qactors.QActorContext;

public class CtxReceiver  {

	public static QActorContext createSubSystem() throws Exception{
		IBasicEnvAwt env = new EnvFrame("ctxReceiver", null, Color.yellow, Color.black);
		env.init();
		env.writeOnStatusBar("ctxReceiver" + " | working ... ", 14);
		return QActorContext.initQActorSystem("ctxReceiver", 
				"./test/it/unibo/qactors/systems/senderReceiverKb.pl", 
				"./test/it/unibo/qactors/systems/sysRules.pl", env.getOutputEnvView());		
	}
	public static void main(String[] args) throws Exception {
		 //Requirement R1 (receiver)
		createSubSystem();
	}
}
 
