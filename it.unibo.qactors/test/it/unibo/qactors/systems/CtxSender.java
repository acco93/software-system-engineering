package it.unibo.qactors.systems;
 
import java.awt.Color;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.qactors.QActorContext;

public class CtxSender  {
	public static QActorContext createSubSystem() throws Exception{
		IBasicEnvAwt env = new EnvFrame("ctxSender", null, Color.cyan, Color.black);
		env.init();
		env.writeOnStatusBar("ctxSender" + " | working ... ", 14);
		return	QActorContext.initQActorSystem("ctxSender", 
				"./test/it/unibo/qactors/systems/senderReceiverKb.pl", 
				"./test/it/unibo/qactors/systems/sysRules.pl", env.getOutputEnvView());		
	}
	public static void main(String[] args) throws Exception {
		//Requirement R1 (sender)
		createSubSystem();
	}
}
