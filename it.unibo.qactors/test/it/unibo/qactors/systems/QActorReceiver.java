/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.systems;

import alice.tuprolog.SolveInfo;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.akka.QActor;

public class QActorReceiver extends QActor{
	protected String evId = "alarm";
	protected int inputInfoCounter = 0;		//FOR TESTING
	
	public QActorReceiver(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, 
				"./test/it/unibo/qactors/systems/WorldTheory.pl", //required to compute fibo/2			
				outEnvView, "noplan");
 	}
	@Override
	public void preStart() {
		try {		 			
			registerForEvent( evId,1000 );// R7 (all events)  
			super.preStart();
		} catch (Exception e) {
 			e.printStackTrace();
		}		
	}
	@Override
	protected void doJob() throws Exception {
   	}
	
 	// === Requirement R7 (show events) ===			
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		inputInfoCounter++;
		println(getName() + " (QActorReceiver)handleQActorEvent : " + ev.getDefaultRep() + + inputInfoCounter);	
	}
	// === Requirement R7 (show messages) ===	 
	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		inputInfoCounter++;
		println(getName() + " (QActorReceiver)handleQActorMessage " + msg.getDefaultRep() + inputInfoCounter);	
		evalPayload(msg.msgSender(), msg.msgContent()); // R8  			
	}
	// === Requirement R8  (evaluate payload)  ===
	protected void evalPayload(String senderId, String payload){
		SolveInfo sol = solveGoal(payload);
		try {
			println("" + sol.getSolution());
			if(sol != null && sol.isSuccess()) 
				sendAnswer(senderId, sol.getSolution().toString());	//R9
			else
				sendAnswer(senderId, "eval failure");				//R9
		} catch (Exception e) {
				e.printStackTrace();
		}		
	}
	// === Requirement R9  ===
	protected void sendAnswer(String actorId, String answerMsg) throws Exception{
		sendMsg("answer", actorId, QActorContext.dispatch,  answerMsg );
 	}

/*
 * FOR TESTING	
 */	
	public int getInputInfoCounter(){
		return inputInfoCounter;
	}
}
