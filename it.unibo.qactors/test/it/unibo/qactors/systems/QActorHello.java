package it.unibo.qactors.systems;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.akka.QActor;

public class QActorHello extends QActor{

	public QActorHello(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, 				
				"./test/it/unibo/qactors/systems/WorldTheory.pl",			
				outEnvView, "noplan");
 	}
	@Override
	public void preStart() {
		try {
 			println( getName() + " preStart "   );
			super.preStart();
		} catch (Exception e) {
 			e.printStackTrace();
		}		
	}
	@Override
	protected void doJob() throws Exception {
 		println( getName() + " Hello world"   );
 	}
}
