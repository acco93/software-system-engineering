package it.unibo.qactors.systems;

import java.util.Iterator;

import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class QActorEvaluator extends QActor{

	public QActorEvaluator(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx,outEnvView );
	}

	@Override
	protected void doJob()   {
 		println( getName() + " Hello world"   );
 		QActorUtils.loadTheoryFromFile(pengine, "./test/it/unibo/qactors/systems/userRules.pl");
 		try {
			findSingleData();
			findManyData();
		} catch (Exception e) {
 			e.printStackTrace();
		}
 	}
	
	protected void findSingleData() throws Exception{
		SolveInfo sol = this.solveGoal("temperature(bologna,TEMPERATURE,celsius)");
		if( sol.isSuccess() ){
			println("bologna:"+sol.getVarValue("TEMPERATURE"));
		}
	}
	protected void findManyData() throws Exception{
		SolveInfo sol = this.solveGoal("allTemp(V)");
		if( sol.isSuccess() ){
			println("allTemp:"+sol.getVarValue("V"));
			Struct tempList  = (Struct) sol.getVarValue("V");
			Iterator<? extends Term> it = tempList.listIterator();
			while( it.hasNext() ){
				println(""+it.next());
			}
		}		
	}
}
