package it.unibo.qactors.systems;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
 
public class TestASenderReceiver {
   
  QActorContext ctxSender, ctxReceiver;
  
  	@Before
	public void setUp()  {
		//R1 (sender)
  		activateCtxSender( );
		//R1 (receiver)
		activateCtxReceiver();
	}
	 @After
	 public void terminate(){
		ctxSender.terminateQActorSystem();
		ctxReceiver.terminateQActorSystem();
	 }
	 @Test
	public void testSenderReceiver() {  
  		System.out.println("====== TestSenderReceiver ==============="  );
		try {
  			QActorReceiver qareceiver   = //we work in the same JVM => QactorUtils static is shared
 					(QActorReceiver) QActorUtils.getQActor("qareceiver");
			Thread.sleep(4000);  //wait for the end
 			System.out.println( "InputInfoCounter=" + qareceiver.getInputInfoCounter() );
 			assertTrue("ctx sender port",   ctxSender.getCtxPort()==8030);
 			assertTrue("ctx receiver port", ctxReceiver.getCtxPort()==8040);			
			assertTrue("testSenderReceiver", qareceiver.getInputInfoCounter()==5);
  		} catch (Exception e) {
			fail("actorTest " + e.getMessage() );
		}		
	}
 	
	protected void activateCtxSender( ){
		new Thread(){
			public void run(){
				try {
					 ctxSender = CtxSender.createSubSystem(); 
				} catch (Exception e) {
 					e.printStackTrace();
				}			
			}
		}.start();
	}
	
	protected void activateCtxReceiver( ){
		new Thread(){
			public void run(){
				try {
					ctxReceiver = CtxReceiver.createSubSystem();
				} catch (Exception e) {
	 				e.printStackTrace();
				}			
			}
		}.start();
	}

}
