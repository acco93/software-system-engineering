testConsult :- output("user rules on").
temperature( bologna, 27, celsius ).
temperature( cesena, 28, celsius ).
temperature( rimini, 26, celsius ).

allTemp(V) :- findall( temperature( C,T,S ), temperature( C,T,S ), V).
