/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.systems;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.akka.QActor;

public class QActorMockAkkaFail extends QActor{
	String evId = "info";
	
	public QActorMockAkkaFail(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, 
				"./test/it/unibo/qactors/systems/WorldTheory.pl",			
				outEnvView, "noplan");
 	}

	@Override
	public void preStart() {
		try {
			registerForEvent( evId, 1000 );
			super.preStart();
		} catch (Exception e) {
 			e.printStackTrace();
		}
		
	}

	protected void doJob() throws Exception {
		throw new Exception("doJob failure");
   	}

 


}
