package it.unibo.qactors.systems;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import alice.tuprolog.SolveInfo;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
 

public class TestQActorEvaluator {
   private QActorContext ctx;
   
  	@Before
	public void setUp() throws Exception  {
   		ctx = CtxEvaluator.createSystem();
 	}
	 @After
	 public void terminate(){
		ctx.terminateQActorSystem();
 	 }

 	@Test
	public void evaluatorTest() {  
  		System.out.println("====== evaluatorTest ==============="  );
		try {
 			System.out.println("evaluatorTest:"+ctx.getName());
			System.out.println("evaluatorTest:"+ctx.getCtxPort());
 			assertTrue("evaluatorTest", ctx.getCtxPort()==8010);
 			QActor qa     = QActorUtils.getQActor("qaevaluator");
  			SolveInfo sol = QActorUtils.solveGoal("temperature( bologna, V, celsius )",qa.getPrologEngine());
  			assertTrue("evaluatorTest", sol.isSuccess());
  			int v = Integer.parseInt(  sol.getVarValue("V").toString() );
			System.out.println("evaluatorTest v:"+ v);
  			assertTrue("evaluatorTest v", v==27);
  		} catch (Exception e) {
			fail("evaluatorTest " + e.getMessage() );
		}		
	}

}
