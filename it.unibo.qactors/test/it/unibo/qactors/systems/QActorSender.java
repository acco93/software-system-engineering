/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.systems;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.akka.QActor;

public class QActorSender extends QActor{
String msgId = "eval";
String evId  = "alarm";

	public QActorSender(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, outEnvView);
 	}
	@Override
	protected void doJob() throws Exception {
		sendDispatchToItself(); // R2		
		emitAlarms("first");	//R3
		sendEval();				//R4
		emitAlarms("again");	//R5		
	}
 	// === Requirement R2 ===
	protected void sendDispatchToItself() throws Exception{
		println( getName() + " send Dispatch to itself "   );
		sendMsg("info", getName(), QActorContext.dispatch, "autoMsg from " + getName() );		
	}	
 	// === Requirement R3 / R5 ===
	protected void emitAlarms(String prefix) throws Exception{
		for( int i=1; i<=2;i++){
			delay(500);
			println( getName() + " EMIT event " + evId  );
			emit( evId, "hello_"+prefix+i);
		}		
	}
 	// === Requirement R4 ===
	protected void sendEval() throws Exception{
		println( getName() + " SEND to receiver "   );
		sendMsg( "info", "qareceiver", QActorContext.dispatch, "fibo(25,V)"    );		
	}		
 	// === Requirement R6 ===
	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " handleQActorMessage " + msg.getDefaultRep() );	
	}	
}
