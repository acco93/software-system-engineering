package it.unibo.qactors.systems;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import alice.tuprolog.SolveInfo;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
 

public class TestQActorHelloWorld {
   private QActorContext ctx;
   
  	@Before
	public void setUp() throws Exception  {
   		ctx = CtxHelloWorld.createSystem();
 	}
	 @After
	 public void terminate(){
		ctx.terminateQActorSystem();
 	 }

 	@Test
	public void actorTest() {  
  		System.out.println("====== actorTest ==============="  );
		try {
// 			Thread.sleep(500);
			System.out.println("actorTest:"+ctx.getName());
			System.out.println("actorTest:"+ctx.getCtxPort());
 			assertTrue("actorTest", ctx.getCtxPort()==8010);
 			QActor qa     = QActorUtils.getQActor("qahello");
 			SolveInfo sol = QActorUtils.solveGoal("fib(41,V)",qa.getPrologEngine());
 			assertTrue("actorTest", sol.isSuccess());
 			int v = Integer.parseInt(  sol.getVarValue("V").toString() );
 			assertTrue("actorTest fib", v==165580141);
  		} catch (Exception e) {
			fail("actorTest " + e.getMessage() );
		}		
	}

}
