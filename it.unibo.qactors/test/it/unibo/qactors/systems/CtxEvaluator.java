package it.unibo.qactors.systems;
 
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.system.SituatedSysKb;

public class CtxEvaluator  {
 
	public static QActorContext createSystem() throws Exception{
		IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
 		return QActorContext.initQActorSystem("ctxEvaluator", 
				"./test/it/unibo/qactors/systems/pevaluator.pl", 
				"./test/it/unibo/qactors/systems/sysRules.pl", 
				outEnvView); 	
	}
	public static void main(String[] args) throws Exception {
		createSystem(); 
	}
}
 
