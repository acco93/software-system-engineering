%==============================================
% USER DEFINED
% system configuration: senderReceiverKb.pl
%==============================================
context( ctxSender,   "localhost", "TCP", "8030" ).
context( ctxReceiver, "localhost", "TCP", "8040" ).
%%% -------------------------------------------
qactor( qasender,   ctxSender,   "it.unibo.qactors.systems.QActorSender"   ).
qactor( qareceiver, ctxReceiver, "it.unibo.qactors.systems.QActorReceiver" ). 

 