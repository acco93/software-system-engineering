package it.unibo.qactors.systems;
 
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.system.SituatedSysKb;

public class CtxHelloWorld  {
 
	public static QActorContext createSystem() throws Exception{
		IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
 		return QActorContext.initQActorSystem("ctxHelloWorld", 
				"./test/it/unibo/qactors/systems/systemConfig.pl", 
				"./test/it/unibo/qactors/systems/sysRules.pl", 
				outEnvView); 	
	}
	public static void main(String[] args) throws Exception {
		createSystem(); 
	}
}
 
