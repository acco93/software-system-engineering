package it.unibo.qactors.systems;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import it.unibo.qactors.QActorContext;
import it.unibo.system.SituatedSysKb;

public class TestQActorEmptyConfig {
   
  	@Test
	public void emptyKb() {
  		System.out.println("====== emptyKb ================="  );
		try {
 			QActorContext.initQActorSystem("ctxtest", 
					"./test/it/unibo/qactors/systems/emptySystemKb.pl", 
					"./test/it/unibo/qactors/systems/sysRules.pl", 
					SituatedSysKb.standardOutEnvView);
			Thread.sleep(500);
			fail("emptyKbAkkaTest "   );
 		} catch (Exception e) {
			System.out.println("           *** emptyKb " +  e.getMessage());
			assertTrue("emptyKbAkkaTest " , e.getMessage() != null );
		}		
	}
 

}
