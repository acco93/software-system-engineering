package it.unibo.qactors.platform;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IActorMessage;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;
 

public class TestQActorMessage {
	private String msgId       = "info";
	private String msgType     = "dispatch";
	private String msgSender   = "actorSender";
	private String msgReceiver = "actorReceiver";
	private String msgContent  = "hello(world)";
	private String msgNum = "1";		
	
	private IActorMessage workMsg;
	
	@Before
	public void setUp(){
//		System.out.println("TestQActorMessage setUp");
		try {
			workMsg = new QActorMessage(msgId,msgType,msgSender,msgReceiver,msgContent,msgNum);
		} catch (Exception e) {
 			fail("setUp " + e.getMessage() );
		}
	}

	@Test
	public void testCreateArgs() {
		System.out.println(" *** testCreateArgs *** ");		
		assertTrue("testCreateArgs", 
 				workMsg.msgId().equals(msgId) &&
 				workMsg.msgType().equals(msgType) &&
 				workMsg.msgSender().equals(msgSender) &&
 				workMsg.msgReceiver().equals(msgReceiver) &&
 				workMsg.msgContent().equals(msgContent) &&
 				workMsg.msgNum().equals(msgNum)				
			);
	}

	@Test
	public void testCreateStruct() {
		System.out.println(" *** testCreateStruct *** ");		
 		String msgStr =  
		"msg(A,B,C,D,E,F)".replace("A", msgId).replace("B", msgType).
			replace("C", msgSender).replace("D", msgReceiver).replace("E", msgContent).replace("F", msgNum );
		Term msgTerm = Term.createTerm(msgStr);
		assertTrue("testCreateStruct", msgTerm != null );
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgStr);
			assertTrue("testCreateStruct", 
					msg.msgId().equals(msgId) &&
					msg.msgType().equals(msgType) &&
					msg.msgSender().equals(msgSender) &&
					msg.msgReceiver().equals(msgReceiver) &&
					msg.msgContent().equals(msgContent) &&
					msg.msgNum().equals(msgNum)				
				);
		} catch (Exception e) {
			fail("testCreateStruct");
	 	}
	}

	@Test
	public void testCreateNoNumSyntax() {
		System.out.println(" *** testCreateNoNumSyntax *** ");		
		try{
			new QActorMessage(msgId,msgType,msgSender,msgReceiver,msgContent,"a");
			fail("testCreateNoNumSyntax");
		}catch(Exception e){
			assertTrue("testCreateNoNumSyntax", true);
		}
	}
	@Test
	public void testCreateNoPrologSyntax() {
		System.out.println(" *** testCreateNoPrologSyntax *** ");		
		try{
			String msgStr =  "msg(info, dispatch, sender, receiver, a( , 1)";
 			new QActorMessage(msgStr);
			fail("testCreateNoPrologSyntax");
		}catch(Exception e){
			assertTrue("testCreateNoPrologSyntax", true);
		}
	}

	@Test
	public void testDefaultRep() {
		System.out.println(" *** testDefaultRep *** ");		
		 String msgRep = workMsg.getDefaultRep();
		 Struct msgTerm = (Struct) Term.createTerm(msgRep);
 		 assertTrue("testDefaultRep", 
				 msgTerm.getArg(0).toString().equals(msgId) &&
				 msgTerm.getArg(1).toString().equals(msgType) &&
				 msgTerm.getArg(2).toString().equals(msgSender) &&
				 msgTerm.getArg(3).toString().equals(msgReceiver) &&
				 msgTerm.getArg(4).toString().equals(msgContent) &&
				 msgTerm.getArg(5).toString().equals(msgNum)
		 );
		 
	}
	@Test
	public void testDefaultRepMsgTypeNull() {
		System.out.println(" *** testDefaultRepMsgTypeNull *** ");		
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgId,null,msgSender,msgReceiver,msgContent,msgNum);
			String msgRep = msg.getDefaultRep();
	  		 assertTrue("testDefaultRepMsgTypeNull", msgRep.equals("msg(none,none,none,none,none,0)")
			 );		
		} catch (Exception e) {
			fail("testDefaultRepMsgTypeNull");
 		}
	}
	@Test
	public void testContentNormalString() {
		System.out.println(" *** testContentNormalString *** ");		
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgId,msgType,msgSender,msgReceiver,"hello world",msgNum);
			 String msgRep    = msg.getDefaultRep();
			 System.out.println("		*** testContentNormalString msgRep=" + msgRep);
			 Struct msgTerm   = (Struct) Term.createTerm(msgRep);
			 System.out.println("		*** " + msgTerm);
			 /*
			  * NOTE the '' around hello world
			  */
			 assertTrue("testContentNormalString", 
					 msg.msgContent().equals("'hello world'") 
					 &&  msgTerm.getArg(4).toString().equals("'hello world'")
	 		);
		} catch (Exception e) {
			fail("testContentNormalString");
		}
 	}
	@Test
	public void testContentQuotedString() {
		System.out.println(" *** testContentQuotedString *** ");		
		IActorMessage msg;
		try {
			 msg = new QActorMessage(msgId,msgType,msgSender,msgReceiver,"'hello world'",msgNum);
			 String msgRep    = msg.getDefaultRep();
			 System.out.println("		*** testContentQuotedString msgRep=" + msgRep);
			 Struct msgTerm   = (Struct) Term.createTerm(msgRep);
			 System.out.println("		*** " + msgTerm);
			 assertTrue("testContentQuotedString", 
					 msg.msgContent().equals("'hello world'") 
					 &&  msgTerm.getArg(4).toString().equals("'hello world'")
	 		);
		} catch (Exception e) {
			fail("testContentQuotedString");
		}
 	}

	@Test
	public void testEmit() {
		try {
			System.out.println(" *** testEmit *** ");		
			IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
			QActorContext ctx = QActorContext.initQActorSystem("ctxSimpleAction", 
					"./test/it/unibo/qactors/akka/action/simple/noQActorSystemKb.pl", 
					"./test/it/unibo/qactors/akka/action/simple/sysRules.pl", stdOut);
			assertTrue("testEmit" , ctx != null);	
			EventHandlerComponent evh = 
					EventHandlerComponent.createEventHandler("evh", ctx, new String[]{"evId"}, stdOut );
			
			System.out.println("testEmit emits at time=" + EventLoopActor.getLocalTime().getTheTime()  );
			QActorUtils.raiseEvent(ctx, "emitter", "evId", "evContent");
			
 			//Look at the eventhandler
			IEventItem ev = evh.waitForCurentEvent();
    		System.out.println("testEmit ev= " + ev.getDefaultRep() );
 			assertTrue("testEmit",  ev.getDefaultRep().startsWith("msg(evId,event,emitter,none,evContent,"));
			
			System.out.println("testEmit emits at time=" + EventLoopActor.getLocalTime().getTheTime()  );
			QActorUtils.raiseEvent(ctx, "otherEmitter", "evId", "otherEvContent");
			ev = evh.waitForCurentEvent();
 			System.out.println("testEmit ev= " + ev  );
 			assertTrue("testEmit",  ev.getDefaultRep().startsWith("msg(evId,event,otherEmitter,none,otherEvContent,"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
 

 
}
