package it.unibo.qactors.receive;
 
import java.awt.Color;
import java.io.FileInputStream;
import java.io.InputStream;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class CtxActorReceiver  {

 	
 	public static void main(String[] args) throws Exception {
		IBasicEnvAwt env = new EnvFrame("ctxActorReceiver", null, Color.yellow, Color.black);
		env.init();
		env.writeOnStatusBar("ctxActorReceiver" + " | working ... ", 14);
		QActorContext ctx = QActorContext.initQActorSystem(
				"ctxActorReceiver", "./test/it/unibo/qactors/receive/receiverSystemKb.pl", 
				"./test/it/unibo/qactors/receive/sysRules.pl", env.getOutputEnvView());
	}




}
 
