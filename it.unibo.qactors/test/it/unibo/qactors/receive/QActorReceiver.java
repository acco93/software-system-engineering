/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.receive;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class QActorReceiver extends QActor {

	public QActorReceiver(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx,
				"./test/it/unibo/qactors/akka/actor/WorldTheory.pl", outEnvView, "noplan");
	}

	protected void doJob() throws Exception {
		boolean res = init();

	}

	public boolean init() throws Exception { // public to allow reflection
		try {
			curPlanInExec = "init";
			boolean returnValue = suspendWork;
			while (true) {
				nPlanIter++;
				temporaryStr = " \"qareceiver STARTS\" ";
				println(temporaryStr);
				if (!planUtils.switchToPlan( "work").getGoon())
					break;
				break;
			} // while
			return returnValue;
		} catch (Exception e) {
			println(getName() + " ERROR " + e.getMessage());
			throw e;
		}
	}

	public boolean work() throws Exception { // public to allow reflection
		println(getName() + " addRule   ");
		this.addRule("allok");
		println(getName() + " onReceive -> p2 ");
//		this.onReceive("p2");
		println(getName() + " sendMsg to self   ");
		this.sendMsg("info", this.getName(), "dispatch", "hello");
		return true;
	}

	public boolean p2() throws Exception { // public to allow reflection
		try {
			curPlanInExec = "p2";
			boolean returnValue = suspendWork;
			while (true) {
				nPlanIter++;
				if ((guardVars = QActorUtils.evalTheGuard(this, " !?allok")) != null) {
					temporaryStr = " \"qareceiver p2\" ";
					temporaryStr = QActorUtils.substituteVars(guardVars, temporaryStr);
					println(temporaryStr);
				}
				returnValue = continueWork;
				break;
			} // while
			return returnValue;
		} catch (Exception e) {
			println(getName() + " ERROR " + e.getMessage());
			throw e;
		}
	}

}
