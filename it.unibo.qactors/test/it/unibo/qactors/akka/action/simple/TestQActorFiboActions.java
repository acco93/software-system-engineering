package it.unibo.qactors.akka.action.simple;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import alice.tuprolog.Prolog;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionSolveTimed;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

 
/*
 * ==========================================================
 * ==========================================================
 */
public class TestQActorFiboActions  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private QActorContext ctx ;
 	 private String terminationEvId = "endFibo";
 	 private IActorAction  action;
  	 private QActor actionEndHandler;	 
  	 private String alarm    = QActorUtils.locEvPrefix+"alarm";
  	 private String obstacle = QActorUtils.locEvPrefix+"obstacle";	 
  	 private String[] alermEvs = new String[]{alarm,obstacle};
   	 private Prolog pengine;
  	 private QActor qa;
  	 /*
	 *  
 	 */
	@Before
	public void setUp() throws Exception{
 		ctx = QActorContext.initQActorSystem("ctxactiontimed", 
				"./test/it/unibo/qactors/akka/action/simple/actiontimed.pl", 
				"./test/it/unibo/qactors/akka/action/simple/sysRules.pl", stdOut);
		assertTrue("setUp" , ctx != null );		
 		qa = QActorUtils.getQActor("dummyactor");
		assertTrue("setUp" , qa != null);			
		pengine = qa.getPrologEngine();
		assertTrue("setUp" , pengine != null);
 	}
	 @After
	 public void terminate(){
 			actionEndHandler.terminate();
//			qa.terminate();
//			QActorUtils.terminateTheQActorSystem();
 			ctx.terminateQActorSystem();
 	 }
	 
  	 	@Test
		public void solveTout() { 
			/*
			 * ------------------------------------------------------------
			 * Action that terminates with a time out
	 		 * ------------------------------------------------------------
			 */
			try {
				System.out.println("		==== solveTout ====  " );
				actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
		 		action = new ActionSolveTimed( "solveTout", qa, ctx, pengine, "fibo(28,V)",
						terminationEvId,alermEvs, stdOut, 300);
	 			action.execASynch(); 				
	 			//wait for the event 
				checkEvent(actionEndHandler,terminationEvId,"actionObs_result(fibo(28,317811),execTime(");			
			} catch (Exception e) {
				System.out.println("solveTout ERROR=" + e.getMessage());
//	 			fail("solveTout " + e.getMessage()   );
			}					
		}
	 
   	@Test
	public void solveReference25() { 
		try {
		System.out.println("		==== Action fibo(25,V) that terminate with success ====  " );
 	 	 	actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
	 							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
	 		action = new ActionSolveTimed("solve25", qa, ctx, pengine, "fibo(25,V)",
					terminationEvId,alermEvs, stdOut, 3000);
			action.execASynch(); 			
 			//wait for the event 
 			checkEvent(actionEndHandler,terminationEvId,"actionObs_result(fibo(25,75025),execTime(");
 		} catch (Exception e) {
			System.out.println("solveReference25 ERROR=" + e.getMessage());
// 			fail("solveReference25 " + e.getMessage()   );
		}				
	}
//   	@Test
//	public void solveReference28() { 
//		try {
//			System.out.println("		==== Action fibo(28,V) that terminate with success ====  " );
//			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
//						"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
//	 		action = new ActionSolveTimed("solve28", qa, ctx, pengine, "fibo(28,V)",
//					terminationEvId,alermEvs, stdOut, 8000);
//			action.execASynch(); 			
// 			//wait for the event 
// 			checkEvent(actionEndHandler,terminationEvId,"actionObs_result(fibo(28,317811),execTime(");
//		} catch (Exception e) {
//			System.out.println("solveReference28 ERROR=" + e.getMessage());
//// 			fail("solveReference28 " + e.getMessage()   );
//		}				
//	}
 	
  	
/*
 * ============================================================================
 */
	
 	
  	protected void checkEvent(QActor evh, String evId, String prefix) throws Exception{
		IEventItem ev = evh.waitForCurentEvent();
		String result = ev.getMsg();
		System.out.println("checkEvent "+result);
		assertTrue("checkEvent" , ev.getEventId().equals(evId) && result.startsWith(prefix));  		
  	}

}
