 
package it.unibo.qactors.akka.action.simple;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.platform.EventHandlerComponent;


public class ActionObsEvh extends EventHandlerComponent { //implements IActionHandler
//protected int count = 0;

	public ActionObsEvh(String name, QActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, eventIds, outEnvView);
  	}
	
	@Override
	public void doJob() throws Exception {
		
	}	
	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
 	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
// 		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );	
		this.setCurrentEvent(ev);
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " RECEIVES QActorMessage " + msg.getDefaultRep() );
		getSender().tell("done", getSelf());
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}

}
