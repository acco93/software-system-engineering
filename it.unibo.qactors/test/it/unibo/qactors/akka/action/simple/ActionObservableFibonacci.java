package it.unibo.qactors.akka.action.simple;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.action.ActionObservableGeneric;

public class ActionObservableFibonacci extends ActionObservableGeneric<String> {
private String goalTodo ;
private String myresult = "unknown";
private int n;
 	public ActionObservableFibonacci(String name, QActorContext ctx, String goalTodo, 
			String terminationEvId, IOutputEnvView outEnvView  ) throws Exception {
		super(name, ctx, terminationEvId, outEnvView);
 		this.goalTodo = goalTodo;  
//		println("%%% " + getName() +"  CREATED with terminationEvId=" +  terminationEvId );		
  	}

  	@Override
	public void execTheAction() throws Exception {
	  println("%%% " + getName() +"  going to run ..."  + goalTodo );
 	  myresult = fibonacci( goalTodo );
	  println("%%% " + getName() +"  EDNS myresult="  + myresult );
   } 	
	protected String fibonacci( String goalTodo ){
		Struct st = (Struct) Term.createTerm(goalTodo);
		n = Integer.parseInt( ""+st.getArg(0) );
		return ""+fibonacci(n);
	} 	
	protected long fibonacci( int n ){
		if( n<0 || n==0 || n == 1 ) return 1;
		else return fibonacci(n-1) + fibonacci(n-2);
	}
 	@Override
	public String getResultRep() {
  		String execTime = "execTime("+this.durationMillis+")";
 		return "fibo("+n+","+this.myresult+","+execTime+")";
	}
	@Override
	protected String endOfAction() throws Exception {
 		return "fibo("+n+","+myresult+")";
	}

}