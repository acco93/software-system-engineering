package it.unibo.qactors.akka.action.simple;

import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.akka.QActor;

public class DummyActor extends QActor{

	public DummyActor(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx,
 				"./test/it/unibo/qactors/akka/action/simple/WorldTheory.pl",
				outEnvView, "noplan");
 	}

	@Override
	protected void doJob() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void handleQActorString(String ev) {
		println(getName()+" string=" + ev);
	}

	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName()+" event=" + ev);
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName()+" msg=" + msg);
		
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
 		println(getName()+" terminate");
	}
 	@Override
	public void onReceive(Object message) throws Throwable {
  		println("QActor "+getName() + " RECEIVES " + message );
  		super.onReceive(message);
 	}

}
