package it.unibo.qactors.akka.action.simple;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import akka.actor.Props;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionFibonacciTimed;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

 
/*
 * ==========================================================
 * ACTIONS as AsynchActionGeneric (timed but NO tout handling)
 * 		execSynch / execAsych 
 * ==========================================================
 */
public class TestQActorTimedActions  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private QActorContext ctx ;
   	 private String terminationEvId = "";
  	 private QActor actionEndHandler;
 	 private String[] alarms = new String[]{}; 	 
 	 private IActorAction  action;
 	 private QActor qa; 	 
	/*
	 *  
 	 */
	@Before
	public void setUp() throws Exception{
		//Create a system without qactors. 
		/*
		 * The context is a singleton recorded in the ActorContext class
		 * To re-create the system we should call ActorContext.terminateQActorSystem()
		 * The context creation calls QActorUtils.activateAkkaSystem()
		 * The context termination calls QActorUtils.terminateTheQActorSystem()
		 */
		ctx = QActorContext.initQActorSystem("ctxSimpleAction", 
 				"./test/it/unibo/qactors/akka/action/simple/noQActorSystemKb.pl", 
//				"./test/it/unibo/qactors/akka/action/simple/actiontimed.pl",
				"./test/it/unibo/qactors/akka/action/simple/sysRules.pl", stdOut);
//		qa = QActorUtils.getQActor("dummyactor");
//		System.out.println("SETIP");
		assertTrue("setUp" , ctx != null );		
 	}
  	
	 @After
	 public void terminate(){
//			if(actionEndHandler!=null) actionEndHandler.terminate();
//   			QActorUtils.terminateTheQActorSystem();
   			ctx.terminateQActorSystem();
	 }

	 /*
  	 * Activate a timed action  
  	 * The action could emit a termination event
  	 * The termination event is perceived by an handler   
  	 */

	 @Test
	public void synchNoToutNoHandler() {
		try {
			System.out.println("		==== synchNoToutNoHandler ====  " );
 			createFiboAction( 41, false ,5000 );
			//Activate the action (callable) in synch mode and waits for termination
 			String result = action.execSynch();	//see AsynchActionGeneric<T>
 			System.out.println("synchNoToutNoHandler  RESULT=" + result );
			assertTrue("synchNoToutNoHandler result" , result.startsWith("fibo(41,val(267914296),exectime("));
 		} catch (Exception e) {
 			System.out.println("synchNoToutNoHandler ERROR=" + e.getMessage());
//			fail("synchNoToutNoHandler" +  e.getMessage());
		}			
  	}
     	@Test
	public void synchNoToutWithHandler() {
		try {			
			System.out.println("		==== synchNoToutWithHandler ====  " );
			createFiboAction( 41, true ,2000 );
			//Activate the action (callable) in synch mode ( waits for termination )
 			String result = action.execSynch();
			System.out.println("synchNoToutWithHandler  RESULT=  " + result );
			assertTrue("synchNoToutWithHandler result" , result.startsWith("fibo(41,val(267914296),exectime("));
			//Check that the event is arrived
			checkTerminationEvent();
 		} catch (Exception e) {
 			System.out.println("synchNoToutWithHandler  ERROR=  " + e.getMessage() );
//			fail("synchNoToutWithHandler" +  e.getMessage());
		}			
  	}
  	
  	
  	@Test
	public void synchWithToutAndHandler() {
		try {
			System.out.println("		==== synchWithToutAndHandler ====  " );
			//Create a termination event (used by the action and the handler)
			createFiboAction( 41, true , 500 );			
			//Activate the action (callable) in synch mode ( waits for termination )
			
// 			AsynchActionGenericResult<String> r1 = 
			action.execSynch();	//see AsynchActionGeneric<T>
			String result = action.getResultRep();
			System.out.println("synchWithToutAndHandler  RESULT=  " + result );
//			assertTrue("synchWithToutAndHandler result" , result.startsWith("fibo(41,val(267914296),timeRemained("));
			//Check that the event is arrived
			checkTerminationToutEvent();
		} catch (Exception e) {
			System.out.println("synchWithToutAndHandler  ERROR=  " + e.getMessage() );
// 			fail("synchWithToutAndHandler" +  e.getMessage());
		}			
  	}

  	@Test
	public void synchWithNOToutAndHandler() {
		try {
			System.out.println("		==== synchWithNOToutAndHandler ====  " );
			//Create a termination event (used by the action and the handler)
			createFiboAction( 41, true , 5000 );			
			//Activate the action (callable) in synch mode ( waits for termination )
			
// 			AsynchActionGenericResult<String> r1 = 
			action.execSynch();	//see AsynchActionGeneric<T>
			String result = action.getResultRep();
			System.out.println("synchWithNOToutAndHandler  RESULT=  " + result );
 			assertTrue("synchWithNOToutAndHandler result" , result.startsWith("fibo(41,val(267914296),exectime("));

			//Check that the event is arrived
 			checkTerminationEvent();
 		} catch (Exception e) {
			System.out.println("synchWithNOToutAndHandler  ERROR=  " + e.getMessage() );
// 			fail("synchWithToutAndHandler" +  e.getMessage());
		}			
  	}
 	
   	@Test
	public void synchWithTout() {
		try {
			System.out.println("		==== synchWithTout ====  " );
			//Create a termination event (used by the action and the handler)
			createFiboAction( 41, true , 100 );			
 			
			//Activate the action (callable) in synch mode and waits for termination
// 			Future<AsynchActionGenericResult<String>> r1 = 
   			action.execSynch();
 			//Check that the event is arrived
			//actionObs_result(fibo(41,val(tout),timeRemained(0)),execTime(103))
			checkTerminationToutEvent();
		} catch (Exception e) {
			System.out.println("synchWithTout  ERROR =  " + e.getMessage() );
//			fail("synchWithTout" +  e.getMessage());
		}	
 		
  	}
 
    	@Test
	public void aSynchNOtout() {
		try {
			System.out.println("		==== aSynchNOtout ====  " );
			//Create a termination event (used by the action and the handler)
			createFiboAction( 41, true , 5000 );			
   // 		Future<AsynchActionGenericResult<String>> r1 = 
 					action.execASynch();  
  			checkTerminationEvent();
 		} catch (Exception e) {
			System.out.println("aSynchNOtout ERROR=" + e.getMessage());
//			fail("aSynchNOtout" +  e.getMessage());
		}			
  	}

  	
/*
 * ============================================================================
 * SUPPORT
 * ============================================================================
 */
  	
  	protected void createFiboAction(int n, boolean withHandler, int maxduration) throws Exception{
		//Create a termination event (used by the action and the handler)
		terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		//Create an observable action with  event handler
		action = createFiboTimedAction( n, stdOut, maxduration  );
		if( withHandler ){
			//Create the action termination handler
			actionEndHandler = createActionTerminationEvHandler(terminationEvId);  	
			assertTrue("handlerTerminate" , actionEndHandler != null );
		}
  	}
  	
	public IActorAction  createFiboTimedAction(  
			int n, IOutputEnvView outEnvView, int maxduration)
			throws Exception {
 		boolean canresume = false;
		String name = QActorUtils.getNewName("fibo_");		
		return  new ActionFibonacciTimed( name, qa, ctx, n,
				canresume, terminationEvId, alarms, outEnvView, maxduration);		
 	}
 
  	protected QActor createActionTerminationEvHandler( String evid ) throws Exception{
  	 	String name =  QActorUtils.getNewName("evh_");
 	 	String[] alarmIds = new String[]{evid};
//	 	ActorRef aref = 
	 			ctx.getAkkaContext().actorOf( Props.create(
	 					Class.forName("it.unibo.qactors.akka.action.simple.ActionObsEvh"), 
	 					name, ctx,  stdOut, alarmIds), name );	
	 	/*
	 	* The action must be activated AFTER that the ActionObsEvh has been registered
	 	*/
	 	return QActorUtils.waitUntilQActorIsOn(name); 	 
 	}
  	
  	protected void checkTerminationEvent() throws Exception{
		IEventItem ev = actionEndHandler.waitForCurentEvent();
		String result = ev.getMsg();
		assertTrue("checkTerminationEvent" , 
					ev.getEventId().equals(terminationEvId)
					&& result.startsWith("actionObs_result(fibo(41,val(267914296),exectime("));  		
  	}
  	protected void checkTerminationToutEvent() throws Exception{
		IEventItem ev = actionEndHandler.waitForCurentEvent();
		String result = ev.getMsg();
 		assertTrue("checkTerminationEvent" , 
					ev.getEventId().equals(terminationEvId)
					&& result.startsWith("actionObs_result(fibo(timeOut("));  		
  	}

}
