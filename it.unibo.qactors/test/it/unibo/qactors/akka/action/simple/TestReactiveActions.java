package it.unibo.qactors.akka.action.simple;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import akka.actor.ActorSelection;
import alice.tuprolog.Prolog;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionSolveTimed;
import it.unibo.qactors.action.ActionFibonacciTimed;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

 
/*
 * ==========================================================
 * ACTIONS as AsynchActionGeneric (timed)
 * 	execSynch / execAsych 
 * ==========================================================
 */
public class TestReactiveActions  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private QActorContext ctx ;
 	 private String goalTodo = "fibo(41,V)";
 	 private String terminationEvId = "endSolveFibo";
 	 private String answerEvId ="";
 	 private IActorAction  action;
  	 private QActor actionEndHandler;	 
  	 private String alarm    = QActorUtils.locEvPrefix+"alarm";
  	 private String obstacle = QActorUtils.locEvPrefix+"obstacle";	 
  	 private String[] alermEvs = new String[]{alarm,obstacle};
  	 private IEventItem evActionTerminate;
  	 private Prolog pengine;
  	 private QActor qa;
  	 /*
	 *  
 	 */
	@Before
	public void setUp() throws Exception{
		//Create a system without qactors
		ctx = QActorContext.initQActorSystem("ctxactiontimed", 
				"./test/it/unibo/qactors/akka/action/simple/actiontimed.pl",
				"./test/it/unibo/qactors/akka/action/simple/sysRules.pl", stdOut);
		assertTrue("setUp" , ctx != null );		
		qa = QActorUtils.getQActor("dummyactor");
		assertTrue("solveReference" , qa != null);			
		pengine = qa.getPrologEngine();
		assertTrue("setUp" , pengine != null);
 	}
	
	
	/*
	 * Many actions working with the same pengine solve a goal in 3 different ways
	 *  - ending with success
	 * 	- interrupted by an alarm
	 *  - interrupted by a time out
	 *  - ending with a failure
	 */
  	@Test
	public void solveReactive() { 
 		try{
			int numOfActions = 3; 
 			
			/*
			 * ------------------------------------------------------------
			 * 1) Actions that terminate with success 
			 * 		each action instance emits the event terminationEvId ("endSolveFibo")
			 * 		the testing operation continues when numOfActions events have been received
			 * ------------------------------------------------------------
			 */
			System.out.println("		==== Actions that terminate with success ====  " );
			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
			/*
 			 * Activate  many actions with the same behavior and the same termination event
 			 * WARNING: all the actions are working with the same pengine
 			 */			
			for( int i=1; i<=numOfActions; i++ ){
				action = new ActionSolveTimed(
						"solve"+i, qa, ctx, pengine, "fibo(25,V)",
						terminationEvId,alermEvs, stdOut, 5000);
				action.execASynch(); 
			}
			//wait for the last (numOfActions) termination event by tracing the previous ones
 			evActionTerminate = actionEndHandler.waitForLastEvent(numOfActions,true);
 			System.out.println("solveReactive evActionTerminate=" + evActionTerminate .getDefaultRep());
   			assertTrue("solveReactive1 ", 
  					evActionTerminate.getMsg().startsWith("actionObs_result(fibo(25,75025),execTime(")   );
//  			actionEndHandler.terminate();	//terminates also the system

  			/*
			 * ------------------------------------------------------------
			 * 2) Actions that terminate interrupted by an alarm
			 * 		each action instance is interrupted by the event alarm  
			 * 		the testing operation continues when numOfActions alarm events have been received
			 * ------------------------------------------------------------
			 */
			System.out.println("		==== Actions that terminate interrupted ====  " );
  			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
			System.out.println("		==== Actions actionEndHandler ====  " );
			for( int i=1; i<=numOfActions; i++ ){
				action = new ActionSolveTimed(
						"solve"+i, qa, ctx, pengine, "fibo(28,V)",
						terminationEvId,alermEvs, stdOut, 8000);
				System.out.println("		==== CREATED ACTION ====  " + action.getName() );
				action.execASynch(); 
			}
 			/*
 			 * Emit an alarm event while the actions are running
 			 */
			QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 400);		//TO CHECK	
 			//wait for the last (numOfActions) interruption (alarm)  event by tracing the previous ones
 			evActionTerminate = actionEndHandler.waitForLastEvent(numOfActions,true);
 			System.out.println("solveReactive evActionTerminate=" + evActionTerminate .getDefaultRep());
   			assertTrue("solveReactive2 ", 
  					evActionTerminate.getMsg().startsWith("actionObs_result(interrupted(local_alarm),execTime(")   );			
//   			actionEndHandler.terminate();
			/*
			 * ------------------------------------------------------------
			 * 3) Actions that terminate with a time out
			 * 		each action instance ends with a timeout 
			 * 		the testing operation continues when numOfActions timeout events have been received
			 * ------------------------------------------------------------
			 */
 			System.out.println("		==== Actions that terminate with tout ====  " );
    		terminationEvId = QActorUtils.getNewName("end");
    		actionEndHandler = 
					QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
   			for( int i=1; i<=numOfActions; i++ ){  
				action = new ActionSolveTimed(
						"solveTout"+i, qa, ctx, pengine, "fibo(28,V)",
						terminationEvId,alermEvs, stdOut, 300);
//				System.out.println("solveReactive actions activated " + "solveTout"+i  );			
				action.execASynch(); 
			}
   			//wait for the last (numOfActions) interruption (alarm)  event by tracing the previous ones
  			evActionTerminate = actionEndHandler.waitForLastEvent(numOfActions,true);
 			System.out.println("solveReactive evActionTerminate=" + evActionTerminate .getDefaultRep());
 			assertTrue("solveReactive3 ", 
 					evActionTerminate.getMsg().startsWith("actionObs_result(timeOut(")   );

// 			actionEndHandler.terminate();
			
			/*
			 * ------------------------------------------------------------
			 * 4) Actions that terminate with a failure
			 * 		each action instance ends with a failure 
			 * 		the testing operation continues when numOfActions timeout events have been received
			 * ------------------------------------------------------------
			 */
			System.out.println("		==== Actions that terminate with failure ====  " );			
	   		terminationEvId = QActorUtils.getNewName("end");
 			actionEndHandler = 
					QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
  			for( int i=1; i<=numOfActions; i++ ){  
//	 			System.out.println("solveReactive actions activated " + "solveKo"+i  );			
				action = new ActionSolveTimed(
						"solveKo"+i, qa, ctx, pengine, "fibbbo(28,V)",
						terminationEvId,alermEvs, stdOut, 8000);
				action.execASynch(); 
			}
			/*
 			 * wait for the last (numOfActions) interruption (alarm)  event by tracing the previous ones
 			 * WARNING: the action could be so fast that the current event in the handler
 			 * could be overwritten, In this case some event could not be trace.
 			 * However, the number of events is correctly updated
 			 */
			evActionTerminate = actionEndHandler.waitForLastEvent(numOfActions,true);
 			System.out.println("solveReactive evActionTerminate=" + evActionTerminate .getDefaultRep());
 			assertTrue("solveReactive4 ", 
 					evActionTerminate.getMsg().startsWith("actionObs_result(failure,execTime(")   );
  		} catch (Exception e) {
			System.out.println("solveReactive ERROR=" + e.getMessage());
// 			fail("solveReactive " + e.getMessage()   );
		}		
   		
   	}
	
   	
/*
 * ============================================================================
 */
  	protected void createFiboAction( int maxduration, boolean withHandler ) throws Exception{
		//Create a termination event (used by the action and the handler)
		terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		action = createReactiveFiboAction( goalTodo, maxduration  );
		if( withHandler ){
			//Create the action termination handler
			actionEndHandler = 
					QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
 			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx, actionEndHandler.getName());
			assertTrue("createFiboAction" , asel != null );
		}
  	}
	
 	public IActorAction createReactiveFiboAction(  String goalTodo, int maxduration )
			throws Exception {
		IActorAction action;
		boolean canresume = false;
		String name = QActorUtils.getNewName("fibo_");	
 		Struct st = (Struct) Term.createTerm(goalTodo);
		int n = Integer.parseInt( ""+st.getArg(0) );
		action = new ActionFibonacciTimed(name, qa, ctx, n,
				canresume, terminationEvId,  alermEvs, stdOut, maxduration);		
		return action;
	}
 	
 	protected void checkTerminationEvent(QActor actionTerminationHandler, String terminationEvId, String prefix) throws Exception{
		IEventItem ev = actionTerminationHandler.waitForCurentEvent();
		String result = ev.getMsg();
		assertTrue("checkTerminationEvent" , ev.getEventId().equals(terminationEvId)
					&& result.startsWith(prefix));  		
  	}

}
