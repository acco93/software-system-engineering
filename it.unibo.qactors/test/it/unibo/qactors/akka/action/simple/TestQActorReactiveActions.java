package it.unibo.qactors.akka.action.simple;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import akka.actor.ActorSelection;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionDummyTimed;
import it.unibo.qactors.action.ActionFibonacciTimed;
import it.unibo.qactors.action.ActionUtil;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

 
/*
 * ==========================================================
 * ACTIONS as AsynchActionGeneric (timed)
 * 	execSynch / execAsych 
 * ==========================================================
 */
public class TestQActorReactiveActions  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private QActorContext ctx ;
// 	 private String goalTodo = "fibo(41,V)";
 	 private String terminationEvId = "endSolveFibo";
  	 private IActorAction  action;
  	 private QActor actionEndHandler;	 
  	 private String alarm    = QActorUtils.locEvPrefix+"alarm";
  	 private String obstacle = QActorUtils.locEvPrefix+"obstacle";	 
  	 private String[] alermEvs = new String[]{alarm,obstacle};
  	 private QActor qa;
  	 /*
	 *  
 	 */
	@Before
	public void setUp() throws Exception{
		//Create a system without qactors
		ctx = QActorContext.initQActorSystem("ctxactiontimed", 
				"./test/it/unibo/qactors/akka/action/simple/actiontimed.pl", 
				"./test/it/unibo/qactors/akka/action/simple/sysRules.pl", stdOut);
		assertTrue("setUp" , ctx != null );		
		qa = QActorUtils.getQActor("dummyactor");
		assertTrue("solveReference" , qa != null);			
//		pengine = qa.getPrologEngine();
//		assertTrue("setUp" , pengine != null);
 	}
	
	 @After
	 public void terminate(){
//			actionEndHandler.terminate();
// 			qa.terminate();
//			QActorUtils.terminateTheQActorSystem();
			ctx.terminateQActorSystem();
	 }
	
	
	/*
	 * Many actions working with the same pengine solve a goal in 3 different ways
	 *  - ending with success
	 * 	- interrupted by an alarm
	 *  - interrupted by a time out
	 *  - ending with a failure
	 */
// 	@Test
	public void testSoundNoAlarm() {	//TO CHECK INTERFERENCE
		try {
			System.out.println(" **** testSoundNoAlarm  *** "  );
 		    terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
 		    action = ActionUtil.buildSoundActionTimed(qa,ctx,
				stdOut,3000,terminationEvId,"audio/computer_complex3.wav", new String[]{});		   
			actionEndHandler = QActorUtils.createActionTerminationEvHandler(
							ctx, stdOut,"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
		    action.execSynch();
		    System.out.println(" testSoundNoAlarm result= " + action.getResultRep() );
//		    checkTerminationEvent(actionEndHandler,terminationEvId,
//		    		"actionObs_result(playsound(timeOut(2946),timeRemained(");		   
		} catch (Exception e) {
			System.out.println("testSoundNoAlarm ERROR=" + e.getMessage());
//			fail("testSoundNoAlarm" +  e.getMessage());
		}	
	}
	
	@Test
	public void testSoundWithAlarm() {	//TO CHECK asynch reactive
		try { 
			System.out.println(" **** testSoundWithAlarm  *** "  );
 		    terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
 		    action = ActionUtil.buildSoundActionTimed(qa,ctx,
				stdOut,3000,terminationEvId,"audio/computer_complex3.wav", alermEvs);		   
			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh",terminationEvId);
		    action.execASynch();		    
		    QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 500);		    
		    System.out.println(" testSoundWithAlarm result= " + action.getResultRep() );
		    checkTerminationEvent(actionEndHandler,terminationEvId,
		    		"actionObs_result(playsound(interrupted(local_alarm),timeRemained(");		   
		} catch (Exception e) {
			System.out.println("testSoundWithAlarm ERROR=" + e.getMessage());
//			fail("testSoundWithAlarm" +  e.getMessage());
		}	
	}
	
	@Test
	public void testDummyNoAlarm() {	
		try {
			System.out.println(" **** testDummyNoAlarm  *** "  );
 		    terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
 		    IActorAction action  = new ActionDummyTimed( "dummyAction",qa, ctx, terminationEvId, alermEvs, stdOut, 1000 );   		   
			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh",terminationEvId);
		    action.execSynch();
		    System.out.println(" testDummyNoAlarm result= " + action.getResultRep() );
		    checkTerminationEvent(actionEndHandler,terminationEvId,
		    		"actionObs_result(dummyAction(1000,timeOut(");		   
		} catch (Exception e) {
			System.out.println("testDummyNoAlarm ERROR=" + e.getMessage());
//			fail("testDummyNoAlarm" +  e.getMessage());
		}	
	}

	@Test
	public void testDummyWithAlarm() {	
		try {
			System.out.println(" **** testDummyWithAlarm  *** "  );
 		    terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
 		    IActorAction action  = new ActionDummyTimed( "dummyAction", qa,ctx, terminationEvId, alermEvs, stdOut, 3000 );    		   
			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh",terminationEvId);
		    action.execASynch();
		    QActorUtils.emitEventAfterTime(qa,"tester", alarm, "fire", 500);		    
		    System.out.println(" testDummyWithAlarm result= " + action.getResultRep() );
		    checkTerminationEvent(actionEndHandler,terminationEvId,
		    		"actionObs_result(dummyAction(3000,interrupted(local_alarm),timeRemained(");		   
		} catch (Exception e) {
			System.out.println("testDummyWithAlarm ERROR=" + e.getMessage());
//			fail("testDummyWithAlarm" +  e.getMessage());
		}	
	}
	
	
   	@Test
	public void fiboReactToAlarm() {
		try {
			System.out.println(" **** fiboReactToAlarm  *** "  );
			terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
    		boolean withHandler  = true;			
   			createFiboAction(  41, 5000,  withHandler);			
   			action.execASynch();   
			System.out.println("fiboReactToAlarm execASynch "  );

			QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 500);
 			
 			IEventItem evActionTerminate = actionEndHandler.waitForCurentEvent();
// 			System.out.println("reactToAlarm evActionTerminate=" + evActionTerminate.getDefaultRep());
 			assertTrue("fiboReactToAlarm " , evActionTerminate.getMsg().contains("interrupted(local_alarm)"));		
		} catch (Exception e) {
			System.out.println("fiboReactToAlarm ERROR=" + e.getMessage());
//			fail("fiboReactToAlarm" +  e.getMessage());
		}	
	}

  	@Test
	public void alarmAfterActionEnd() {
		try {
			terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
    		boolean withHandler  = true;		
   			createFiboAction(  41, 2000,  withHandler);   			
   			action.execASynch();  
 			//Check  the termination event 
 			IEventItem evActionTerminate = actionEndHandler.waitForCurentEvent();
 			System.out.println("alarmAfterActionEnd evActionTerminate=" + evActionTerminate.getDefaultRep());
   			assertTrue("reactToAlarm " , evActionTerminate.getMsg().contains("actionObs_result(fibo(41,val(267914296),exectime(")   );
			QActorUtils.raiseEvent(ctx,"tester", alarm, "fire");
			System.out.println("alarmAfterActionEnd raiseEvent "  );
			/*
			 * Wait for a while to see tha end of the timer that is still working
			 * since we do not (at the moment) interrupt an akka actor
			 */
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println("alarmAfterActionEnd ERROR=" + e.getMessage());
//			fail("alarmAfterActionEnd" +  e.getMessage());
		}	
	}
   	
   	
  
  	
/*
 * ============================================================================
 */
  	protected void createFiboAction( int n, int maxduration, boolean withHandler ) throws Exception{
		//Create a termination event (used by the action and the handler)
		terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		action = createReactiveFiboAction( n, maxduration  );
		if( withHandler ){
			//Create the action termination handler
			actionEndHandler = 
					QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
							"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
 			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx, actionEndHandler.getName());
			assertTrue("createFiboAction" , asel != null );
		}
  	}
	
 	public IActorAction createReactiveFiboAction(  int n, int maxduration )
			throws Exception {
		IActorAction action;
		boolean canresume = false;
		String name = QActorUtils.getNewName("fibo_");		
		action = new ActionFibonacciTimed(name, qa, ctx, n,
				canresume, terminationEvId,  alermEvs, stdOut, maxduration);		
		return action;
	}
 	
 	protected void checkTerminationEvent(QActor actionTerminationHandler, String terminationEvId, String prefix) throws Exception{
		IEventItem ev = actionTerminationHandler.waitForCurentEvent();
		String result = ev.getMsg();
		assertTrue("checkTerminationEvent" , ev.getEventId().equals(terminationEvId)
					&& result.startsWith(prefix));  		
  	}

}
