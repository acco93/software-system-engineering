package it.unibo.qactors.akka.action.simple;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.util.concurrent.Future;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import akka.actor.ActorSelection;
import akka.actor.Props;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IObservableActionGeneric;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

/*
 * ==========================================================
 * ACTIONS as ActionObservableGeneric 
 * ==========================================================
 */

public class TestActionObservable  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private QActorContext ctx ;
	 private String goalTodo ;
	 String terminationEvId = "";
	 
	 IObservableActionGeneric<String> action;
	 QActor actionEndHandler; 
	 
	/*
	 *  
 	 */
	@Before
	public void setUp() throws Exception{
		//Create a system without qactors
		ctx = QActorContext.initQActorSystem("ctxSimpleAction", 
				"./test/it/unibo/qactors/akka/action/simple/noQActorSystemKb.pl", 
				"./test/it/unibo/qactors/akka/action/simple/sysRules.pl", stdOut);
		assertTrue("setUp" , ctx != null);		
		//Create a goal to be solved by fibonacci action
		goalTodo = "fibo(30,V)";
	}
	
	 @After
	 public void terminate(){
			if(actionEndHandler!=null) actionEndHandler.terminate();
//  			QActorUtils.terminateTheQActorSystem();
  			ctx.terminateQActorSystem();
	 }

  	/*
  	 * Activate an action in synch way 
  	 * The action will emit a termination event but no handler is defined
	 * The control is returned when the action is terminated
   	 */
  	@Test
	public void synchTest() {
		try {
 			System.out.println("===== synchTest ==="); 			
			//Create an observable action without  event handler
			creatFiboeAction(false);
 			String result = action.execSynch();
 			System.out.println( "	*** synchTest result" + result );
 			assertTrue("synchTest " , result.equals("fibo(30,1346269)"));
		} catch (Exception e) {
			System.out.println("synchTest ERROR " + e.getMessage());
//			fail("synchTest " + e.getMessage() );
		}		
	}
  	
  	/*
  	 * Activate an action in asynch way via activate (Callable)
  	 * The action will emit a termination event
  	 * The termination event is perceived by an handler   
  	 */
   	@Test
	public void aSynchTest() {
		try {
			System.out.println("===== aSynchTest ==="); 			
			//Create a termination event (used by the action and the handler)
			creatFiboeAction(true); 						
			//Activate the action (callable) without waiting for termination
 			Future<String> future = action.execASynch();
			//Check that the event is arrived
			checkTerminationEvent();
		} catch (Exception e) {
			System.out.println("aSynchTest ERROR " + e.getMessage());
//			fail("aSynchTest " + e.getMessage() );
		}				
	}
 	
  	/*
  	 * Activate an action that emits a termination event in synch way 
	 * The control is returned when the action is terminated
	 * The termination event is perceived by an handler  
   	 */
//   	@Test 
	public void synchWithHandler() { //TODO registerForEvent too early
		try {
			System.out.println("===== synchWithHandler ==="); 			
			//Create a termination event (used by the action and the handler)
			creatFiboeAction(true); 			
			//Activate the action (callable) in synch mode and waits for termination
			String result = action.execSynch();
			assertTrue("synchWithHandler " , result.equals("fibo(30,1346269)"));			
			//Check that the event is arrived
			checkTerminationEvent();
 			result = action.getResultRep();	
			System.out.println( "	*** synchWithHandler result=" + result );
			assertTrue("synchWithHandler " , result.startsWith("fibo(30,1346269,execTime("));
		} catch (Exception e) {
			System.out.println("synchWithHandler ERROR " + e.getMessage());
//			fail("synchWithHandler " + e.getMessage() );
		}		
	}

  	@Test
	public void handlerTerminate() {
		try {
			System.out.println("===== handlerTerminate ==="); 			
			//Create an  action with  event handler
			creatFiboeAction(true);			
			//Activate the action (callable) without waiting for termination
			action.execASynch();
			//Check that the event is arrived
			checkTerminationEvent();				
			String result = action.getResultRep();
			System.out.println( "	*** handlerTerminate result" + result );
			//TERMINATE the handler
    		actionEndHandler.terminate();
  			
    		System.out.println("===== EXECUTE ANOTHER ACTION with the same termination event and NO handler ==="); 			
			action =  createFiboAction( goalTodo, stdOut   );
			action.execSynch();
			result = action.getResultRep();
			assertTrue("handlerTerminate " , result.startsWith("fibo(30,1346269,execTime("));
			//Check that the event handler does not exists anymore
			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx, actionEndHandler.getName());
			assertTrue("handlerTerminate" , asel == null ); 
		} catch (Exception e) {
			System.out.println("handlerTerminate ERROR " + e.getMessage());
//			fail("handlerTerminate " + e.getMessage() );
		}				
	}
 
  	/*
  	 * =======================================================
  	 * SUPPORT
  	 * =======================================================
 	 */
  	
  	protected void creatFiboeAction(boolean withHandler) throws Exception{
		//Create a termination event (used by the action and the handler)
		terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		//Create an observable action with  event handler
		action = createFiboAction( goalTodo, stdOut  );
		if( withHandler ){
			//Create the action termination handler
			actionEndHandler = createActionTerminationEvHandler();  	
			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx, actionEndHandler.getName());
			assertTrue("handlerTerminate" , asel != null );
		}
  	}
  	
  	protected IObservableActionGeneric<String> createFiboAction(
			String goalTodo, IOutputEnvView outEnvView ) throws Exception{
 		String name = QActorUtils.getNewName("fibo_");
 		IObservableActionGeneric<String> action = 
 				new ActionObservableFibonacci( name, ctx, goalTodo,  terminationEvId, outEnvView   ); 
	 	return action;
	}
 	
 	protected QActor createActionTerminationEvHandler(  ) throws Exception{
  	 	String name =  QActorUtils.getNewName("evh_");
 	 	String[] alarmIds = new String[]{terminationEvId};
//	 	ActorRef aref = 
	 			ctx.getAkkaContext().actorOf( Props.create(
	 					Class.forName("it.unibo.qactors.akka.action.simple.ActionObsEvh"), 
	 					name, ctx,  stdOut, alarmIds), name );	
	 	/*
	 	* The action must be activated AFTER that the ActionObsEvh has been registered
	 	*/
	 	return QActorUtils.waitUntilQActorIsOn(name); 	 
 	}

  	protected void checkTerminationEvent() throws Exception{
		IEventItem ev = actionEndHandler.waitForCurentEvent();
		String result = ev.getMsg();
		assertTrue("checkTerminationEvent" , 
					ev.getEventId().equals(terminationEvId)
					&& result.startsWith("actionObs_result(fibo(30,1346269),execTime("));  		
  	}
 	
}
