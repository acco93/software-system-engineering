%==============================================
% USER DEFINED
% system configuration: actionSystemKb.pl
%==============================================
context( ctxAction, "localhost", "TCP", "8070" ).
 
qactor( qaplayer, ctxAction, "it.unibo.qactors.akka.action.QActorActionSound" ).
qactor( qaclient, ctxAction, "it.unibo.qactors.akka.action.QActorClient" ).
  