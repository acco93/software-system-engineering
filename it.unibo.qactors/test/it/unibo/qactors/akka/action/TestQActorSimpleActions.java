package it.unibo.qactors.akka.action;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;


public class TestQActorSimpleActions  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private String alarm    = QActorUtils.locEvPrefix+"alarm";
	 private String obstacle = QActorUtils.locEvPrefix+"obstacle";	 
	 private String alermEvs = alarm+","+obstacle;
	 private QActorContext ctx;
	 private QActor actionEndHandler;
	 private QActor qa;
	 
	/*
	 * A system in which:
	 * TODO
 	 */
		@Before
		public void setUp() throws Exception{
			ctx = QActorContext.initQActorSystem("ctxSimple", 
					"./test/it/unibo/qactors/akka/action/actionSimpleSystemKb.pl", 
					"./test/it/unibo/qactors/akka/action/sysRules.pl",stdOut,null,true);
			assertTrue("setUp" , ctx != null );		
			qa = QActorUtils.getQActor("qaworker");
			assertTrue("setUp" , qa != null);			
//			pengine = qa.getPrologEngine();
//			assertTrue("setUp" , pengine != null);
		}
		 @After
		 public void terminate(){
			ctx.terminateQActorSystem();
		 }

  	
   
		@Test 
		public void synchExec() {
			try {
				System.out.println("		==== synchExec ====  " );			
				String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
	 			String recoveryPlans = "";
	 			AsynchActionResult aar = 
					qa.fibo(ActionExecMode.synch, 30, terminationEvId, 5000, alermEvs, recoveryPlans);
	 			System.out.println("synchExec aar=" + aar.getResult()  );
 	 			assertTrue("synchExec",
 	 					 aar.getResult().startsWith("fibo(30,val(1346269),exectime("));	 			
 			} catch (Exception e) {
				System.out.println("synchExec ERROR:"+e.getMessage());
// 				fail("synchExec" + e.getMessage() );
			}		
		}
 		
 		@Test
		public void synchReact() {
			try {
 				System.out.println("		==== synchReact ====  " );			
	 			String recoveryPlans = "";
				String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
				
	 			QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 600);
	 			
	 			AsynchActionResult aar = 
					qa.fibo(ActionExecMode.synch, 41, terminationEvId, 5000, alermEvs, recoveryPlans);
 				
	 			System.out.println("synchReact aar=" + aar.getResult()   );
   	 			assertTrue("", aar.getResult().startsWith("fibo(interrupted(local_alarm),exectime("));
  			} catch (Exception e) {
 				System.out.println("synchReact ERROR:"+e.getMessage());
// 				fail("synchReact" + e.getMessage() );
			}		
		}
		
  		@Test
		public void asynchReact() {
			try {
				System.out.println("		==== asynchReact ====  " );			
	 			String recoveryPlans = "";
				String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);				
				actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
						"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
				QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 600);
 	 			AsynchActionResult aar = 
					qa.fibo(ActionExecMode.asynch, 41, terminationEvId, 5000, alermEvs, recoveryPlans);
 				
	 			System.out.println("asynchReact aar=" + aar   );
	 			String result = aar.getResult();
	 			assertTrue("asynchReact",result.startsWith("fibo(41,val(going),exectime("));
	 			
  	 			checkEvent(actionEndHandler, terminationEvId, "actionObs_result(fibo(interrupted(local_alarm),exectime(");
			} catch (Exception e) {
 				System.out.println("asynchReact ERROR:"+e.getMessage());
// 				fail("asynchReact" + e.getMessage() );
			}		
		}
  		@Test
		public void asynchReactAgain() {
			try {
				System.out.println("		==== asynchReactAgain ====  " );			
	 			String recoveryPlans = "";
				String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);				
				actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
						"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
				QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 600);
 	 			AsynchActionResult aar = 
					qa.fibo(ActionExecMode.asynch, 41, terminationEvId, 5000, alermEvs, recoveryPlans);
 				
	 			System.out.println("asynchReactAgain aar=" + aar   );
	 			String result = aar.getResult();
	 			assertTrue("asynchReactAgain",result.startsWith("fibo(41,val(going),exectime("));
	 			
  	 			checkEvent(actionEndHandler, terminationEvId, "actionObs_result(fibo(interrupted(local_alarm),exectime(");
			} catch (Exception e) {
 				System.out.println("asynchReactAgain ERROR:"+e.getMessage());
// 				fail("asynchReactAgain" + e.getMessage() );
			}		
		}		
// 		@Test
		public void noPlansForASynchReactive() {
			try {
				System.out.println("		==== noPlansForASynchReactive ====  " );			
  				String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
	 			String recoveryPlans = "plan1";
 				qa.fibo(ActionExecMode.asynch, 30, terminationEvId, 5000, alermEvs, recoveryPlans);
	 			fail("noPlansForASynchReactive" );	 			
 			} catch (Exception e) {
 				System.out.println("noPlansForASynchReactive WARNING:" + e.getMessage());
 				assertTrue("noPlansForASynchReactive" , e!=null );
			}		
		}
		
//		@Test
		public void asynchReactContinue() {
			try {
				System.out.println("		==== asynchReactContinue ====  " );			
	 			String recoveryPlans = "continue";
				String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);				
				actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
						"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
				QActorUtils.emitEventAfterTime(ctx,"tester", alarm, "fire", 600);
 	 			AsynchActionResult aar = 
					qa.fibo(ActionExecMode.asynch, 41, terminationEvId, 5000, alermEvs, recoveryPlans);
 				
	 			System.out.println("asynchReactContinue aar=" + aar   );
	 			String result = aar.getResult();
	 			assertTrue("asynchReactContinue",result.startsWith("ar(fibo(41,val(going),exectime("));	 			
  	 			checkEvent(actionEndHandler, terminationEvId, 
  	 					"actionObs_result(fibo(interrupted(local_alarm),timeRemained(");
			} catch (Exception e) {
 				System.out.println("asynchReactContinue ERROR:"+e.getMessage());
// 				fail("asynchReactContinue" + e.getMessage() );
			}		
		}

//  	 	@Test
		public void askRunAsynch() {
			try {
				System.out.println("		==== askRunAsynch ====  " );
 	 			String result = qa.askMessageWaiting("qaworker", "playAsynchronous", 500);
	 			System.out.println("	*** result=" + result);
	 			assertTrue("askRunAsynch", result.equals("done_playAsynchronous") );
 			} catch (Exception e) {
				System.out.println("askRunAsynch ERROR " + e.getMessage());
//				fail("askRunAsynch " + e.getMessage() );
			}		
		}

// 	@Test
	public void workerActionTest() {
		try {
			System.out.println("		==== workerActionTest ====  " );			
			String result = qa.askMessageWaiting("qaworker", "playSynchronous", 500);
 			System.out.println("	*** result=" + result);
 			assertTrue("workerActionTest", result.equals("done_playSynchronous") );
 			
			String recoveryPlans = "";
// 			qa.playSound("audio/computer_complex3.wav", ActionExecMode.synch, 3000, alermEvs, recoveryPlans);
 			
			String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
			actionEndHandler = QActorUtils.createActionTerminationEvHandler(ctx, stdOut, 
					"it.unibo.qactors.akka.action.simple.ActionObsEvh", terminationEvId);
			AsynchActionResult aar = 
 					qa.fibo(ActionExecMode.synch, 30, terminationEvId, 3000, alermEvs, recoveryPlans);
 
 			System.out.println("aar= " + aar  );
// 			System.out.println("aar event=" +  aar.getEvent() );	 			
 			assertTrue("aar event", aar.getEvent()==null);
// 			System.out.println("aar goon=" +  aar.getGoon() );
	 			assertTrue("aar goon",   aar.getGoon() );
// 			System.out.println("aar interrupted =" +  aar.getInterrupted() );
 			assertTrue("aar interrupted",   ! aar.getInterrupted() );
// 			System.out.println("aar time remained=" +  aar.getTimeRemained() ) ;
 			assertTrue("aar timeremained",  aar.getTimeRemained() >= 2000   ); 			
  			checkEvent(actionEndHandler,terminationEvId,"actionObs_result(fibo(30,val(1346269),exectime(");
			
  		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
			fail("workerActionTest " + e.getMessage() );
		}		
	}

 	/*
 	 * UTILITEIS
 	 */
	
  
 	protected void checkEvent(QActor evh, String evId, String prefix) throws Exception{
 		if( evh == null ) return;
		IEventItem ev = evh.waitForCurentEvent();
		String result = ev.getMsg();
		assertTrue("checkEvent" , ev.getEventId().equals(evId) && result.startsWith(prefix));  		
  	}
	

}
