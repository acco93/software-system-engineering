/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.akka.action;
import java.util.Hashtable;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionSoundTimed;
import it.unibo.qactors.action.ActionFibonacciTimed;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.akka.QActor;
import it.unibo.qactors.akka.QActorActionUtils;

public class QActorWorker extends QActor{
	private ActionFibonacciTimed action ;
	
	public QActorWorker(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, outEnvView);

 	}



	/*
	 * 1) ask to play a short sound and waits until the sound  ends 
	 * 2) ask play a music in asynchronous  way
	 * 3) while the music is playing ask to play a message in synchronous way 
	 */
	protected void doJob() throws Exception {
 	}
	

	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );		
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg)  {
		try{
 				if( msg.msgType().equals(QActorContext.request) ){
					if( msg.msgId().equals("request") ){
						String msgContent = msg.msgContent();  
						String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
						if(msgContent.equals("playAsynchronous") ){
							fibo(ActionExecMode.asynch, 30, terminationEvId, 3000, "", "");
//							playSound("audio/music_dramatic20.wav",  ActionExecMode.asynch, 6000, "", "" ); 	 
							//send the answer
							getSender().tell("done_playAsynchronous", getSelf());
		 				}else if(msgContent.equals("playSynchronous") ){
		 					fibo(ActionExecMode.synch, 30, terminationEvId, 3000, "", "");
//		 					playSound("audio/computer_complex3.wav", ActionExecMode.synch, 3000, "", "" );	
							//send the answer
							getSender().tell("done_playSynchronous", getSelf());
		 				}
					} // 	
				}//request 
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}
}
