package it.unibo.qactors.akka.actor;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import alice.tuprolog.Var;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

 

public class TestQActors  {
 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
 private String alarm    = QActorUtils.locEvPrefix+"alarm";
 private String obstacle = QActorUtils.locEvPrefix+"obstacle";	 
 private String[] alermEvs = new String[]{alarm,obstacle};
 private String alermEvsStr = alarm+","+obstacle; 
 private QActorContext ctx;
 private QActor qa;
 private Prolog pengine ;
 
	/*
	 * A system in which we use a QActor like an object
	 * in order to test some important operations in the WorldTheory
	 */
 
 @Before
 public void setUp(){
		try {
 			ctx = QActorContext.initQActorSystem("ctxactortest", 
					"./test/it/unibo/qactors/akka/actor/actorTestSystemKb.pl", 
					"./test/it/unibo/qactors/akka/actor/sysRules.pl",
					stdOut,null,true);
			assertTrue("setUp" , ctx != null);
 			qa = QActorUtils.getQActor("qatester");
			assertTrue("setUp" , qa != null);		
			pengine = qa.getPrologEngine();
			assertTrue("setUp" , pengine != null);
		} catch (Exception e) {
			System.out.println("setUp ERROR " + e.getMessage());
			fail("setUp " + e.getMessage()   );
		}			 
 }
 @After
 public void terminate(){
  			ctx.terminateQActorSystem();
  }
   
  	@Test
	public void testWorldTheory() {
		try {
			System.out.println(" *** testWorldTheory *** ");
			qa.addRule("a(1)");
			qa.addRule("a(2)");
			qa.addRule("a(3)");			
			SolveInfo sol = QActorUtils.solveGoal("a(X)", pengine);
			assertTrue("testWorldTheory" , sol.isSuccess());			
			System.out.println("testWorldTheory FIRST sol=" + sol.getSolution() );			
			Term tt = sol.getVarValue("X");
			System.out.println("testWorldTheory X=" + tt );
			assertTrue("testWorldTheory" , tt.toString().equals("1"));			
			while( pengine.hasOpenAlternatives() ){
				sol = pengine.solveNext();
				assertTrue("testWorldTheory" , sol.isSuccess());
				System.out.println("testWorldTheory OTHER sol=" + sol.getSolution() );
			}
		} catch (Exception e) {
			System.out.println("testWorldTheory ERROR " + e.getMessage());
//			fail("testWorldTheory " + e.getMessage()   );
		}		
	}

    	@Test
	public void testGuards() {
		try {
		System.out.println(" *** testGuards *** ");		
		qa.addRule( "soundFile('./audio/any_commander3.wav',3000)" );  
		List<Var> guardVars = QActorUtils.evalTheGuard( qa, "soundFile(F,T)", QActorUtils.guardVolatile);		
		if( guardVars != null ){
			String fname = "";
			String time  = "";
			Iterator<Var> iter = guardVars.iterator();
			while( iter.hasNext() ){
				Var curVar = iter.next();
				System.out.println("	%%% testGuards curVar " + curVar.getName() + " = " + curVar.getTerm() );
				if( curVar.getName().equals("F")) fname = curVar.getTerm().toString();
				if( curVar.getName().equals("T")) time  = curVar.getTerm().toString();
			}
			assertTrue("testGuards" , fname.equals("'./audio/any_commander3.wav'") &&
						time.equals("3000"));			 
			 
			int duration = Integer.parseInt(time);			
			qa.playSound( fname, "endOfSound", duration  );
		}
				
	} catch (Exception e) {
		System.out.println("testGuards ERROR " + e.getMessage());
//		fail("testGuards " + e.getMessage()   );
	}		
		
   	}
 

    		
   	@Test
	public void actorSolveOk() { 
		try {			
			System.out.println(" *** actorSolveOk *** "); 			
			AsynchActionResult aar = qa.solveGoalReactive("fibo(27,V)" , 6000, alermEvsStr , "");
   			System.out.println("aar= " + aar  );
  			System.out.println("aar event=" +  aar.getEvent() );	 			
    		assertTrue("aar event", aar.getEvent() == null );
  			System.out.println("aar goon=" +  aar.getGoon() );
  			assertTrue("aar goon",   aar.getGoon() );
  			System.out.println("aar interrupted =" +   aar.getInterrupted() );
//   			assertTrue("aar interrupted",  ! aar.getInterrupted() );
   			System.out.println("aar time remained=" +  aar.getTimeRemained() ) ;
//  			assertTrue("aar timeremained",  aar.getTimeRemained() > 0   ); 			
 			System.out.println("aar result= " + aar.getResult()  );
  			assertTrue("aar result",  aar.getResult().equals("fibo(27,196418)"));
  		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
//			fail("actorSolveOk " + e.getMessage() );
		}		
	}

   	@Test
	public void actorSolveTout() { 
		try {			
			System.out.println(" *** actorSolveTout *** ");
 			AsynchActionResult aar = qa.solveGoalReactive("fibo(27,V)" , 200,  alermEvsStr , "");
//   			System.out.println("aar= " + aar  );
//asynchActionResult(action(solve),result(timeOut(200)),suspended(true),times(exec(200),max(200)),goon(true),event(timeOut))  			
//  			System.out.println("aar event=" +  aar.getEvent() );	 			
   			assertTrue("aar event", aar.getEvent().getEventId().equals("timeOut") );
//  			System.out.println("aar goon=" +  aar.getGoon() );
  			assertTrue("aar goon",   aar.getGoon() );
  			System.out.println("aar interrupted =" +   aar.getInterrupted() );
   			assertTrue("aar interrupted",   aar.getInterrupted() );
//  			System.out.println("aar time remained=" +  aar.getTimeRemained() ) ;
  			assertTrue("aar timeremained",  aar.getTimeRemained() == 0   ); 			
// 			System.out.println("aar result= " + aar.getResult()  );
   			assertTrue("aar result",  aar.getResult().startsWith("timeOut("));
  		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
//			fail("actorMsgTest " + e.getMessage() );
		}		
	}
  	
   	@Test
	public void actorSolveReact() { 
		try {			
			System.out.println(" *** actorSolveReact *** "); 
			QActorUtils.emitEventAfterTime(ctx,"testing", alarm, "fire", 500);			
			AsynchActionResult aar = qa.solveGoalReactive("fibo(42,V)" , 8000,  alermEvsStr , "");
   			System.out.println("aar= " + aar  );
//asynchActionResult(action(solve_1),result(interrupted(local_alarm)),suspended(true),times(exec(458),max(2000)),goon(true),event(local_alarm))   			
//  			System.out.println("aar event=" +  aar.getEvent() );	 			
  			assertTrue("aar event", aar.getEvent().getEventId().equals(alarm));
//  			System.out.println("aar goon=" +  aar.getGoon() );
  			assertTrue("aar goon",   aar.getGoon() );
// 			System.out.println("aar interrupted =" +  aar.getInterrupted() );
  			assertTrue("aar interrupted",  aar.getInterrupted() );
//  			System.out.println("aar time remained=" +  aar.getTimeRemained() ) ;
  			assertTrue("aar timeremained",  aar.getTimeRemained() >= 0   ); 			
// 			System.out.println("aar result= " + aar.getResult()  );
 			assertTrue("aar result",  aar.getResult().equals("interrupted(local_alarm)"));
  		} catch (Exception e) {
			System.out.println("actorSolveReact ERROR " + e.getMessage());
//			fail("actorSolveReact " + e.getMessage() );
		}		
	}

 
}
