/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.akka.actor;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import alice.tuprolog.SolveInfo;
import alice.tuprolog.Var;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.akka.QActor;

public class QActorTester extends QActor{
	
	public QActorTester(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, 
				"./test/it/unibo/qactors/akka/actor/WorldTheory.pl",				
				outEnvView, "noplan");
 	}


	/*
	 * 1) ask to play a short sound and waits until the sound  ends 
	 * 2) ask play a music in asynchronous  way
	 * 3) while the music is playing ask to play a message in synchronous way 
	 */
	protected void doJob() throws Exception {
//		testRules();
//		addRule( "a(1)" );  
//		addRule( "a(2)" );  
 	}
	
	protected void testRules() throws Exception{
 		addRule( "a(2)" );  
 		AsynchActionResult aar = //solveGoal( "a(X)" , 1000, "", "" , "" ); //solve timed
 				QActorUtils.solveGoal( this,myCtx, pengine, "a(X)" , new String[]{}, this.outEnvView, 2000 );
 		println("	%%% "+getName() + " testRules aar=" + aar.getResult() );
 		SolveInfo sol = solveGoal( "goalResult(X)"   ); //solve immediately
 		println("	%%% "+getName() + " goalResult sol=" + sol );
	}
	
	protected void testGuards() throws Exception{
		addRule( "soundFile(filename,2000)" );  
		List<Var> guardVars = QActorUtils.evalTheGuard( this, "soundFile(F,T)", QActorUtils.guardVolatile);
		if( guardVars != null ){
			Iterator<Var> iter = guardVars.iterator();
			while( iter.hasNext() ){
				Var curVar = iter.next();
				println("	%%% "+getName() + " curVar " + curVar.getName() + " = " + curVar.getTerm() );
			}
			
		}
	}

	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );		
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " RECEIVES QActorMessage " + msg.getDefaultRep() );	
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}
}
