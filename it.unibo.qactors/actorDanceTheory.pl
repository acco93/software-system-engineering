/*
===============================================================
actorDanceTheory.pl in project it.unibo.qactors
===============================================================
*/

dance :-  
 	actorobj( Actor ),
 	actorPrintln(forward),
 	Actor <- delay(2000),
  	actorPrintln(left),
 	Actor <- delay(2000),
 	actorPrintln(right),
 	Actor <- delay(2000),
 	actorPrintln(backward),
 	Actor <- delay(2000).
 
initdance  :-   actorPrintln("initializing the ActorDanceTheory ...").
:- initialization(initdance).