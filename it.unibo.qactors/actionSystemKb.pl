%==============================================
% USER DEFINED
% system configuration: mockSystemKb.pl
%==============================================
using(akka).
context( ctxAction, "localhost", "TCP", "8070" ).
 
qactor( qaplayer, ctxAction, "it.unibo.qactors.akka.action.QActorActionSound" ).
qactor( qaclient, ctxAction, "it.unibo.qactors.akka.action.QActorClient" ).
  