%==============================================
% USER DEFINED
% system configuration: mockSystemKb.pl
%==============================================
using(akka).
context( ctxMockAkka, "localhost", "TCP", "8010" ).
context( ctxOther, "localhost", "TCP", "8020" ).

qactor( qamock1, ctxMockAkka, "it.unibo.qactors.akka.QActorMockAkka" ).

qactor( qamock2, ctxOther, "it.unibo.qactors.akka.QActorMockAkka" ). 
qactor( qamock3, ctxOther, "it.unibo.qactors.akka.QActorMockAkkaFail" ). 
 