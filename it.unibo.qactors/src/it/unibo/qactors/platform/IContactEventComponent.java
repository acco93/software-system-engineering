package it.unibo.qactors.platform;

import it.unibo.contactEvent.interfaces.IContactComponent;

public interface IContactEventComponent extends IContactComponent{
	public boolean isStared();
}
