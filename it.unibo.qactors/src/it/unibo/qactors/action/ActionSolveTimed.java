package it.unibo.qactors.action;
import java.util.concurrent.Callable;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.akka.QActor;
 
/*
 * ActionSolveTimed
 */
public class ActionSolveTimed extends ActorTimedAction{
private String goal ;
private String myresult;
private Prolog pengine;

 	public ActionSolveTimed(String name,   QActor actor,  QActorContext ctx, Prolog pengine, String goal,
			String terminationEvId, String[] alarms, IOutputEnvView outView,
			int maxduration) throws Exception {
		super(name, actor, ctx, false, terminationEvId,  alarms, outView, maxduration);
		this.goal    = goal;
		this.pengine = pengine;
		myresult     ="going";
 	}
 
 	@Override
	protected Callable<String> getActionBodyAsCallable(){
 		return new Callable<String>(){
			@Override
			public String call()   {
				try{
//  		  				println("ActionSolveTimed " + getName() + " solving ... " + goal + " maxduration=" +maxduration);
						SolveInfo sol = pengine.solve(goal+".");
//   		 				println("ActionSolveTimed goal= " + goal + " sol= " + sol.isSuccess() ); 
						if( sol.isSuccess() ){
							myresult = ""+sol.getSolution();
						}else{
// 							if(suspendevent!=null) println("ActionSolveTimed " + goal + " failure " +  suspendevent );
							myresult="failure";
 							if(suspendevent==null) pengine.solve("setPrologResult(failure).");  
						}
				}catch( Exception e){
						println("ActionSolveTimed " + goal + " ERROR= " + e.getMessage() );
						 myresult="failure";
				}
				return myresult;
			}
		};
	}
	
	@Override
	public String getApplicationResult() throws Exception {
		//we return the solution as it is !!
// 		println("ActionSolveTimed  getApplicationResult myresult=" + myresult +" suspendevent=" + suspendevent);
// 		println("ActionSolveTimed  getApplicationResult execTime=" + this.getExecTime() );
		if( this.suspendevent == null ){
			pengine.solve("setPrologResult(result("+myresult+"," + this.getExecTime() + ")).");  //SIDE EFFECT
			return myresult;	 
		}else{			  
			pengine.solve("setPrologResult(EVENT).".replace("EVENT", suspendevent));
 			return suspendevent;
		}
	}
 }
