package it.unibo.qactors.action;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface IObservableActionGeneric<T> extends Callable<T>  {
	public T execSynch() throws Exception;
	public Future<T> execASynch() throws Exception;
 	public String getName();
	public String getTerminationEventId(); 
 	public String getResultRep();
	public long   getExecTime();	 
}