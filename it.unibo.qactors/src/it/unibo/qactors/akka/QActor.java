package it.unibo.qactors.akka;

import static akka.pattern.Patterns.ask;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.UntypedActor;
import akka.util.Timeout;
import alice.tuprolog.Library;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.lib.InvalidObjectIdException;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IBasicUniboEnv;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActionRegisterMessage;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionDummyTimed;
import it.unibo.qactors.action.ActionReceiveTimed;
import it.unibo.qactors.action.ActionSolveTimed;
import it.unibo.qactors.action.ActionUtil;
import it.unibo.qactors.action.ActorTimedAction;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
//import it.unibo.qactors.action.PlanActionDescr;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.platform.EventPlatformKb;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

 
/*
 * A QActor is an akka actor that owns a local Prolog theory
 */
public abstract class QActor extends UntypedActor{
protected String actorId;
protected QActorContext myCtx;
protected IOutputEnvView outEnvView;
protected Prolog pengine ;
protected ActorRef evlpActorRef;
protected String worldTheoryPath = null;
protected String initPlan;

//TODO REGENERATE
protected IBasicUniboEnv env = null;	
protected String temporaryStr = "0";     
protected Hashtable<String, String> guardVars ;	

public static final boolean suspendWork      = false;
public static final boolean continueWork     = true;
public static final boolean interrupted      = true;
public static final boolean normalEnd        = false;
//visible to QActorPlanUtils
public int nPlanIter = 0;		
public String curPlanInExec;	 
public Stack<String>  planStack = new Stack<String>(); 
public Stack<Integer> iterStack = new Stack<Integer>();
public QActorActionUtils actionUtils;
public QActorPlanUtils planUtils;

protected IEventItem currentEvent, lastEvent;
protected int numOfInterruptEvents = 0;
protected int timeoutval           = 0;

public IMsgQueue mysupport ; //used by AbstractXxx extends QActor

	public QActor(String actorId, QActorContext myCtx, IOutputEnvView outEnvView ) {
		this.actorId    = actorId;
		this.myCtx      = myCtx;
		this.outEnvView = outEnvView;
		this.pengine    = new Prolog();	//each QActor has its own prolog machine
		actionUtils     = new QActorActionUtils(this,outEnvView);
		planUtils       = actionUtils.getQActorPlanUtils(); 
		numOfInterruptEvents = 0;
 	}
	public QActor(String actorId, QActorContext myCtx, 
			String worldTheoryPath, IOutputEnvView outEnvView, String defaultPlan ) {
		this.actorId    = actorId;
		this.myCtx      = myCtx;
		this.outEnvView = outEnvView;
		this.pengine    = new Prolog();	//each QActor has its own prolog machine
		this.env        = outEnvView.getEnv();
		this.initPlan   = defaultPlan;
		this.worldTheoryPath = worldTheoryPath;
		numOfInterruptEvents = 0;
		actionUtils = new QActorActionUtils(this,outEnvView);
		planUtils   = actionUtils.getQActorPlanUtils(); 
	}
	
	@Override
	public void preStart() {
// 		println("QActor " + getSelf() + " STARTS with ctx=" + myCtx.getName());
		/*
		 * An actor should terminate in a coordinated way
		 */
		try {
			QActorUtils.memoQActor(  this );
			if(worldTheoryPath !=null ) loadWorldTheory();	
			QActorUtils.registerActorInProlog18(this.pengine, this);
//  	  		QActorUtils.solveGoal(pengine, "actorobj(X), output( actorobj(X) )"); //
//  	  		QActorUtils.solveGoal(pengine, "actorPrintln(xxx)"); //
			doJob();
		} catch (Exception e) {
 			println("QActor " + getName() + " ENDS with ERROR=" + e.getMessage() ) ;	
//			this.terminate();
		}
		/*
		 * Termination must be called by the user-defined actor
		 */
 	}
	
	protected void set_evlpActorSel(){ 
		if( evlpActorRef != null ) return;
//		evlpActorSel = QActorUtils.getEvlpActorSel();
		evlpActorRef = myCtx.getEvlpActorRef();
// 		evlpActorSel = QActorUtils.getSelectionIfLocal(myCtx,QActorUtils.getEventLoopActorName(myCtx)); 
// 		println("QActor " + getName() + " evlpActorSel = " + evlpActorSel );
////		if(evlpActorSel !=null) evlpActorRef = QActorUtils.getActorRefFromActorSelection(evlpActorSel);
//		if(evlpActorSel != null && evlpActorRef == null){
//			evlpActorRef = QActorUtils.getActorRefFromActorSelection(evlpActorSel);
// 		}
	 }

	protected abstract void doJob() throws Exception;
	
	public QActorContext getQActorContext(){
		return myCtx;
	}
	public Prolog getPrologEngine(){
		return pengine;
	}
	public  IEventItem getCurrentEvent( ) {
 		return currentEvent;
	}
 	/*
	 * The action could terminate before the application calls waitForLastEvent
	 * In this case we keep track of the last interrupt event (currentEvent) only
	 */
	public synchronized void setCurrentEvent(IEventItem ev){
//		println(getName() +  " setCurrentEvent " + ev );	
		numOfInterruptEvents++;
		this.currentEvent = ev;
		this.lastEvent    = ev;
		this.notifyAll();
	}
	public synchronized IEventItem waitForCurentEvent( ) throws Exception{
 		return waitForLastEvent(1,true);
	}
	public synchronized IEventItem waitForLastEvent( int num, boolean withTrace ) throws Exception{
 		println(getName() + " waitForLastEvent n=" + numOfInterruptEvents ); 
		while( lastEvent == null ){ //&& numOfInterruptEvents < num 
			wait();
  			if( withTrace )
			println(getName() + " n=" + numOfInterruptEvents + " EVENT=" + lastEvent.getDefaultRep() ); 
		}
		IEventItem temp = lastEvent;
		lastEvent = null;
		return temp;
	}

/*
 * ------------------------------------------------------------------
 * 	MESSSAGING
 * ------------------------------------------------------------------
 */
	public void sendMsg(String msgID, String dest, String msgType, String msg) throws Exception {
		//Prepare the massage
 		 QActorMessage mout  = QActorUtils.buildMsg(myCtx, getName(), msgID, dest, msgType, msg);
  		 ActorSelection asel = QActorUtils.getSelectionIfLocal( myCtx, dest );
// 		 println(getName() +" sendMsg  mout=" + mout + " to " + dest + " asel=" + asel);
		 //The the destActorId is LOCAL: Send the message
		 if( asel != null ) asel.tell( mout, getSelf() ); 
		 else {
			 //dest is remote
			 SenderObject sa = myCtx.getSenderAgent( dest );
			 msg=envelope(msg);
// 			 println(getName() +" sendMsg   " + msg + " to " + sa);
			 sa.sendMsg( this, dest, msgID, msgType, msg );
		 }
  	}
	protected String envelope( String content){
		try{
			Term tt = Term.createTerm(content);
			return tt.toString();
		}catch(Exception e){
			return "'"+content+"'";
		}
	}

// 	public void sendMsg( String msg ) throws Exception {
// 		QActorMessage mout = new QActorMessage(msg);
// 		sendTheMsg( mout ); 
// 	}
// 	protected void sendTheMsg(QActorMessage msg) throws Exception{
// 		 
// 		 String dest = msg.msgReceiver() ;
// 		 ActorSelection asel = QActorUtils.getSelectionIfLocal( myCtx, dest );
//		 //The the destActorId is local: Send the message
//		 if( asel != null ) asel.tell( msg, getSelf() ); 
//		 else {
//			 //dest  
//			 SenderObject sa = myCtx.getSenderAgent( dest );
//			 println("sendTheMsg   " + msg + " to " + sa);
//			 sa.sendMsg( msg.getDefaultRep() );
//		 }
// 	}
//		
	protected Future<Object> askMessage(ActorRef dest, Object askmsg, int tout){
		Timeout timeout = new Timeout(Duration.create(tout, "seconds"));
// 		println("askMessage to " + dest.path());	
 		Future<Object> future = ask( dest, askmsg, timeout);
		return future;
	}
	protected void askMessageWaiting(ActorRef dest, Object askmsg, int tout){
		Future<Object> future = askMessage(  dest,   askmsg,   tout);
		waitForAskResult(future,tout);
	}
	public String askMessageWaiting(String dest, String askmsg, int tout) throws Exception{
		askmsg=envelope(askmsg);
//		println("QActor " + getName() + " askMessageWaiting   " + askmsg + " to " + dest);
		QActorMessage mout = QActorUtils.buildMsg(myCtx, getName(), "request", dest, QActorContext.request, askmsg);
		ActorSelection destsel = QActorUtils.getSelectionIfLocal( myCtx, dest );
		 //The the destActorId is local: Send the message
		 if( destsel != null ){
			    ActorRef destRef = QActorUtils.getActorRefFromActorSelection(destsel);
				Future<Object> future = askMessage(  destRef,   mout,   tout);
				//The sender is NOT the current akka actor
				return waitForAskResult(future,tout); 
		 }
		 else {
			 //dest is remote TODO
			 SenderObject sa = myCtx.getSenderAgent( dest );
 //			 println("sendTheMsg   " + msg + " to " + sa);
 			 sa.sendMsg( mout.getDefaultRep() );
 			 String answer = sa.receiveWakeUpAnswer();
 			 return "TODO";
		 }
	}
	

	protected String waitForAskResult(Future<Object> future, int tout){
		try {
 			String	result =  (String) Await.result(future, new Timeout(Duration.create(tout, "seconds")).duration());
//  			println("	*** QActor " + getName() +" waitForAskResult result="+result + " tout=" + tout );
 			//Auto propagate the answer
// 			getSelf().tell(result, getSelf());
 			return envelope(result);
		} catch (Exception e) {
// 			e.printStackTrace();
 			return "failure("+e.getMessage()+")";
		}						
	}
	
 	public void emit( String evId, String evContent ) {
		set_evlpActorSel();
 		//Send a message to the context event loop actor
		IEventItem ev = QActorUtils.buildEventItem(  this.getName(), evId, evContent  );
		evlpActorRef.tell(ev, getSelf());
//		println("QActor " + getName() + " emit " + evId + " evContent=" + evContent  + " evlpActorRef=" + evlpActorRef);
  		if( ! evId.startsWith(QActorUtils.locEvPrefix)){
  			String evRep = ev.getPrologRep();
  			try {
				QActorUtils.propagateEvent(this.myCtx,evRep);
			} catch (Exception e) {
 				e.printStackTrace();
			} 
  		}
	}
	protected void raiseEvent( String evId, String evContent) throws Exception{
		emit( evId, evContent );
 	}
 	public void registerForEvent( String evId , int time) throws Exception{
		set_evlpActorSel();
//		println("QActor " + getName() + " registerForEvent " + evId + " evlpActorRef=" + evlpActorRef  );
 		//Send a message to the context event loop actor
		IEventItem ev  = QActorUtils.buildEventItem(  getName(), EventPlatformKb.register, evId  );
		if(evlpActorRef != null){
//			ActorRef evlpActorRef = QActorUtils.getActorRefFromActorSelection(evlpActorSel);
			askMessageWaiting( evlpActorRef, ev, time);
		}else{
 			println("	*** QActor registerForEvent evlpActorRef = "+evlpActorRef);
//			throw new Exception("registerForEvent too early");
		}
//		println("	*** QActor has registered " + getName() + " for " + evId);
	}
	public void registerForEvent(  String evId, ActorTimedAction action ) throws Exception{
		set_evlpActorSel();
 		ActionRegisterMessage msg = new ActionRegisterMessage(evId, action, true);
		if(evlpActorRef != null){
 			askMessageWaiting( evlpActorRef, msg, 1000);
		}else{
			throw new Exception("registerForEvent too early");
		}		
	}
	public void unregisterForEvent( String evId )throws Exception{
		//Send a message to the context event loop actor
		IEventItem ev =  QActorUtils.buildEventItem(  getName(), EventPlatformKb.unregister, evId );
		if(evlpActorRef != null) evlpActorRef.tell(ev, getSelf());
	}
	
	public String receiveAction(IMsgQueue mysupport, int maxTime) throws Exception {
		IActorAction action = new ActionReceiveTimed(
				getName() + "action", this, myCtx, mysupport, false,
				QActorUtils.getNewName(IActorAction.endBuiltinEvent),
				new String[] {}, outEnvView, maxTime);
		String res = action.execSynch();
		println("	--------------- receiveAction res=" + res);
		//msg(interrupt,event,callable,none,receive(timeOut(30),timeRemained(0)),0)
		currentMessage = new QActorMessage(res);
		return  res;
	}

	
 /*
 *  ENTRY POINTS for messages
 *  
 */ 	
	public AsynchActionResult senseEvents(int tout, String events) throws Exception {
		return this.planUtils.senseEvents(tout, events, "", "", "", ActionExecMode.synch);
	}
 	@Override
	public void onReceive(Object message) throws Throwable {
//   		println("QActor "+getName() + " onReceive: " + message.getClass().getName() );
		if (message instanceof String){
			handleQActorString( (String)message );
//	  		println(getName() + " RECEIVES String=" + message );
	  		return;
 		}		
		if (message instanceof IEventItem){ //For eventLoopActor
			handleQActorEvent( (IEventItem)message );
//			IEventItem ev = (IEventItem)message;
//	  		println(getName() + " RECEIVES EVENT=" + ev.getDefaultRep() );
	  		return;
 		}		
		if (message instanceof QActorMessage){
			handleQActorMessage( (QActorMessage)message );
//			QActorMessage msg = (QActorMessage)message;
//	  		println(getName() + " RECEIVES MSG =" + msg.getDefaultRep() );
	  		return;
 		}		 
//		if( message instanceof RequestForMessage ){
//			handleRequestForMessage( (RequestForMessage)message );
//			return;
//		}
		if (message instanceof ActorTerminationMessage){
			handleTerminationMessage((ActorTerminationMessage)message);
//			ActorTerminationMessage msg = (ActorTerminationMessage)message;
//	  		println(getName() + " RECEIVES TERMINATION MSG =" + msg.getName() );
//	  		this.terminate();
	  		return;
 		}		
	}
 
	protected void handleQActorString(String msgStr) {
		try {
//			println("	%%% "+ getName() + " handleQActorString : " + msgStr  );
			QActorMessage msg = new QActorMessage(msgStr);
			handleQActorMessage( msg );
		} catch (Exception e) {
 			e.printStackTrace();
		}
	}
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " (QActor)handleQActorEvent : " + ev.getDefaultRep() );		
	}
 	 
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " (QActor)handleQActorMessage " + msg.getDefaultRep() );	
	}
 	 
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " (QActor)handleTerminationMessage "  );	
		this.terminate();
	}
	
	protected QActorMessage currentMessage = null;
	
	public QActorMessage getMsgFromQueue( ){ return currentMessage;}
		

	public void printCurrentMessage(boolean withMemo){
		println("--------------------------------------------------------------------------------------------");
		if(currentMessage != null){
			String msgStr = currentMessage.getDefaultRep();
			println(getName() + " currentMessage=" + msgStr );
			if( withMemo ) addRule(msgStr);
		}
		else println(getName() + " currentMessage IS null"  );
		println("--------------------------------------------------------------------------------------------");
	}
	public void printCurrentEvent(boolean withMemo){
		println("--------------------------------------------------------------------------------------------");
		if(currentEvent != null){
			String eventStr = currentEvent.getDefaultRep();		
			println(getName() + " currentEvent=" + eventStr );
			if( withMemo ) addRule(eventStr);
		}
		else println(getName() + " currentEvent is null"  );
		println("--------------------------------------------------------------------------------------------");
	}
	public void memoCurrentEvent(IEventItem currentEvent , boolean lastOnly) throws Exception{
 		try{
 			if( currentEvent == null ) return;
 			String evId = currentEvent.getEventId();
			Term t = Term.createTerm(currentEvent.getDefaultRep());
// 			println(getName() + " memoCurrentEvent " + currentEvent.getPrologRep() );
			String eventStr = currentEvent.getPrologRep();	
//  	 	println(getName() + " 	memoCurrentEvent:" + evId + " " + eventStr );	
			if( lastOnly ){
				String fact = "msg(A,event,C,none,E,F)".replace("A", evId);
				this.pengine.solve("removeRule("+fact +").");
			}
	 		this.pengine.solve("asserta("+eventStr +").");
		}catch( Exception e){
			println("memoCurrentEvent ERROR " + e.getMessage() );
  		}		
	}
	public void memoCurrentEvent(boolean lastOnly) throws Exception{
		memoCurrentEvent(currentEvent,lastOnly);
	}
	public void memoCurrentMessage(boolean lastOnly) throws Exception{
		String msgStr = currentMessage.getDefaultRep();		
		String msgId  = currentMessage.msgId();
//		println(getName() + " 			memoCurrentMessage:" +msgStr );		 
//		addRule(msgStr);
		if( lastOnly ){
			String fact = "msg(A,B,C,D,E,F)".replace("A", msgId).replace("D", this.getName());
			this.pengine.solve("removeRule("+fact +").");
		}
		this.pengine.solve("asserta("+msgStr +").");
 	}
  	
	
  
/*
 * 	--------------------------------------------------------
 *  METHODS
 * 	--------------------------------------------------------
 */
	public String getName(){
		return actorId;
	}	
	/*
	 * ENTRY POINT of WorldTheory
	 */
	public void println(String msg) {
		outEnvView.addOutput(msg);
	}

/*
 * 	
 */
	public void terminate(){
//		IEventItem ev =  QActorUtils.buildEventItem(  getName(), EventPlatformKb.unregister, evId );
// 		evlpActorSel.tell(ev, getSelf());
		QActorUtils.forgetQActor(this);
		ActorTerminationMessage msg = new ActorTerminationMessage( this.getName(),QActorContext.testing );
// 		this.sendPoisonPill();
		getQActorContext().getSystemCreator().tell(msg, getSelf());

	}
	protected void sendPoisonPill(){
		getSelf().tell(akka.actor.PoisonPill.getInstance(), getSelf());		
	}

	/*
	 * ----------------------------------------	
	 * WORLD THEORY    
	 * ----------------------------------------	
	*/  
		protected void loadWorldTheory() throws Exception{
			try{
	 	   		Theory worldTh = new Theory( getClass().getResourceAsStream("WorldTheory.pl") );
		  		pengine.addTheory(worldTh);
	   	  		pengine.solve("setActorName(" + getName()  + ").");
	  	  		pengine.solve("actorobj(X).");
//	 	  		println(getName() + " loadWorldTheory done "   );	 		
	  		}catch( Exception e){
//	 			println(getName() + " loadWorldTheory WARNING: "  + e.getMessage() );
	 			loadWorldTheoryFromFile();
	 		}
	 	}

		protected void loadWorldTheoryFromFile()  {
			try{
 			   	Theory worldTh = new Theory( new FileInputStream(worldTheoryPath) );
		  		pengine.addTheory(worldTh);
	   	  		pengine.solve("setActorName(" + getName()  + ").");
	  	  		pengine.solve("actorobj(X).");
//	 	  		println(getName() + " loadWorldTheoryFromFile done " + worldTheoryPath  );	 		
 	 		}catch( Exception e){
	 			println(" loadWorldTheory WARNING: "  + e.getMessage() );
	 		}		
		}
  		
	public AsynchActionResult delayReactive(int time, String  alarmEvents, String recoveryPlans) throws Exception{
//		IActorAction action = new ActionDummyTimed( this, alarmEvents, outEnvView, time );	 
		String name = QActorUtils.getNewName("da_");		
		String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		if( alarmEvents.length() == 0 ){
			Thread.sleep(time);
			return new AsynchActionResult(null, time, false, true, "", null);
		}
		String[] evarray   = QActorUtils.createArray(alarmEvents);
		String[] planarray = QActorUtils.createArray(recoveryPlans);
 
		IActorAction action    = new ActionDummyTimed( name,this,myCtx,terminationEvId, evarray, outEnvView, time );	 
		AsynchActionResult aar = actionUtils.executeReactiveAction(action, ActionExecMode.synch, evarray, planarray);		 
		return aar;		
	}

	public void delay( int dt ){
		try {
			Thread.sleep(dt);
		} catch (InterruptedException e) {
// 			println("QActor delay interrupted");
		}
	}
	public AsynchActionResult playSound(String fName, String terminationEvId, int duration ) throws Exception{
//  		println("QActor playSound " + fName + " terminationEvId=" + terminationEvId  + " duration=" + duration );
		return playSound(fName,ActionExecMode.synch,terminationEvId,duration,"","");
	}
	//Called by WorldTheory executedCmd
	public AsynchActionResult playSound(String fName, int duration,String alarmEvents, String recoveryPlans) throws Exception{
//  		println("QActor playSound " + fName + " duration=" + duration +  " alarmEvents=" + alarmEvents + " recoveryPlans=" + recoveryPlans);
		String terminationEvId = QActorUtils.getNewName("endSound");
		return playSound(fName,ActionExecMode.synch,terminationEvId,duration,alarmEvents,recoveryPlans);
	}
	//play('./audio/music_interlude20.wav'),20000,"alarm,obstacle", "handleAlarm,handleObstacle"
	public AsynchActionResult playSound(String fName, ActionExecMode mode, String terminationEvId,
		 			int duration,String alarmEvents, String recoveryPlans) throws Exception{
//  		println("QActor playSound " + fName + " terminationEvId=" + terminationEvId + " alarmEvents=" + alarmEvents + " recoveryPlans=" + recoveryPlans);
 		String[] evarray   = QActorUtils.createArray(alarmEvents);
		String[] planarray = QActorUtils.createArray(recoveryPlans);
		if(mode==ActionExecMode.asynch && planarray.length>1) 
			throw new Exception("Plans not supported for asynch actions");
		if(mode==ActionExecMode.asynch && planarray.length==1 && ! planarray[0].equals("continue")) 
			throw new Exception("Only plan=continue is supported for asynch actions");		
// 		println("QActor playSound " + fName + " evarray=" + evarray.length + " recoveryPlans=" + planarray.length);
		IActorAction action = 
				ActionUtil.buildSoundActionTimed(this,myCtx,outEnvView,duration,terminationEvId,fName,evarray);			
		AsynchActionResult aar=actionUtils.executeReactiveAction(action, mode, evarray, planarray);
//     	println("QActor playSound " + fName + " aar=" + aar);
		return  aar;
	}

	public AsynchActionResult fibo(ActionExecMode mode, int n, String terminationEvId,
			int duration,String alarmEvents, String recoveryPlans) throws Exception{
		String[] evarray   = QActorUtils.createArray(alarmEvents);
		String[] planarray = QActorUtils.createArray(recoveryPlans);

		if(mode==ActionExecMode.asynch && planarray.length>1) 
			throw new Exception("Plans not supported for asynch actions");
		if(mode==ActionExecMode.asynch && planarray.length==1 && ! planarray[0].equals("continue")) 
			throw new Exception("Only plan=continue is supported for asynch actions");
		
  		IActorAction action    = ActionUtil.buildFiboActionTimed(this,myCtx,outEnvView, n, duration,terminationEvId,evarray);			
		return  actionUtils.executeReactiveAction(action, mode, evarray, planarray);		
	}
  
	 /*
	  * ----------------------------------------------
	  * RULES	
	  */
	 	
	 	public  synchronized void addRule( String rule  ){
	 		try{
//    	 			println("addRule:" + rule   );
	 			if( rule.equals("true")) return;
//  	 			println("addRule:" + rule   );
	  			SolveInfo sol = 
	  					pengine.solve( "addRule( " + rule + " ).");
//  	 			println("addRule:" + rule + " " + sol.isSuccess() );
 	  		}catch(Exception e){
	  			println("addRule ERROR:" + rule + " " + e.getMessage() );
	   		}
	   	}
	 	public  synchronized void removeRule( String rule  ){
	 		try{
	 			rule = rule.trim();
	 			if( rule.equals("true")) return;
	 			SolveInfo sol = pengine.solve( "removeRule( " + rule + " ).");
//   	 			println("removeRule:" + rule + " " + sol.isSuccess() );
	 	  	}catch(Exception e){
	  			println("removeRule ERROR:" + e.getMessage() );
	   		}	 		
	 	}
	 	
	 	public SolveInfo solveGoal(String goal ){
	 		SolveInfo sol = null;
	 		try {
// 	 			println(" ***   solveGoal SolveInfo "  + goal + " dir= "  + pengine.getCurrentDirectory() );				
	 			sol = pengine.solve(goal+".");
// 	 			println(" ***  solveGoal SolveInfo dir= "  + pengine.getCurrentDirectory() );				
	 	 	} catch (Exception e) {
	 			println("solveGoal sol WARNING: "  + e.getMessage() );
	 	 	}	
	 		return sol;
	 	}

/*
 * SHOULD BE MOVED in QActorPlanUtils
 */
//	 	public AsynchActionResult solveGoal( String goal, int duration, String events, String plans) throws Exception{
////        	 	println("QActor solveGoal " + goal + " duration=" + duration +" events=" + events  + " " + pengine.isHalted() );
//	 		//The goal executeInput(do(GUARD,MOVE,TIME))
//        	 if( duration == 0   ){
// 	 			println("solveGoal immediate " + goal   );
//	 			SolveInfo sol = pengine.solve(goal+"."); 
//// 	 	 		println("solveGoal " + goal + " sol=" + sol + " " + pengine.getCurrentDirectory()    ); //+ " " + pengine.getCurrentDirectory() 
//// 	 	 		pengine.setCurrentDirectory(this.pengigeBasicDirectory); 			
// 	 			if( sol != null && sol.isSuccess() ){
//  	 				pengine.solve("setPrologResult(" + sol.getSolution()+").");
//	 				return new AsynchActionResult(null,0,false,true,sol.getSolution().toString(),null);
//	 			}else{
// 	 				this.pengine.solve("setPrologResult( failure ).");
//	 				return new AsynchActionResult(null,0,false,true,"failure",null);	 				
//	 			}
//	 		}
//	 		/*
//	 		 * duration > 0 => we must use reactive 
//	 		 */
//        	return solveGoalReactive( goal,   duration,   events,   plans);
//	 	}
	 	
	 	public AsynchActionResult solveGoalReactive( String goal, int duration, String events, String plans) throws Exception{
//  	 		println("QActor solveGoalReactive  " + goal  + " duration=" +  duration  );
//      	   if( duration == 0 ) return solveGoal(goal,   duration,   events,   plans);	 		
	 	   String[] evArray    = QActorUtils.createArray( events );
      	   String[] planArray  = QActorUtils.createArray( plans );
     	   IActorAction action =  new ActionSolveTimed(
   						"solve", this, myCtx, pengine, goal,
   						QActorUtils.getNewName(IActorAction.endBuiltinEvent), evArray, outEnvView, duration);
  		   return  actionUtils.executeReactiveAction(action, ActionExecMode.synch, evArray, planArray ); 		
	 	}
	 	
   		
    		
 /*        	 
 * 
 * PRE-AKKA
 	 		ActionSolveGoal action = new ActionSolveGoal( pengine, outEnvView, duration , answerEv, goal );

 	 		AsynchActionResult aar = actionUtils.executeActionAsFSM( action, events , plans , ActionExecMode.synch );  
	 		SolveInfo sol = action.getSolveResult();  			
//     		println("solveGoal interrupted:" + aar.getInterrupted() 
//     				+ " action suspended:" + action.isSuspended() + " action result=" + sol);
	 		if( sol != null && sol.isSuccess() ){
 	 			this.pengine.solve("setResult(" + sol.getSolution()+").");	//setResult in solve 
//	 			println("solveGoal sol=" + sol.getSolution() );
	 			aar.setResult(""+sol.getSolution());
	 			aar.setGoon(true);
//		 		System.out.println("solveGoal " + goal +" success execTime=" + action.getExecTime() );
	 		}
	 		else{
 	 			if( aar.getTimeRemained() == 0 ){
 	 	 			println("solveGoal TOUT "   );
  	 				this.pengine.solve("setPrologResult(tout).");
	 				aar.setResult("tout");
//TOUT 	 				addRule("tout(solve,"+getName()+")");
 	 			}	 			
 	 			else{
// 	 	 			println("solveGoal interrupted "   );
 	 				this.pengine.solve("setPrologResult(interrupted).");
	 				aar.setResult("failure");
 	 			}
	 			aar.setGoon(true);	//actor continues and checks the result
		 		//println("solveGoal " + goal +" failure execTime=" + action.getExecTime() );
	 		}
// 			println("solveGoal aar=" + aar );
 	 		return aar;	
*/ 	 		

	 	 

		/*
		 * --------------------------------------------------- 
		 * REFLECTION
		 * ---------------------------------------------------
		 */
		public boolean execByReflection(Class C, String methodName) {
			Method method = null;
			Class curClass = C;
// 			println("QActor execByReflection " + methodName + " curClass=" + curClass );
			while (method == null)
				try {
					if (curClass == null)
						return false;
					method = getByReflection(curClass, methodName);
					if (method != null) {
						// println("QActor execByReflection method: " +method + " in
						// class " + curClass.getName());
						Object[] callargs = null;
						Object returnValue = method.invoke(this, callargs);
						// println("QActor execByReflection " + methodName + "
						// returnValue: " +returnValue );
						Boolean goon = (Boolean) returnValue;
						return goon;
					} else {
						// println("QActor execByReflection " + methodName + " not
						// found in " +curClass.getName() );
						curClass = curClass.getSuperclass();
					}
				} catch (Exception e) {
					// If the method does not exist or does not return a boolean
					// return false
					println("QActor execByReflection " + methodName + "  WARNING: " + e.getMessage());
					// break;
				}
			return false;
		}

		public Method getByReflection(Class C, String methodName) {
			try {
				Class noparams[] = {};
				Method method = C.getDeclaredMethod(methodName, noparams);
				return method;
			} catch (Exception e) {
				// println("QActor getByReflection ERROR: " + e.getMessage() );
				return null;
			}
		}

		public boolean execApplicationActionByReflection(Class C, String methodName, String arg1, String arg2) {
			Method method = null;
			Class curClass = C;
			while (method == null)
				try {
					if (curClass == null)
						return false;
					method = getActionByReflection(curClass, methodName);
					if (method != null) {
						// println("QActor execByReflection method: " +method + " in
						// class " + curClass.getName());
						Object[] callargs = new Object[] { arg1, arg2 };
						Object returnValue = method.invoke(this, callargs);
						// println("QActor execByReflection returnValue: "
						// +returnValue );
						Boolean goon = (Boolean) returnValue;
						return goon;
					} else {
						// println("QActor execByReflection " + methodName + " not
						// found in " +curClass.getName() );
						curClass = curClass.getSuperclass();
					}
				} catch (Exception e) {
					// If the method does not exist or does not return a boolean
					// return false
					println("QActor execApplicationActionByReflection " + methodName + "  ERROR: " + e.getMessage());
					// break;
				}
			return false;
		}

		public Method getActionByReflection(Class C, String methodName) {
			try {
				Class twoparams[] = { String.class, String.class };
				Method method = C.getDeclaredMethod(methodName, twoparams);
				return method;
			} catch (Exception e) {
				// println("QActor getByReflection ERROR: " + e.getMessage() );
				return null;
			}
		}
		
		
		
		/*
	 	 * 		
	 	 */


		protected int waitForUserCommand( )  {		
	 		  		try {
	 		  			int inp;
	 		  			int ch;
	 		  			System.out.println("USER>: to end press 'e'" );
	 		  			ch = System.in.read();
	 		  			System.out.println("user:" + ch);
	 		  			do{
	 		  				inp = System.in.read();
	 		  			}while( inp != 10 );
	 		  			return ch;			
	 		  		} catch (Exception e) {
	 		  			System.out.println("USER>: ERROR" );
	 		  			return 'e';
	 		  		}
	 		  	}
	 		 	
		/*
		 * Local to QActor since it modifies guardVars
		 */
	/*
		public String updateVarsOld( Term tmsgdef, Term tmsguser, Term tmsg, String swithvar) throws Exception{
			Hashtable<String,String> htss = new Hashtable<String,String>();
			//1) Check msg templates
	 	println("*** updateVars tmsguser=" + tmsguser + " tmsg=" + tmsg + " tmsgdef=" + tmsgdef);
		SolveInfo sol = pengine.solve( tmsgdef + "  = "+ tmsguser +".");
			if( sol.isSuccess()){
				QActorUtils.memoVars(sol,htss);  				
			}else new Exception("msg template do not match");
		//2) Check msg payload
		sol = pengine.solve( tmsguser + "  = "+ tmsg +".");
		if( sol.isSuccess())  QActorUtils.memoVars(sol,htss); else{
		    	println("*** no match between tmsguser=" + tmsguser + " and tmsg=" + tmsg);
			return null; //the msg payload does not match
		}
 	  	println("*** updateVars/5 (1) swithvar=" + swithvar );
  		if( guardVars != null ) swithvar = QActorUtils.substituteVars(guardVars,swithvar);
 	 	println("*** updateVars/5 (2) " + swithvar );
 		swithvar = QActorUtils.substituteVars(htss,swithvar);  //TODO AKKA - QUTILS substituteVars COMMENTED
 	   	println("*** updateVars/5 (3) " + swithvar + " " + guardVars);
 	  	if( guardVars == null ){
	  		//Copy variables in guardVars, otherwise guardVars.get(..) does not work
//	  		println("*** updateVars/5 (4) myguardVars null"   );
	  		guardVars = new Hashtable<String,String>();
	  		Enumeration<String> es = htss.keys();
	  		while(es.hasMoreElements()){
	  			String key=es.nextElement();
 	  			println("*** updateVars/5 (4) " + key + "  " + htss.get(key) );
	  			guardVars.put(key, htss.get(key));
	  		}
 	  	}
		return swithvar;
	}	 			  	
 */ 
		public String updateVars( Term tmsgdef, Term tmsguser, Term tmsg, String swithvar) throws Exception{
			Hashtable<String,String> htss = new Hashtable<String,String>();
			//1) Check msg templates
			guardVars = null;
//	 	println("*** updateVars tmsguser=" + tmsguser + " tmsg=" + tmsg + " tmsgdef=" + tmsgdef + " guardVars="+ guardVars);
		SolveInfo sol = pengine.solve( tmsgdef + "  = "+ tmsguser +".");
			if( sol.isSuccess()){
				QActorUtils.memoVars(sol,htss);  				
			}else new Exception("msg template do not match");
		//2) Check msg payload
		sol = pengine.solve( tmsguser + "  = "+ tmsg +".");
		if( sol.isSuccess())  QActorUtils.memoVars(sol,htss); else{
		    	println("*** no match between tmsguser=" + tmsguser + " and tmsg=" + tmsg);
			return null; //the msg payload does not match
		}
	  		//Copy variables in guardVars, otherwise guardVars.get(..) does not work
	  		guardVars = new Hashtable<String,String>();
	  		Enumeration<String> es = htss.keys();
	  		while(es.hasMoreElements()){
	  			String key=es.nextElement();
// 	  			println("*** updateVars/5 (4) " + key + "  " + htss.get(key) );
	  			guardVars.put(key, htss.get(key));
	  		}
// 	  	println("*** updateVars/5 (1) swithvar=" + swithvar );
  		if( guardVars != null ) swithvar = QActorUtils.substituteVars(guardVars,swithvar);
// 	 	println("*** updateVars/5 (2) " + swithvar );
 		swithvar = QActorUtils.substituteVars(htss,swithvar);  //TODO AKKA - QUTILS substituteVars COMMENTED
// 	   	println("*** updateVars/5 (3) " + swithvar + " " + guardVars);
		return swithvar;
	} 
	 	
		public void replyToCaller(String msgId, String msg) throws Exception {
			String caller = currentMessage.msgSender().replace("_ctrl", "");
			println(getName() + " replyToCaller  " + msgId + ":" + msg + " to " + caller );
			sendMsg(msgId, caller, QActorContext.dispatch, msg);
		}
	 	/*
	 	 * -------------------------------------------------------------
	 	 * actorop  results 
	 	 * -------------------------------------------------------------
	 	 */ 
protected Struct resultName = new Struct( "actoropresult"  );	 
public void setActionResult(  Object actionResult ) throws Exception{
//	println("QActor setActionResult " +  " " + actionResult );
	if( actionResult == null ) unregisterResult();
	else this.registerResult(actionResult);
}
public void registerResult (Object obj) throws InvalidObjectIdException{
	registerResultInProlog18(obj);
}
public void unregisterResult( ) throws InvalidObjectIdException{
	unregisterResultInProlog18();
}
public void unregisterResultInProlog18( ) throws InvalidObjectIdException{
//	println("QActor unregisterResultInProlog18: " );
	Library lib = pengine.getLibrary("alice.tuprolog.lib.OOLibrary");
		((alice.tuprolog.lib.OOLibrary)lib).unregister( resultName ); 
}
public void registerResultInProlog18(Object obj) throws InvalidObjectIdException{
//	println("QActor registerResultInProlog18: " + resultName + " " + obj);
	Library lib = pengine.getLibrary("alice.tuprolog.lib.OOLibrary");
		((alice.tuprolog.lib.OOLibrary)lib).register( resultName, obj); 
}
//For talktheory (action interpreter)
public boolean isSimpleActor(){
	return true;
}
}
