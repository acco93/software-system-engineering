/*
  * Provides a method to send messages to a remote context
  * (by creating a connection with that remote context)
 */
package it.unibo.qactors.akka;
import java.util.Hashtable;

import alice.tuprolog.Term;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.qactors.QActorContext;
import it.unibo.supports.FactoryProtocol;
import it.unibo.system.SituatedPlainObject;

public class SenderObject extends SituatedPlainObject{	
	protected IConnInteraction conn = null;
	protected String hostName;
	protected int port;
 	protected FactoryProtocol factoryP;
 	protected QActorContext ctx;
	protected String protocol;
	protected SenderObject myself;
	protected boolean tryingToConnect = true;
	
	protected  Hashtable<String,IConnInteraction> connectionTable;	
	
	public SenderObject(String name, QActorContext ctx, IOutputView outView,  String protocol, String hostName, int port ) {
		super(name, outView);
		this.ctx = ctx;
		this.hostName 	= hostName;
		this.port     	= port;
		this.protocol	= protocol;
		factoryP 		= new FactoryProtocol(outView, protocol, "fp");
		myself 			= this;
		
		connectionTable	= new Hashtable<String,IConnInteraction>();
		tryTheConnection();
		
    } 
	protected void tryTheConnection(){
		//Attempt to connect with the remote node
		new Thread(){
			public void run(){
				boolean res = false;
				while( ! res ){
					res = setConn( );
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
							e.printStackTrace();
					}
				}//while
				tryingToConnect = true;
				try {
					myself.updateWaiting();
				} catch (Exception e) {
 					e.printStackTrace();
				}
			}
		}.start();		
	}
	protected synchronized void updateWaiting() throws Exception{
		conn = connectionTable.get(hostName+port);
		this.notifyAll();
	}
	public synchronized void sendMsg(QActor sender, String receiverName, String msgId, String msgType, String m) throws Exception {
// 		println("SenderObject sendMsg/5 to " + hostName + " conn=" + conn );
		conn = connectionTable.get(hostName+port);
		while( conn == null ){
 			println("SenderObject waits since NO CONNECTION to " + hostName + port  );
			wait();
		}
		if( conn != null ){
//			println("SenderObject sends " + m + " on " + conn  );
			sendTheMsg(  sender, receiverName,  msgId, msgType, m) ;	
		} else{
			println("SenderObject NO CONNECTION to " + hostName + ":" + port   );
 		}
  	}
	protected void sendTheMsg(QActor sender, String receiverName,  String msgId, String msgType, String m) throws Exception {
		try {
			int msgNum = ctx.newMsgnum();
			//msg( MSGID, MSGTYPE SENDER, RECEIVER, CONTENT, SEQNUM )
			String senderName  = sender.getName();//.toLowerCase();
  	 		String mout = "msg(" + msgId +","+ msgType + ","+ senderName +","+ receiverName +","+ m+","+ msgNum +")";
//  	 		println("SenderObject sends " + mout + " " + hostName + ":" + port  + " conn=" + (conn != null) );
			conn = connectionTable.get(hostName+port);			 
			//If the connection has been rest we block forever
			while( conn == null ){
//				println("SenderObject waits since NO CONNECTION to " + hostName + port   );
				if( ! tryingToConnect ) tryTheConnection();
				wait();
			}
	 		conn.sendALine( mout );
		} catch (Exception e) {
//			println("SenderObject " + name + " send/5 ERROR " + e.getMessage()   );
			connectionTable.remove(hostName+port);
//			ctx.ctxTable.remove(name);
			conn = null;
  			throw e;
		}									
	}
	public synchronized void sendMsg(String mout) throws Exception {
		try {
			conn = connectionTable.get(hostName+port);
			 
			//If the connection has been rest we block forever
			while( conn == null ){
//				println("SenderObject waits since NO CONNECTION to " + hostName + port   );
				if( ! tryingToConnect ) tryTheConnection();
				wait();
			}
			 
			if( conn != null ){
//				println("SenderObjectsendALine " + mout    );
				conn.sendALine( mout );	
			} else{
				println("SenderObject NO CONNECTION to " + hostName + ":" + port   );
  		}
		} catch (Exception e) {
//			println("SenderObject " + name + " send/1 ERROR " + e.getMessage()   );
			connectionTable.remove(hostName+port);
//			ctx.ctxTable.remove(name);
			conn = null;
 			throw e;
		}									
	}
	protected boolean setConn( ){
		try {
//  			println("SenderObject " + name + " try conn " +  protocol + " " + hostName + " " + port);
			IConnInteraction conn = factoryP.createClientProtocolSupport(hostName, port);
//  			println("SenderObject " + name + " set  conn to " +   hostName + port);
			connectionTable.put(hostName+port, conn);
			return true;
		} catch (Exception e) {
//			 println("SenderObject setConn ERROR "  + e.getMessage() );
		}				
		return false;
 	}
	/*
	 * Called by ActorContext
	 */
	public String receiveWakeUpAnswer() throws Exception{
		String line = conn.receiveALine();
		println(" *** SenderObject receiveWakeUpAnswer " + line );
		return line;
	}
}
