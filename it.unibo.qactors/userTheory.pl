testConsult :- output("userTheory works").

select( FileName ) :- 
/*
-----------------------------------------
ACCESS TO JAVA OBJECT INSTANCES
-----------------------------------------
*/ 
	java_object('javax.swing.JFileChooser', [], Dialog),
	Dialog <- showOpenDialog(_),
	Dialog <- getSelectedFile returns File,
	File   <- getPath returns Path,
/*
-----------------------------------------
ACCESS TO CLASS STATIC OPERATION
-----------------------------------------
*/  	
	class("it.unibo.qactors.QActorUtils") <- adjust( Path ) returns FileName. 		 

welcome :-  actorPrintln("welcome from userTheory.pl").
:- initialization(welcome).