package it.unibo.qactors.action;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.platform.EventHandlerComponent;
 
 
public class ActionInterruptEventHandler extends  EventHandlerComponent{
protected ActorAction action;
protected ActionTimer at;

	public ActionInterruptEventHandler(String name, ActorContext myctx, ActorAction action, ActionTimer at,
			String[] events, IOutputEnvView view) throws Exception {
		super(name, myctx, events, view);
		this.action = action;
		this.at     = at;
// 		println("ActionInterruptEventHandler events " + events.length );
		//the component auto-register
 	}

	@Override
	public void doJob() throws Exception {
//			currentEvent = getEventItem();
//	  		 println("ActionInterruptEventHandler &&& REACTS to " + currentEvent  );
//			if( currentEvent == null ) return;
//	showMsg( "---------------------------------------------------------------------" );	
//	showMsg( currentEvent.getPrologRep()  );				 
//	showMsg( "---------------------------------------------------------------------" );	
//	  		 println("ActionInterruptEventHandler &&& REACTS to " + currentEvent.getEventId() + " for: " + action.getActionName() );	 
// 			unregister();
//		 if( at != null) at.interrupt();
//		 action.suspendAction();
	}

	public IEventItem getEvent(){  
		return currentEvent;
	}

	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );		
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " RECEIVES QActorMessage " + msg.getDefaultRep() );	
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}
}
