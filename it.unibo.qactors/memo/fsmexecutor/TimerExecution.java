package it.unibo.qactors.fsmexecutor;
import java.util.Calendar;

public class TimerExecution  {
	private long tStart  = 0;
	private long durationMillis;
	
	public TimerExecution( long durationMillis ) throws Exception{
		this.durationMillis = durationMillis;
		tStart = Calendar.getInstance().getTimeInMillis();
	}
	public long getElapsedTime() {
		long tEnd = Calendar.getInstance().getTimeInMillis();
		return  tEnd - tStart ;		
	}	
	public long getRemainingTime() {
		long dt = durationMillis - getElapsedTime();
		return dt < 0 ?   0 :   dt ;		
	}	
	public String getTimeRep( long exectime ){
  		long hr = 0;
 		long min = 0;
 		long sec = 0;
 		long msec = 0;
 		if( exectime < 1000  ){
 			msec = exectime;
  		}else if( exectime < 60 * 1000  ) {
  			sec = exectime / 1000 ;
  			msec = exectime - sec * 1000;
  		}
 		return exectime + "|" + hr+":"+min+":"+sec+":"+msec;
	}
	
}
