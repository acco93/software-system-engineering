package it.unibo.qactors.fsmexecutor;
import it.unibo.qactors.action.IActorAction;

public class StateAction  {
private TaskActionFSMExecutor tce;
private String msg;
private  Boolean terminating;
private int restOfTime;
private String answer;
private IActorAction action;

	public StateAction(IActorAction action, TaskActionFSMExecutor tce, String msg, Boolean terminating, int restOfTime, String answer){
		this.action = action;
		this.tce = tce;
		this.msg = msg;
		this.terminating = terminating;
		this.restOfTime = restOfTime;
		this.answer = answer;
	}
 
	public void call( ) {
		try {			
			if(terminating) action.waitForTermination();
// 			System.out.println("StateAction " + action.getActionRep() + " terminating=" + terminating + " msg=" + msg + " tce=" + tce);
			tce.endOfAction(msg,terminating,restOfTime,answer);
		} catch (Exception e) {
 			e.printStackTrace();
		}
 	}

	public String toString(){
		return "action("+action.getActionName()+","+ restOfTime + ")";
	}
}
