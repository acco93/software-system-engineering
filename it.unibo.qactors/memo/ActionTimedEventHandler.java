package it.unibo.qactors.action;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.platform.EventHandlerComponent;
 
public class ActionTimedEventHandler extends EventHandlerComponent{
protected ActorTimedAction action;
//protected ActionTimer at;

	public ActionTimedEventHandler(String name, ActorContext myctx, ActorTimedAction action, 
//			ActionTimer at,
			String[] events, IOutputEnvView view) throws Exception {
		super(name, myctx, events, view);
		this.action = action;
//		this.at     = at;
//  	  	println("	*** ActionTimedEventHandler " + getName() + " CREATED " );
//  	    showEvents();
	} 
  	@Override
	public void doJob() throws Exception {		
	}
 	protected void showEvents(){
		int n = this.events.length;
		for( int i = 0; i < n; i++ ){
			println("	*** ActionTimedEventHandler event " + events[i]  );
		}
	}
 	
	protected void handleQActorEvent(IEventItem ev)   {
		try{
			println("	*** ActionTimedEventHandler " +getName() + " EVENT " + ev.getDefaultRep() + " action suspended="+ action.suspended );		
			if( action == null || ! action.suspended ) return;
// 			if( at != null) at.interrupt(); 	//interrupt the timer (sleeping ..)
			currentEvent = ev; //getEventItem(); //TODO
//	 	  	println("	*** ActionTimedEventHandler " + getName() + " handleEvent currentEvent=" + currentEvent);
			if( currentEvent != null ){
//	  	  		println("	*** ActionTimedEventHandler " + getName() + " REACTS to " + 
//	  	  			currentEvent.getEventId() + " action=" + action.getName() + " terminate=" + action.actionTerminated  );
	  	  		action.setInterruptEvent(currentEvent);	//inject  in action
//	  			if( ! action.actionTerminated )   action.suspendAction(); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
 
	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );
	}
	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " RECEIVES QActorMessage " + msg.getDefaultRep() );	
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}
	
}
