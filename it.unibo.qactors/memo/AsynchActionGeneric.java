package it.unibo.qactors.action;
import it.unibo.is.interfaces.IOutputEnvView;

/*
 * -------------------------------------------------------------------------
 * 					AsynchActionGeneric
 * Author: AN DISI
 * Goal:     TODO
 * 			
 * Usage:  
 * 			if answerEvId is empty("") it emits terminationEvId at the end
 * 			if answerEvId is not empty it emits IMMEDIATEDELY terminationEvId and answerEvId at the end
 * 		execSynch:		executes the action in synchronous way (waits for termination)
 * 		execAsynch:		executes the action in asynchronous way
 * 		suspendAction: 	suspends the execution of the action if not already terminated
 * 		waitForTermination: waits until the action is terminated (mainly for execAsynch)
 *  	getExecMode:	returns an instance of ActionRunMode (termination with answer or not)
 * 
 * Implementation details:
 * 		Callable<T>, Future<T>, SituatedSysKb.executorManyThread
 *  	execTheAction: 	protected abstract operation that defines the action behavior
 *      getApplicationResult: protected abstract operation that defines the result (of type T9 of the action
 *  	getResult:		returns an instance of AsynchActionGenericResult<T> the wraps the application
 *  					result of the action into a structure that gives another information about the action
 *  					(interrupted or not, execution time remained)
 *  					
 * -------------------------------------------------------------------------
 */

public abstract class AsynchActionGeneric<T> extends ActionObservableGeneric<AsynchActionGenericResult<T>> {
//							implements IAsynchAction<AsynchActionGenericResult<T>>{ 
	protected String arg; 
// 	protected boolean actionTerminated = false;
//	protected String answerEvId = "";
	protected String answer     = "";
//	protected boolean cancompensate;
//	protected  int maxduration;
//	protected boolean suspended = false;
//	protected String emptys = "";
//	protected int timeRemained = 0;
	
	public AsynchActionGeneric(String name,  
			String terminationEvId, IOutputEnvView outEnvView ) throws Exception {
		super(name, terminationEvId, outEnvView);
// 		this.answerEvId    = answerEvId.trim();
//   		println("	%%% AsynchActionGeneric " + name + " answerEvId=" + answerEvId + " terminationEvId=" + terminationEvId + " "+ maxduration);
//		this.cancompensate = cancompensate;
// 		this.maxduration   = (int) maxduration;
 	}
/*
 *  START
*/
//	@Override
//	protected void startOfAction() throws Exception {
//		super.startOfAction();
////		suspended = false;  
////		if( answerEvId.length() > 0){
////			emitEvent(terminationEvId, "immediateEnd");	//terminates immediately
////		}
////		println("%%% AsynchActionGeneric " + getName() + " startOfAction myself="  + myself.getName());
//	}
/*
 * EXECUTE
 * To be defined by the application designer
 */
//	@Override
//	protected abstract void execTheAction() throws Exception;
//	@Override
	protected abstract T getApplicationResult() throws Exception;
/*
 *  END OF ACTION
 */
	@Override
	protected AsynchActionGenericResult<T> endActionInternal() throws Exception{
//   	   	println("%%% AsynchActionGeneric endActionInternal terminationEvId=" + terminationEvId + " answerEvId=" + answerEvId   );
		evalDuration();
    	AsynchActionGenericResult<T> res = endOfAction();
//   	   	println("%%% AsynchActionGeneric endActionInternal terminationEvId=" + terminationEvId + " answerEvId=" + answerEvId   );
//    	if( answerEvId.length() > 0  ){
////      	   	println("%%% AsynchActionGeneric endActionInternal answerEvId=" + answerEvId + " durationMillis=" + durationMillis   );
//      	   	emitEvent(answerEvId, ""+res.getResult());
//    	}
//    	else 
    		if( terminationEvId.length() > 0) {
//      	   	println("%%% AsynchActionGeneric endActionInternal terminationEvId=" + terminationEvId + " durationMillis=" + durationMillis   );
			emitEvent(terminationEvId, ""+res.getResult());
     	}
// 		actionTerminated = true;
   	    return res;
	}
//    protected void evalDuration(){
//		if( durationMillis == -1 ){
//			 long tEnd = Calendar.getInstance().getTimeInMillis();
//			 durationMillis =  tEnd - tStart ;	
//// 			 println("	%%% ActionObservableGeneric " + getName() + " duration="  +  durationMillis);
//		}    	
//    }

 	@Override
	protected AsynchActionGenericResult<T> endOfAction() throws Exception {
 		return getResult();
	}

	protected AsynchActionGenericResult<T> getResult() throws Exception {
  		println( "	%%% AsynchActionGeneric  " + getName() + " getResult durationMillis=" + durationMillis  );
		AsynchActionGenericResult<T> aar;
//		println( "%%% AsynchActionGeneric  " + getName() + " getResult timeRemained= " + aar.timeRemained );
//		if( answerEvId.length()>0 ){
//			aar= new AsynchActionGenericResult<T>( this, getApplicationResult(), durationMillis, this.suspended );
//		}else
		aar= new AsynchActionGenericResult<T>(this, getApplicationResult(), durationMillis, false );
		return aar;
	}
/*
* ======================================================================== 
* METHODS
* ======================================================================== 
*/
 
	/*
	@Override 
	public AsynchActionGenericResult<T> execSynch() throws Exception   { 
// 		if( this.suspended ) {
////			throw new Exception("The action " + this.getActionName() + " is  suspended before starting"); //an interrupt could arrive before the action is started
//			println("%%% AsynchActionGeneric " + getName() + " WARNING: suspended before starting "  );
//			this.suspended = false;
// 		}
		suspended = false;
 		Future<AsynchActionGenericResult<T>> fResult = activate();
// 		println("%%% AsynchActionGeneric " + getName() + " execSynch ACTIVATES "  + fResult );
		AsynchActionGenericResult<T> res = fResult.get();
//		println("%%% AsynchActionGeneric " + getName() + " execSynch res= " + res);
		return res;
 	}	
	@Override
	public Future<AsynchActionGenericResult<T>> execASynch() throws Exception {
		Future<AsynchActionGenericResult<T>> fResult = activate();
		return fResult;
 	}
	*/
	/*
	* ======================================================================== 
	* (BEAN) METHODS
	* ======================================================================== 
	*/
//	@Override 
//	public void setTheName(String name){
//		this.name=name;
//	}
//	@Override
//	public void setMaxDuration(int d){
//		maxduration =  d;
//		suspended = false; //the action is resumed => it is no more interrupted
//	}
//	@Override
//	public void setAnswerEventId(String evId){
//		answerEvId = evId;
//	}
//	@Override
//	public void setCanCompensate(boolean b){
//		cancompensate = b;
//	}
//	@Override
//	public void setTerminationEventId(String evId){
//		terminationEvId = evId;
//	}
//	@Override
//	public String geAnswerEventId(){
//		return answerEvId;
//	}
//	@Override
//	public boolean canBeCompensated(){
//		return cancompensate;
//	}
//	@Override
//	public boolean isSuspended(){
//		return suspended;
//	}
//	public void showMsg(String msg){
//		this.println(msg);
//	}
// 	@Override
//	public String getActionName() {
//		return name;
//	}
//	@Override
//	public int getMaxDuration() {
// 		return this.maxduration;
//	}
// 	@Override
//	public IEventItem getInterruptEvent(){  
//		return null;
//	}

// 	@Override
//	public ActionRunMode getExecMode() {
// 		if( answerEvId.length() == 0) return ActionRunMode.terminationEventAtEnd;
// 		else return ActionRunMode.terminationEventImmediate;
//	}
/*
 * 	REPRESENTATION
 */
// 	@Override
//	public String getActionAndResultRep() throws Exception{
//// 	AsynchActionGenericResult<T> result = fResult.get();
// 		AsynchActionGenericResult<T> result= getResult(); //WITS FOR RESULT
// 		return "action(NAME,TERMINATION,DURATION, RESULT, CANCOMPENSATE, ANSWEREVENT, TERMINATED, SUSPENDED, ACTIONROUT)".
//				replace("NAME",  getName()).
//				replace("TERMINATION", terminationEvId ).
//				replace("DURATION", ""+durationMillis ).
// 				replace("RESULT", "result('"+result+"')").
//				replace("CANCOMPENSATE", "compensate("+this.cancompensate+")").
// 				replace("ANSWEREVENT", "answerEv('"+ (answerEvId.length()>0 ? answerEvId : "noevent") +"')").
////				replace("MAXTIME", "maxduration("+this.maxduration+")").
//				replace("TERMINATED", "actionTerminated("+this.actionTerminated+")").
//				replace("SUSPENDED", "suspended("+this.suspended+")").
//				replace("ACTIONROUT", "answer('"+this.answer+"')") 
//				;
//	}
//	@Override
//	public String getActionRep()  {
//		return "action(NAME,TERMINATION,CANCOMPENSATE,ANSWEREVENT )".
//				replace("NAME",  getName()).
//				replace("TERMINATION", terminationEvId ).
//				replace("CANCOMPENSATE", "compensate("+this.cancompensate+")").
// 				replace("ANSWEREVENT", "answerEv("+ (answerEvId.length()>0 ? answerEvId : "noevent") +")")
////				replace("MAXTIME", "maxduration("+this.maxduration+")")
// 				;
//	}	 
//	@Override
	public String getResultRep() {
 		try {
			return getApplicationResult().toString();
		} catch (Exception e) {
 			return "unknown";
		}
	}

/*
 * =========================================================================== 
 * Called by another thread
 * ===========================================================================
 */
//	@Override 
//	public AsynchActionGenericResult<T> waitForTermination() throws Exception{
// 		return fResult.get();
//	}
//	@Override 
//	public void suspendAction(){
// 			println("	%%% AsynchActionGeneric " + getName() + " INTERRUPTS  " + myself );		 
////    		suspended = true;  
//    		if(myself !=null ){
//       			myself.interrupt();  
//    		}
//	}
}
