package it.unibo.qactors.action;

public class AsynchActionGenericResult<T> {
protected AsynchActionGeneric<T> action;
protected long execTime ;
protected  T result;
protected  boolean suspended; 
	public AsynchActionGenericResult( AsynchActionGeneric<T> action, T result, long execTime, boolean suspended){
		this.action		= action;
		this.result     = result;
		this.execTime   = execTime;
		this.suspended  = suspended;
	}
	public long getTimeRemained(){
		return execTime;
	}
	public T getResult(){
		return result;
	}
	public boolean getInterrupted(){
		return suspended;
	}
	public void  setResult(T result){
		this.result = result;;
	}
	@Override
	public String toString(){
		String execTime=""+action.getExecTime();
//		String maxTime =""+action.getMaxDuration();
		return "asynchActionResult(ACTION, RESULT,SUSPENDED,TIMES)".
				replace("ACTION","action("+action.name+")").
				replace("RESULT","result("+result+")").
				replace("SUSPENDED","suspended("+suspended+")").
//				replace("TIMES","times(exec("+execTime+"),max("+maxTime+"))");
				replace("TIMES"," exec("+execTime+")");
	}
}
