package it.unibo.qactors.action;

import it.unibo.contactEvent.interfaces.IEventItem;

public class TaskActionFSMExecutoResult {
protected IEventItem event;
protected String planTodo;
protected  long moveTimeNotDone; 
	public TaskActionFSMExecutoResult(IEventItem eventId, String planTodo,  long moveTimeNotDone){
		this.event = eventId;
		this.planTodo = planTodo;
		this.moveTimeNotDone = moveTimeNotDone;
	}	
	public IEventItem getEventItem(){
		return event;
	}
	public String getPlanTodo(){
		return planTodo;
	}
	public  long getMoveTimeNotDone(){
		return moveTimeNotDone;
	}
}
