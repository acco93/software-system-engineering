package it.unibo.qactors.akka.action;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.CtxMockAkka;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

import akka.actor.ActorSystem;
import akka.actor.Actor;
 

public class TestQActorActions  {
 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
 private ActorContext ctx;
 
	/*
	 * A system in which:
 	 */
 
	@Before
	public void setUp() throws Exception{
		ctx = ActorContext.initQActorSystem("ctxactiontimed", 
				"./test/it/unibo/qactors/akka/action/simple/actiontimed.pl", stdOut);
		assertTrue("setUp" , ctx != null );		
		qa = QActorUtils.getQActor("dummyactor");
		assertTrue("setUp" , qa != null);			
//		pengine = qa.getPrologEngine();
//		assertTrue("setUp" , pengine != null);
	}
	 @After
	 public void terminate(){
			actionEndHandler.terminate();
			qa.terminate();
			QActorUtils.terminateTheQActorSystem();
			ActorContext.terminateQActorSystem();
	 }
 
//   	@Test
	public void testSound() {
		try {
			ActorContext.initQActorSystem("ctxAction", 
					"./test/it/unibo/qactors/akka/action/actionSystemKb.pl", stdOut);
	 		Thread.sleep(15000);  

		} catch (Exception e) {
			fail("testSound " + e.getMessage()   );
		}		
	}
	
	/*
	 * A system in which:
	 * 	an actor client send a request to an actor player to play a sound
	 *  the client waits until the sound is terminated (synch)
	 *  
	 *  an actor perceives the event 'playsound' and plays
	 */
// 	@Test
	public void actorMsgTest() { //TO CHECK INTERFERENCE
		try {			
			ActorContext ctx = ActorContext.initQActorSystem("ctxAction", 
 					"./test/it/unibo/qactors/akka/action/actionSystemKb.pl", stdOut);
			assertTrue("	*** actorMsgTest" , ctx != null);

// 			ActorRef player = QActorUtils..getQActorRef("qaplayer");
//			System.out.println("	*** player=" + player);
			/*
			 * 			assertTrue("actorMsgTest" , player != null);
			ActorRef client = ctx.getQActorRef("qaclient");
			assertTrue("actorMsgTest" , client != null);
			String msgContent = "playAsynchronous";
			QActorMessage msg = QActorUtils.buildMsg(
					ActorContext.getSelf(), "qaclient", "play", "qaplayer", ActorContext.request, msgContent);
			assertTrue("actorMsgTest" , msg.msgContent().equals(msgContent));
 			player.tell(msg, client);
 			*/
			QActor qa = QActorUtils.getQActor("qaclient");
			System.out.println("	*** qa=" + qa);
 			
			String result = qa.askMessageWaiting("qaplayer", "playSynchronous", 3000);
 			System.out.println("	*** result=" + result);
//			msgContent = "playSynchronous";
//			msg = QActorUtils.buildMsg(
//					ActorContext.getSelf(), "qaclient", "play", "qaplayer", ActorContext.request, msgContent);
//			assertTrue("actorMsgTest" , msg.msgContent().equals(msgContent));
//			player.tell(msg, client);
//	 		Thread.sleep(4000);  //when terminates, it kills the system
		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
			fail("actorMsgTest " + e.getMessage() );
		}		
	}

// 	@Test
// 	FROM NETWORK
// 	public void demonstrateTestActorRef() {
//	 	final Props props = Props.create(it.unibo.qactors.akka.action.QActorActionSound.class);
//	 	final TestActorRef<QActorActionSound> ref = TestActorRef.create(system, props, "testA");
//	 	final QActorActionSound actor = ref.underlyingActor();
////	 	assertTrue(actor.testMe());
// 	}
}
