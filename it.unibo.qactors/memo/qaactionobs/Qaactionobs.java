/* Generated by AN DISI Unibo */ 
/*
This code is generated only ONCE
*/
package it.unibo.qaactionobs;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Panel;

import akka.actor.ActorContext;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.action.ActionFibonacciTimed;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.akka.QActor;

public class Qaactionobs extends AbstractQaactionobs { 

//	protected ActionNoFsm actionNoFsm;
	protected Checkbox endUImmeduateCkb, synchCkb , interruptCkb, withActionEvHandleCkb; //, continueWorkCkb;
	protected  boolean withActionInterrupt     = true;
	protected  boolean withActionEventHandler  = true;
	protected  boolean terminateImmediately    = true;
	protected  boolean synchMode     		   = true;	
	protected String maxTimeBt         = "maxtime";
	protected String runWithFSMBt      = "run";
	protected int maxduration          = 6000;
 	protected String  interruptHandle  =  "reactionToUsercmd";	
	protected String  goalTodo		   = "fibo(24,V)";
	protected String actionAnswerEv    = "" ;

	public Qaactionobs(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
		super(actorId, myCtx, outEnvView);
	}
	public void createAppplicationInterface(String goalTodo, int maxduration){
		this.goalTodo    = goalTodo;
		this.maxduration = maxduration;
		outEnvView.getEnv().addInputPanel(10);
		outEnvView.getEnv().addCmdPanel("cmd", new String[]{ maxTimeBt }, this);
		addCustomPanel();
		outEnvView.getEnv().addCmdPanel("", new String[]{ runWithFSMBt }, this);
	}		
	protected void addCustomPanel(){
		Panel p = new Panel( );
		p.setBackground(Color.yellow);
		synchCkb              = new Checkbox("synch", true); 
 		endUImmeduateCkb      = new Checkbox("endImmediate"); 
		interruptCkb          = new Checkbox("interrupt");
		withActionEvHandleCkb = new Checkbox("actionEvHandle",true);
//		continueWorkCkb       = new Checkbox("goon",true);
		p.add( endUImmeduateCkb );
		p.add( synchCkb );
		p.add( interruptCkb );
		p.add( withActionEvHandleCkb );
//		p.add( continueWorkCkb );
		outEnvView.getEnv().addPanel( p );		
	}	
 	 /*
	  * ENTRY for all the button clicks (IActivity)
	  */
	@Override
	public void execAction(String cmd) {
		try {
	 		terminateImmediately   = endUImmeduateCkb.getState() ;
	 		withActionInterrupt    = interruptCkb.getState() ;
	 		withActionEventHandler = withActionEvHandleCkb.getState() ;
	 		synchMode              = synchCkb.getState();
 			if( cmd.equals(maxTimeBt)){
 				setDuration();
			}else
				if( cmd.equals(runWithFSMBt)){
				IActorAction aAction =  createTheAction( myCtx, this, goalTodo, outEnvView,		 					
						maxduration,  terminateImmediately, withActionEventHandler );
	  			runTheAction(aAction);
//	  			println(" runTheAction done" );
	 		} 
		} catch (Exception e) {
			e.printStackTrace();
		}	
		}
	protected void setDuration(){
		try{
			maxduration = Integer.parseInt( ((EnvFrame) env).readln() );
			this.solveGoal("setMaxActionTime("+maxduration+")", 0, "", "", "");
			SolveInfo sol = this.pengine.solve("actiontodo( G, T )." );
			println(" " + sol.getSolution());
		}catch(Exception e) {
			println("duration error");
		}		
	}
	
	
 	public IActorAction createTheAction(ActorContext myCtx, QActor actor, String goalTodo, IOutputEnvView outEnvView,
		int maxduration, boolean  terminateImmediately, boolean withActionEventHandler) throws Exception{
 		IActorAction action;
 		String terminationEvId = IActorAction.endBuiltinEvent+nameCount++;
 		boolean cancompensate = false;
		if( terminateImmediately ){
			actionAnswerEv = "actionAnswer"+ nameCount++;
//			action = new ActionReactiveFibonacci("actionFibo", actor, goalTodo, cancompensate, terminationEvId, 
//					actionAnswerEv, outEnvView, maxduration  );  		
			action = new ActionFibonacciTimed("actionFibo", this, goalTodo,
					cancompensate, terminationEvId, actionAnswerEv, new String[]{interruptEv}, outEnvView,
					maxduration);
		}else{
			actionAnswerEv="";
//			action = new ActionReactiveFibonacci("actionFibo", actor, goalTodo, cancompensate, terminationEvId, 
//					"", outEnvView, maxduration  );  	
			action = new ActionFibonacciTimed("actionFibo",  this, goalTodo,
					cancompensate, terminationEvId, "", new String[]{interruptEv}, outEnvView, maxduration);
		}
//		if( withActionEventHandler )
//			new TerminationHandler("th", myCtx, new String[]{terminationEvId,interruptEv,actionAnswerEv}, outEnvView, action);
		return action;
	}
 	

}
