package it.unibo.qactors.action;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedActiveObject;

/*
 *  
 */
public class ActionTimedTimer extends UntypedActor{
protected int execTime ;
protected String toutEventId;
protected IOutputEnvView outEnvView ;
protected String name;
protected ActorRef actionevHandler;

	public ActionTimedTimer(String name, IOutputEnvView outEnvView, int execTime , ActorRef actionevHandler ){
		this.name        		= name;
		this.outEnvView  		= outEnvView;
 		this.execTime    		= execTime;
		this.actionevHandler    = actionevHandler;
//		outEnvView.addOutput("%%% ActionTimer " + name + " CREATED " +  execTime    );
 	}
	
	@Override
	public void preStart() {
		//doJob();
		getSelf().tell("START", getSelf());
	}

 	protected void doJob()   {
 		try {
//  			outEnvView.addOutput("	+++ ActionTimer " + name + " STARTS " +  execTime    );
 			Thread.sleep( execTime  ); //BLOCK all the computation if executed in preStart
 			String m = "tout("+execTime+")";
// 			outEnvView.addOutput("	+++ ActionTimer " + name + " ENDS and raiseEvent timeOut "      );
 			/* 
 			 * We do not generate an event to be handled by the eventloop
 			 * Rather, we send a message directly to the action event handler
 			 */
//			QActorUtils.raiseEvent(getName(), toutEventId, m );
   			IEventItem ev = QActorUtils.buildEventItem(  name, "timeOut", m  );
//   			outEnvView.addOutput("	+++ ActionTimer " + name + " tell " +  ev    );
 			actionevHandler.tell(ev, getSelf() );
 		} catch (Exception e) {
 			outEnvView.addOutput( "	+++ ActionTimer " + name + " doJob has been interrupted " );
  		} 		
	}

 	@Override
	public void onReceive(Object arg0) throws Throwable {
 		doJob();
	}
 	
 
}
