package it.unibo.qactors.action;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IActorMessage;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
//import it.unibo.qactors.web.GuiUiKb;

public class ActionReceiveAsynch extends ActorAction { 
protected static int n = 1;
protected  QActor actor;
protected  String msg = null;
protected String msgTermStrToReceive;
protected Prolog pengine;
protected Term termToReceive = null;
protected boolean msgReceived;
protected boolean timeOut;
private ActionTimer at;
private ActorContext myctx; 
private ActionInterruptEventHandler evh;
private String[] alarms ;
private String[] events ;

public ActionReceiveAsynch(String name, QActor actor, ActorContext myctx, 
		String[] alarms, String msgTermStrToReceive, IOutputEnvView outView,
		long maxduration ) throws Exception {
	super(name, false, QActorUtils.locEvPrefix+"EndEv"+n++, "", outView, maxduration);
	this.actor            = actor;
	this.myctx    = myctx;
	this.alarms   = alarms;
	init();
	/*
	 * msgTermToReceive could be:
	 * 	msg( MID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM  )
	 *  or
	 *  [mid1, ..., midn]
	 */
	this.msgTermStrToReceive = msgTermStrToReceive;
	pengine                  = actor.getPrologEngine();
	msgReceived              = false;
//	timeOut 				 = false;
	if( ! msgTermStrToReceive.trim().equals("null")){
		this.msgTermStrToReceive = msgTermStrToReceive;
		termToReceive         = Term.createTerm(msgTermStrToReceive);
	}
//	println("--- ActionReceiveAsynch msgTermStrToReceive= "  + msgTermStrToReceive );
}
	protected void init() throws Exception{
 		String toutevId = getName()+"tout";
 		events = new String[alarms.length + 1];
 		if( alarms.length > 0 ){
 			System.arraycopy(alarms,0,events,0,alarms.length );
  			System.arraycopy(new String[]{toutevId},0,events,alarms.length,1 );
   		}
 		else events = new String[]{toutevId};
  		at  = new ActionTimer(getName()+"Timer",this.outEnvView, maxduration,toutevId);
 	}
	public String getReceivedMsg() throws Exception{
		if( timeOut ) return "msg(none,none,none,none,none,0)";
		removeTheMessage( termToReceive.toString() );
		return termToReceive.toString(); 
	}
	@Override
	protected void execTheAction() throws Exception {
	 try{
 		 //Inspect the world 		 			
		 if( checkAMsgInWorld() ) return;
		 //Inspect the actor message queue
		 if( removeAMsg( ) ) return;
		 at.start();
	 	 evh = new ActionInterruptEventHandler(getName()+"Evh", myctx, this, at, events, outEnvView);		
		 while( ! msgReceived ){
//			println("--- ActionReceiveAsynch execTheAction waits for a message for:" +  actor.getName()  );
//TODO			msg = actor.receiveMsg();
//  			println("--- ActionReceiveAsynch execTheAction msg=" + msg  ); 		 		 			
			if( handleMsg( msg ) ){
				msgReceived = true;
			};		 		 			
		 }//while	  				
  		 at.interrupt();	 			
//		 evh.unregister();	 			
   	 }catch(Exception e){
// 		 println("--- ActionReceiveAsynch EXIT " +  e.getMessage() );
		 this.timeOut = true;
//TOUT		 String rule = "tout(receive,"+ getName()+")";
//		 pengine.solve( "addRule( " + rule + " ).");
	 }
    } 
 	
	protected boolean checkAMsgInWorld( ) throws Exception{
		SolveInfo sol = pengine.solve("msg( MID, MSGTYPE, SENDER, RECEIVER, CONTENT, SEQNUM  ).");
//    	println("--- ActionReceiveAsynch checkAMsgInWorld " +  sol.isSuccess() );
		if( sol.isSuccess() && 
			(termToReceive==null || pengine.unify(sol.getSolution(), termToReceive) )){
//			removeTheMessage(msgTermStrToReceive);
//			println("--- ActionReceiveAsynch checkAMsgInWorld msgTermStrToReceive=" + msgTermStrToReceive + " termToReceive="+ termToReceive );
			msgReceived = true;
			return true;
		}
		return false;
	}
	/*
	 * Removes FIFO all the messages (and puts them in the actor's world theory)
	 * from  the msgqueue  until the required one is found (it it exists)
	 */
	protected synchronized boolean removeAMsg( ) throws Exception{
		//TODO
//		while( actor.getQueueSise()>0 ){
//			msg = actor.receiveMsg(); //blocking => not reactive REMOVES the message
//			if( handleMsg(msg) ){
//				msgReceived = true;
//				return true ;
//			}
// 		}
//		println("--- ActionReceiveAsynch removeAMsg fails with msgqueue size=" +  actor.getQueueSise() );
		return  false ;
	}
  
	protected  boolean handleMsg(String msg){ //to avoid re-rentrance by  ExecReceive
		Term msgTerm = null;
		try{
//  			println("--- ActionReceiveAsynch handleMsg1 "  +  msg  );	
	 		if( msg == null ) return false;
//TODO 	 		msg = GuiUiKb.buildCorrectPrologString(msg);
//			println("--- ActionReceiveAsynch handleMsg2 "  +  msg  );	
  			IActorMessage m = new QActorMessage(msg); //to check the syntax 			
  			addTheMessageInWorldTheory( msg );		//puts the removed messages in the actor world theory
 			msgTerm = Term.createTerm(msg);			 
//  			println("--- ActionReceiveAsynch handleMsg msgTerm="  +  msgTerm  );	
//  			println("--- ActionReceiveAsynch handleMsg termToReceive="  +  termToReceive + " " + pengine.unify(msgTerm, termToReceive)  );	
			return( termToReceive==null || checkIfMessageExpected(msgTerm, termToReceive) ) ;
  		}catch( Exception e){
			println("--- ActionReceiveAsynch ERROR "  +  e.getMessage() + " msg=" + msg + " msgTerm=" + msgTerm);	
			return false;
		}
	}
	
	protected boolean checkIfMessageExpected(Term msgTerm, Term termToReceive) throws Exception{
		if( termToReceive.isList() ){
			SolveInfo sol = pengine.solve( "checkMsg("+ msgTerm +"," + termToReceive + ")." );
 			return sol.isSuccess();
		}
		else return pengine.unify( msgTerm, termToReceive );
	}
	
	protected void addTheMessageInWorldTheory( String msg  ){
 		try{
//  			println("addTheMessage:" + msg  );
 			msg = msg.trim();
 			if( msg.equals("true")) return;
//  			SolveInfo sol = 
  					pengine.solve( "addRule( " + msg + " ).");
//  			println("			--- ActionReceiveAsync addTheMessageInWorldTheory done " + msg  );
   		}catch(Exception e){
  			println("addTheMessageInWorldTheory " + msg + " ERROR:" + e.getMessage() );
   		}
   	}
	protected  void removeTheMessage( String msg  ){
 		try{
// 			if( msg.startsWith("'") || msg.startsWith("\"")) 	msg = msg.substring(1, msg.length()-1);
//  			println("removeTheMessage:" + msg  );
 			msg = msg.trim();
 			if( msg.equals("true")) return;
//  			SolveInfo sol = 
  					pengine.solve( "removeRule( " + msg + " ).");
// 			println("removeTheMessage done " + msg  );
   		}catch(Exception e){
  			println("removeTheMessage " + msg + " ERROR:" + e.getMessage() );
   		}
   	}

	@Override
	public void suspendAction(){
		if( msgReceived ) return;
//		IEventItem ev = evh.getEvent();
// 		println("%%% ActionReceiveAsynch " + getName() + " suspendAction " + ev  + " " + myself  );
		myself.interrupt();
 	}
 	@Override
	protected String getApplicationResult() throws Exception {
//		println("--- ActionReceiveAsynch getApplicationResult = " + msg );	
 		return timeOut ? "tout" : msg;
	}
 	
 }
