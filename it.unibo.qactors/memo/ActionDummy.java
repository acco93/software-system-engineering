package it.unibo.qactors.action;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorUtils;
   
/*
 * Each ActionDummy action must generate a different termination event
 */
public class ActionDummy extends ActorAction  {
private static int ncount = 0;
private ActorContext myctx; 
private ActionInterruptEventHandler evh;
private String[] alarms ;
private String myresult = "unknown";

public ActionDummy(ActorContext myctx, String[] alarms, IOutputEnvView outView, int maxduration ) throws Exception {
	super("adummy"+getCounter(), false, QActorUtils.locEvPrefix+"dmyend"+getCounter(), "", outView, maxduration); //adterminate never emitted null IS A MUST
	this.myctx    = myctx;
	this.alarms   = alarms;
	init();
}

 	protected void init() throws Exception{
 		String toutevId = getName()+"tout";
 		String[] events = new String[alarms.length + 1];
 		if( alarms.length > 0 ){
 			System.arraycopy(alarms,0,events,0,alarms.length );
  			System.arraycopy(new String[]{toutevId},0,events,alarms.length,1 );
   		}
 		else events = new String[]{toutevId};
// 		at  = new ActionTimer(getName()+"Timer",this.outEnvView,this.maxduration,toutevId);
 		evh = new ActionInterruptEventHandler(getName()+"Evh", myctx, this, null, events, outEnvView);		
 	}
	public static synchronized int getCounter() throws Exception{
// 		System.out.println("=== ActionDummy " + ncount);
 		ncount++;
		return ncount;
	}
 	@Override
	protected void execTheAction() throws Exception {
 		try{
// 			println("=== ActionDummy sleeps " + maxduration);
 			Thread.sleep(getMaxDuration());
// 			evh.unregister();
 	   	 }catch(Exception e){
// 			println("%%% ActionDummy " + getName() + " EXIT " +  e.getMessage() );
 		 }
	}
	@Override
	protected String getApplicationResult() throws Exception {
//		println("%%% ActionDummy "+getName() +  " getApplicationResult "  );
		if( this.isSuspended() ) return myresult;
		else return "timing(" + getName() + "," + durationMillis + "," + timeRemained +")";
	}
 
	public void suspendAction(){
		IEventItem ev = evh.getEvent();
//		println("%%% ActionDummy " + getName() + " suspendAction " + ev + " " + myself  );
		if(ev!=null){
			myresult ="interrupted("+ev.getEventId()+")";			
			super.suspendAction();
		} 
		else{
			myself.interrupt();
		}
	}

	@Override
	public int getMaxDuration() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setMaxDuration(int d) {
		// TODO Auto-generated method stub
		
	}
}
