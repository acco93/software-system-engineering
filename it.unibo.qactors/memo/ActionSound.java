package it.unibo.qactors.action;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.ActorAction;
 
/*
 * ActionSound new version
 */
public class ActionSound extends ActorAction{
protected String fName;
protected Clip clip = null;
 	public ActionSound(String name, boolean cancompensate,
			String terminationEvId, String answerEvId, IOutputEnvView outView,
			long maxduration, String fName) throws Exception {
		super(name, cancompensate, terminationEvId, answerEvId, outView, maxduration);
		this.fName = fName.trim();
		if( fName.startsWith("'")) this.fName = fName.substring(1,fName.length()-1);
    	println("	%%% ActionSound CREATED " +  fName + " time=" + maxduration + " answerEvId=" + (answerEvId.length()==0?"":answerEvId) );
 	}
	/*
	 * This operation is called by ActionObservableGeneric.call  
 	 */
	@Override
	public void execTheAction() throws Exception {
	 try{	   		
// 	   	println("	%%% ActionSound execTheAction " +  fName   );
		 InputStream inputStream = new FileInputStream(fName);	
		 // get the sound file as a resource out of my jar file;
		 // the sound file must be in the same directory as this class file.
		 InputStream bufferedIn       = new BufferedInputStream(inputStream);
		 AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);
		 clip = AudioSystem.getClip(); 
		 // Open audio clip and load samples from the audio input stream.
		 clip.open(audioStream);
		 clip.start();
		 dosleep( maxduration );  //can be interrupted by a suspendAction
		 clip.stop();
  		 println("	%%% ActionSound END OF ACTION "   );
	 }catch(Exception e){
		 println("	%%% ActionSound ERROR " +  e.getMessage() );
	 }
    } 
	@Override
	public void suspendAction(){
		try{
  			println("	%%% ActionSound suspendAction "   );
//	 		if( clip.isRunning() ) clip.stop(); //done after dosleep interrupted
	 		super.suspendAction();
		}catch(Exception e){
			System.out.println("	%%% ActionSound suspendAction ERROR " +  e.getMessage() );
		}
	}
	@Override
	public String getApplicationResult() throws Exception {
 		return "sound played";
	}
 

}
