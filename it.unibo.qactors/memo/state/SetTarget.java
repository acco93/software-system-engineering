package it.unibo.qactors.state;

import akka.actor.ActorRef;

public class SetTarget {
	final ActorRef ref;

	public SetTarget(ActorRef ref) {
		this.ref = ref;
	}
}
