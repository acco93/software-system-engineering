package it.unibo.qactors.state;

import java.util.List;

public class Batch {
	final List<Object> objects;

	public Batch(List<Object> objects) {
		this.objects = objects;
	}
}
