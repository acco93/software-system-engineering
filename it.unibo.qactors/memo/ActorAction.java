package it.unibo.qactors.action;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public abstract class ActorAction extends ActionObservableGeneric<String> implements IActorAction{
	protected int maxduration;
	
	public ActorAction(String name,  QActorContext ctx, boolean cancompensate,
			String terminationEvId, String answerEvId,
			IOutputEnvView outEnvView, long maxduration) throws Exception {
		super(name, ctx, terminationEvId,  outEnvView );
  	}
	
	protected boolean dosleep(long execTime) {
		try {
			Thread.sleep( execTime  );
			return true;
		} catch (InterruptedException e) {
//			println("%%% ActorAction " + getName() + " dosleep " +  execTime + " has been interrupted "  );
			return false;
		}
	}
	
	@Override
	public int getMaxDuration() {
 		return this.maxduration;
	}
	@Override
	public void setMaxDuration(int d) {
		maxduration =  d;		
	}
	
}