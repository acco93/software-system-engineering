package it.unibo.actionobsexecutor;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.ActionObservableGeneric;

public class ActionObservableFibonacci extends ActionObservableGeneric<String> {
private String goalTodo = "fibo(25,V).";
private String myresult = "unknown";
private int n;
 	public ActionObservableFibonacci(String name, String goalTodo, 
			String terminationEvId, IOutputEnvView outView) throws Exception {
		super(name, terminationEvId, outView);
 		this.goalTodo = goalTodo;  
		println("%%% ActionObservableFibonacci CREATED with terminationEvId=" +  terminationEvId );
  	}
 	@Override
	public void execTheAction() throws Exception {
	  myresult = fibonacci( goalTodo );
    } 	
	protected String fibonacci( String goalTodo ){
		Struct st = (Struct) Term.createTerm(goalTodo);
		n = Integer.parseInt( ""+st.getArg(0) );
		return ""+fibonacci(n);
	} 	
	protected long fibonacci( int n ){
		if( n<0 || n==0 || n == 1 ) return 1;
		else return fibonacci(n-1) + fibonacci(n-2);
	}
 	@Override
	public String getResultRep() {
 		return "fibo("+n+","+this.myresult+")";
	}
	@Override
	protected String endOfAction() throws Exception {
 		return this.myresult;
	}
}