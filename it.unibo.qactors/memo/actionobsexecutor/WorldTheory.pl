%==============================================
% WorldTheory.pl for actor actioneobsxecutor
%==============================================
/*
For a QActor as a singleton statically degined in the model
*/
myname(qatuactioneobsxecutor).	%%old version (deprecated)
actorobj(qatuactioneobsxecutor).	%% see registerActorInProlog18 in QActor

/*
For a QActor instance of name=Name dynamically created
*/
setActorName( Name ):-
	retract( myname(X) ),
	retract( actorobj(X) ),
	text_term( TS,Name ),
	text_concat("qatu",TS,NN),
	assert( myname(NN) ),
	assert( actorobj(NN) ).
createActor(Name, Class, NewActor):-
 	actorobj(A),
	A <- getName returns CurActorName,
 	A <- getContext  returns Ctx,
	A <- getOutputEnvView returns View,
 	Ctx <- addInstance(Name),  
  	java_object(Class, [Name,Ctx,View], NewActor),
 	NewActor <-  getName returns NewActorName,
	actorPrintln( createActor(NewActorName, NewActor) ).
/*
Name generator
*/	
value(nameCounter,0). 
newName( Prot, Name,N1 ) :-
	inc(nameCounter,1,N1),
	text_term(N1S,N1),
	text_term(ProtS,Prot),
 	text_concat(ProtS,N1S,Name),
	assert( instance( Prot, N1, Name ) ),
	actorPrintln( newname(Name,N1) ) .

	
setPrologResult( Res ):-
	( retract( goalResult( _ ) ),!; true ),		    %%remove previous goalResult (if any) 
	assert( goalResult(Res) ).

addRule( Rule ):-
	%%output( addRule( Rule ) ),
	assert( Rule ).
removeRule( Rule ):-
	retract( Rule ),
	%%output( removedFact(Rule) ),
	!.
removeRule( A  ):- 
	%%output( remove(A) ),
	retract( A :- B ),!.
removeRule( _  ).

setResult( A ):-
 	( retract( result( _ ) ),!; true ), %%remove previous result (if any)	
	assert( result( A ) ).

evalGuard( not(G) ) :-
	G, !, fail .
evalGuard( not(G) ):- !.
evalGuard( true ) :- !.
evalGuard( G ) :-  
	%stdout <- println( evalGuard( G ) ),
	G . 

output( M ):-stdout <- println( M ).
%-------------------------------------------------
%  TuProlo FEATURES of the QActor actioneobsxecutor
%-------------------------------------------------
dialog( FileName ) :-  
	java_object('javax.swing.JFileChooser', [], Dialog),
	Dialog <- showOpenDialog(_),
	Dialog <- getSelectedFile returns File,
	File <- getName returns FileName. 		 

%% :- stdout <- println(  "hello from world theory of actioneobsxecutor" ). 

%-------------------------------------------------
%  UTILITIES for TuProlog computations
%-------------------------------------------------
loop( N,Op ) :- 
	assign(loopcount,1),
	loop(loopcount,N,Op).

loop(I,N,Op) :-  
		getVal( I , V ),
		%%output( values( I,V,N ) ),
 		V =< N ,!,
   		%% qatuactioneobsxecutor <- println( loop( I,V ) ),
     	%% execActor(  println( "doing loop" ) ),
     	execActor( Op  ),
		V1 is V + 1 ,
		assign(I,V1) ,
		loop(I,N,Op).	%%tail recursion
loop(I,N,Op).

getVal( I, V ):-
	value(I,V).

assign( I,V ):-
	retract( value(I,_) ),!,
	assert( value( I,V )).
assign( I,V ):-
	assert( value( I,V )).

inc(I,1,N):-
	value( I,V ),
	N is V + 1,
	assign( I,N ).

actorPrintln( X ):- actorobj(A), A  <- println( X ).

%-------------------------------------------------
%  User static rules about actioneobsxecutor
%------------------------------------------------- 
actiontodo( fibo( 40,V),2000).
/*
------------------------------------------------------------------------
execActor( testForProlog( X)):- 
	java_catch(
		 qatuactioneobsxecutor <- testForProlo( X) ,
		[('java.lang.Exception'(Cause, Msg, StackTrace),output(Msg))],
		output(done)
	).
execActor( ANY ):- qatuactioneobsxecutor  <- println( execActor(failure,ANY) ).
------------------------------------------------------------------------
*/
execActor( Op ) :- output( Op ), qatuactioneobsxecutor <- Op, !.
execActor( _ ):- output( failure ).

androidConsult(T) :- qatuactioneobsxecutor <- androidConsult(T), !.
androidConsult(T) :- actorPrintln( failure(androidConsult) ).

/*
%-------------------------------------------------
%  Some predefined code
%------------------------------------------------- 
*/
fibo(0,1).
fibo(1,1).
fibo(I,N) :- V1 is I-1, V2 is I-2,
  fibo(V1,N1), fibo(V2,N2),
  N is N1 + N2.

%% Fibonacci with cache (to be used in guards)

fib(V):-
	fibmemo( V,N ),!,
	actorPrintln( fib_a(V,N) ).
fib(V):-
	fibWithCache(V,N), 					
	actorPrintln( fib_b(V,N) ).

fib( V,R ) :-
	fibWithCache(V,R).

fibmemo( 0,1 ).
fibmemo( 1,1 ).
fibWithCache( V,N ) :-
	fibmemo( V,N ),!.
fibWithCache( V,N ) :-
	V1 is V-1, V2 is V-2,
  	fibWithCache(V1,N1), fibWithCache(V2,N2),
  	N is N1 + N2,
	%% actorPrintln( fib( V,N ) ),
	assert( fib( V,N ) ).

