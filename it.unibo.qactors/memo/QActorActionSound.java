/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.akka.action;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.ActionSoundTimed;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.akka.QActor;
import it.unibo.qactors.akka.QActorActionUtils;

public class QActorActionSound extends QActor{
	private String fName = "./audio/tada2.wav";
	private int durationMsec = 1400;
	private boolean cancompensate = false;
	private String answerEvent = "";
	private String alarmEvents = "";
	private String recoveryPlans = "";
	
	public QActorActionSound(String actorId, ActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, 				"./test/it/unibo/qactors/akka/action/plans.txt",
				"./test/it/unibo/qactors/akka/action/WorldTheory.pl",			
				outEnvView, "noplan");

 	}


	protected void doJob() throws Exception {
//		println(getName() + " doJob " );
   	}

	protected void executeSoundTimedAction() throws Exception{
//		playSound(fName, durationMsec, answerEvent, alarmEvents, recoveryPlans);
		
		String terminationEvId = IActorAction.endBuiltinEvent+"sound";
		registerForEvent( terminationEvId );

		String[] alarmEvArray  = QActorUtils.createArray( alarmEvents );
		String[] planarray   = QActorUtils.createArray(recoveryPlans);
 		IActorAction action    = new ActionSoundTimed(
 				"sound", terminationEvId, alarmEvArray, outEnvView, durationMsec, fName );  
		ActionExecMode mode = (answerEvent.length()==0) ? ActionExecMode.synch : ActionExecMode.asynch ;
  		println("QActor mode=" + mode + " action=" + action );
   		actionUtils.executeReactiveAction(action, mode, alarmEvArray, planarray);
 	}
	
	/*
	 * Play a sound of duration=3 sec for 3 sec waiting for its termination
	 */
	protected void playSynchronous() throws Exception{
		println("play a sound in synchronous way");
		String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		playSound("audio/computer_complex3.wav",ActionExecMode.synch, terminationEvId, 4000,  "", "" );			
	}
	/*
	 * Play a sound of duration=20 sec for 5 sec
	 * without waiting for its termination
	 */
	protected void playAsynchronous() throws Exception{
		println("play a sound in asynchronous way");
		String terminationEvId = QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		playSound("audio/music_dramatic20.wav",  ActionExecMode.asynch, terminationEvId, 7000,  "", "" ); 	 			
	}
	

	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );		
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg)  {
		try{
			String msgType    = msg.msgType();	
			String msgId      = msg.msgId();
			String msgContent = msg.msgContent();  
				if( msgType.equals(ActorContext.request) ){
					if( msgId.equals("play") ){
						if( msgContent.equals("play")){ 
							executeSoundTimedAction();
							//send the answer
							getSender().tell("done_play", getSelf());
						}else if(msgContent.equals("playAsynchronous") ){
		 					playAsynchronous();
							//send the answer
							getSender().tell("done_playAsynchronous", getSelf());
		 				}else if(msgContent.equals("playSynchronous") ){
		 					playSynchronous();
							//send the answer
							getSender().tell("done_playSynchronous", getSelf());
		 				}
				} //play 	
				}//request 
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		this.terminate();		
	}
 
 	
}
