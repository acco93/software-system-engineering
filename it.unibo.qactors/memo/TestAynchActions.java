package it.unibo.qactors.akka.action.simple;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.util.concurrent.Future;
import org.junit.Before;
import org.junit.Test;
import akka.actor.ActorSelection;
import akka.actor.Props;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.AsynchActionGenericResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IAsynchAction;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

 
/*
 * ==========================================================
 * ACTIONS as AsynchActionGeneric (timed but NO tout handling)
 * 		execSynch / execAsych 
 * ==========================================================
 */
public class TestAynchActions  {
	 private IOutputEnvView stdOut = SituatedSysKb.standardOutEnvView;
	 private ActorContext ctx ;
 	 private String goalTodo ;
 	 private String terminationEvId = "";
 	 private String answerEvId ="";
 	 private QActor actionTerminationHandler;
 	 
  	 IAsynchAction<AsynchActionGenericResult<String>>  action;
 	 
	/*
	 *  
 	 */
	@Before
	public void setUp() throws Exception{
		//Create a system without qactors
		ctx = ActorContext.initQActorSystem("ctxSimpleAction", 
				"./test/it/unibo/qactors/akka/action/simple/noQActorSystemKb.pl", stdOut);
		assertTrue("setUp" , ctx != null );		
		//Create a goal to be solved by fibonacci action
		goalTodo = "fibo(41,V)";
 	}
  	
 	/*
  	 * Activate a timed action  
  	 * The action could emit a termination event
  	 * The termination event is perceived by an handler   
  	 */
   	@Test
	public void synchNoHandler() {
		try {
 			createAction( false  );
			//Activate the action (callable) in synch mode and waits for termination
 			AsynchActionGenericResult<String> r1 = action.execSynch();	//see AsynchActionGeneric<T>
			String result = r1.getResult();
			System.out.println("synchNoHandler  RESULT=" + result );
			assertTrue("synchNoHandler result" , result.equals("fibo(41,val(267914296))"));
 		} catch (Exception e) {
			fail("synchNoHandler" +  e.getMessage());
		}			
  	}
 	@Test
	public void syncWithHandler() {
		try {			
			createAction( true  );
			//Activate the action (callable) in synch mode ( waits for termination )
 			AsynchActionGenericResult<String> r1 = action.execSynch();	//see AsynchActionGeneric<T>
			String result = r1.getResult();
			System.out.println("syncWithHandler  RESULT=  " + result );
			assertTrue("syncWithHandler result" , result.startsWith("fibo(41,val(267914296))"));
			//Check that the event is arrived
			checkTerminationEvent();
		} catch (Exception e) {
			fail("syncWithHandler" +  e.getMessage());
		}			
  	}
    @Test
	public void aASynchWithHandler() {
		try {
			createAction( true  );			
			//Activate the action (callable) in synch mode and waits for termination
// 			Future<AsynchActionGenericResult<String>> r1 = 
			action.execASynch(); //.activate();	//see AsynchActionGeneric<T>
 			//Check that the event is arrived
 			checkTerminationEvent();
		} catch (Exception e) {
			fail("timedASynchWithToutTest" +  e.getMessage());
		}	
 		
  	}
 
  	@Test
	public void aSynchNohandler() {
		try {
			System.out.println("caSynchNohandler STARTS");		
			createAction( false  );
 			
			//Activate the action (callable) in synch mode and waits for termination
  			Future<AsynchActionGenericResult<String>> r1 = 
 					action.execASynch(); //.activate();	//see AsynchActionGeneric<T>
 			System.out.println("aSynchNohandler code after activation");	
			System.out.println("aSynchNohandler WAITS FOR THE RESULT"   );
			AsynchActionGenericResult<String> result = r1.get();
			System.out.println("aSynchNohandler RESULT= " + result.getResult() );
			System.out.println("caSynchNohandler ENDS");		
   		} catch (Exception e) {
			fail("fiboTimedNoToutTest" +  e.getMessage());
		}	
 		
  	}

//  	@Test
	public void timedASynchEndImmediate() {
		try {
			//Create a termination event (used by the action and the handler)
			terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
			answerEvId      =  QActorUtils.getNewName("answerFibo");
 			//Create the action termination handler
 			QActor actionTerminationHandler = createActionTerminationEvHandler(terminationEvId);
 			QActor actionCompletionHandler  = createActionTerminationEvHandler(answerEvId);

 			//Create a timed action 
			action =  createTimedEndImmediateAction( goalTodo, stdOut, answerEvId   );


			//Activate the action (callable) in synch mode and waits for termination
 					action.execSynch();  //see AsynchActionGeneric<T>

 			//Check that the event is arrived
 			IEventItem ev = actionTerminationHandler.waitForCurentEvent();
  			assertTrue("fiboTimedASynchTest " , ev.getEventId().equals(terminationEvId)   );
   			
  			ev = actionCompletionHandler.waitForCurentEvent();
  			assertTrue("fiboTimedASynchTest " , 
  					ev.getEventId().equals(answerEvId) &&
  					ev.getMsg().startsWith("actionObs_result(fibo(41,val(267914296)),exectime(")  );
  			
  			/*
  			 * REMOVE from the system the actionTerminationHandler
  			 * BUT maintain the actionCompletionHandler
  			 */
   			actionTerminationHandler.terminate();
//   			actionCompletionHandler.terminate();
  			System.out.println("============= CREATE ANOTHER ACTION ==============");
			action =  createTimedEndImmediateAction( goalTodo, stdOut, answerEvId   );
			//ACTIVATE THE ACTION 
// 			Future<AsynchActionGenericResult<String>> r1 = 
  			action.execASynch(); 
   			System.out.println("code after action activation");
  			ev = actionCompletionHandler.waitForCurentEvent();
  			assertTrue("fiboTimedASynchTest " , 
  					ev.getEventId().equals(answerEvId) &&
  					ev.getMsg().startsWith("actionObs_result(fibo(41,val(267914296)),exectime(")  );
		} catch (Exception e) {
			fail("fiboTimedNoToutTest" +  e.getMessage());
		}	
 		
  	}
  	
/*
 * ============================================================================
 * SUPPORT
 * ============================================================================
 */
  	
  	protected void createAction( boolean withHandler ) throws Exception{
		//Create a termination event (used by the action and the handler)
		terminationEvId =  QActorUtils.getNewName(IActorAction.endBuiltinEvent);
		//Create an observable action with  event handler
		action = createAsynchAction( goalTodo, stdOut   );
		if( withHandler ){
			//Create the action termination handler
			actionTerminationHandler = createActionTerminationEvHandler(terminationEvId);  	
			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx, actionTerminationHandler.getName());
			assertTrue("handlerTerminate" , asel != null );
		}
  	}
  	
	public IAsynchAction<AsynchActionGenericResult<String>>  createAsynchAction(  
			String goalTodo, IOutputEnvView outEnvView )
			throws Exception {
 		boolean canresume = false;
		String name = QActorUtils.getNewName("fibo_");		
		return  new ActionAsynchFibonacci( name, goalTodo,
				canresume, terminationEvId, "",  outEnvView );		
 	}
	public IAsynchAction<AsynchActionGenericResult<String>> createTimedEndImmediateAction(  
			String goalTodo, IOutputEnvView outEnvView, String answerEv )
			throws Exception {
 		boolean canresume = false;
		String name = QActorUtils.getNewName("fiboend_");		
		return new ActionAsynchFibonacci(name, goalTodo,
				canresume, terminationEvId, answerEv, outEnvView );
 	}
 
  	protected QActor createActionTerminationEvHandler( String evid ) throws Exception{
  	 	String name =  QActorUtils.getNewName("evh_");
 	 	String[] alarmIds = new String[]{evid};
//	 	ActorRef aref = 
	 			QActorUtils.creatorAkkaContext.actorOf( Props.create(
	 					Class.forName("it.unibo.qactors.akka.action.simple.ActionObsFiboEvh"), 
	 					name, ctx,  stdOut, alarmIds), name );	
	 	/*
	 	* The action must be activated AFTER that the ActionObsFiboEvh has been registered
	 	*/
	 	return QActorUtils.waitUntilQActorIsOn(name); 	 
 	}
  	
  	protected void checkTerminationEvent() throws Exception{
		IEventItem ev = actionTerminationHandler.waitForCurentEvent();
		String result = ev.getMsg();
		assertTrue("checkTerminationEvent" , 
					ev.getEventId().equals(terminationEvId)
					&& result.startsWith("actionObs_result(fibo(41,val(267914296)),execTime("));  		
  	}

}
