package it.unibo.actiontimedexecutor;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.platform.EventHandlerComponent;

public class TerminationHandler extends EventHandlerComponent{
 	public TerminationHandler(String name, ActorContext myctx, String[] eventsId,
			IOutputEnvView view) throws Exception {
		super(name, myctx, eventsId, view);
//		this.action = action;
		println(""+ getName() + " STARTS with " + eventsId[0]);
 	} 
//	@Override
//	public void doJob() throws Exception {
//		IEventItem event = getEventItem();
//		String msg;
//		msg = event.getEventId() + "|" + event.getMsg() + " actionResult=" + action.getResultRep()  ;
//		showMsg( msg );		
//	}
	@Override
	public void doJob() throws Exception {
		
	}
	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
 	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );		
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " RECEIVES QActorMessage " + msg.getDefaultRep() );
		getSender().tell("done", getSelf());
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}

} 