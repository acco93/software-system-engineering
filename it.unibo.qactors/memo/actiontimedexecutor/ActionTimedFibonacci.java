package it.unibo.actiontimedexecutor;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.ActorTimedAction;

  
public class ActionTimedFibonacci extends ActorTimedAction{
private String goalTodo = "fibo(25,V).";
private int n = 0;
 	public ActionTimedFibonacci(String name, String goalTodo, boolean cancompensate,
			String terminationEvId, String answerEvId, String[] alarms, IOutputEnvView outView,
			int maxduration) throws Exception {
		super(name, cancompensate, terminationEvId, answerEvId, alarms, outView, maxduration);
 		this.goalTodo = goalTodo;
//		println("%%% ActionTimedFibonacci CREATED goalTodo " +  goalTodo  );
  	}
	/*
	 * This operation is called by ActionObservableGeneric.call 
  	 */
	@Override
	public void execTheAction() throws Exception {
	 try{	  
		 println("%%% ActionTimedFibonacci STARTS terminationEvId= "  + terminationEvId);
  		 actionTimedResult = fibonacci( goalTodo );
 		 //The real result is set by endOgAction() -> getResult() -> getApplicationResult
// 		 println("%%% ActionTimedFibonacci ENDS result=" +  actionTimedResult );
   	 }catch(Exception e){
		 println("%%% ActionTimedFibonacci EXIT " +  e.getMessage() );
	 }
    } 	
	protected String fibonacci( String goalTodo ) throws Exception{
		Struct st = (Struct) Term.createTerm(goalTodo);
		n = Integer.parseInt( ""+st.getArg(0) );
 		return ""+fibonacci(n);
	}	
	protected long fibonacci( int n ) throws Exception{
 		if( n<0 || n==0 || n == 1 ) return 1;
		else return fibonacci(n-1) + fibonacci(n-2);
	}	 
	@Override
	public String getApplicationResult() throws Exception {
		if( this.suspended)
			return "fibo(" + n + "," + suspendevent +")" ;
		else 
 			return "fibo("+n+",val("+this.actionTimedResult+"),timeRemained("+timeRemained+"))";
	}
 }
