/* Generated by AN DISI Unibo */ 
package it.unibo.actiontimedexecutor;
import alice.tuprolog.Term;
import alice.tuprolog.Struct;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.is.interfaces.IOutputEnvView;

import it.unibo.qactors.action.ActionDummy;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.akka.QActor;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import alice.tuprolog.SolveInfo;
import it.unibo.is.interfaces.IActivity;
import it.unibo.is.interfaces.IIntent;

public abstract class AbstractActiontimedexecutor extends QActor implements IActivity{ 
	protected AsynchActionResult aar = null;
	protected boolean actionResult = true;
	protected alice.tuprolog.SolveInfo sol;
	
			protected static IOutputEnvView setTheEnv(IOutputEnvView outEnvView ){
				EnvFrame env = new EnvFrame( "Env_actiontimedexecutor", java.awt.Color.cyan  , java.awt.Color.black );
				env.init();
				env.setSize(800,400);
				IOutputEnvView newOutEnvView = ((EnvFrame) env).getOutputEnvView();
				return newOutEnvView;
			}
	
	
		public AbstractActiontimedexecutor(String actorId, ActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
			super(actorId, myCtx, "./srcMore/it/unibo/actiontimedexecutor/plans.txt", 
			"./srcMore/it/unibo/actiontimedexecutor/WorldTheory.pl",
			setTheEnv( outEnvView )  , "init");		
			addInputPanel(80);
			addCmdPanels();	
	 	}
	protected void addInputPanel(int size){
		((EnvFrame) env).addInputPanel(size);			
	}
	protected void addCmdPanels(){
		((EnvFrame) env).addCmdPanel("input", new String[]{"INPUT"}, this);
		((EnvFrame) env).addCmdPanel("alarm", new String[]{"FIRE"}, this);
		((EnvFrame) env).addCmdPanel("help",  new String[]{"HELP"}, this);				
	}
//		@Override
//		protected void doJob() throws Exception {
//	 		initSensorSystem();
//			boolean res = init();
//			//println(getName() + " doJob " + res );
//		} 
	//TODO REGENERATE
	@Override
	protected void doJob() throws Exception {
		println(getName() + " doJob "  );
 		initSensorSystem();
		boolean res = init();
		println(getName() + " doJob continue=" + res );
	} 
		/* 
		* ------------------------------------------------------------
		* PLANS
		* ------------------------------------------------------------
		*/
	    public boolean init() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "init";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		temporaryStr = " \" 	*** Fibonacci as Timed Action 		*** \" ";
	    		println( temporaryStr );  
	    			/*
	    		{ 
	    			String parg = "consult( \"./actionDemoTheory.pl\" )";
	    		  aar = solveGoal( parg , 0, "","" , "" );
	    		//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    		if( aar.getInterrupted() ){
	    			curPlanInExec   = "init";
	    			if( ! aar.getGoon() ) break;
	    		} 			
	    		if( aar.getResult().equals("failure")){
	    		if( ! switchToPlan("prologFailure").getGoon() ) break;
	    		}else if( ! aar.getGoon() ) break;
	    		}
	    		*/
	    			boolean res = QActorUtils.loadTheoryFromFile( 
	    					this.pengine,"./test/it/unibo/actiontimedexecutor/actionDemoTheory.pl" );
	    			if( !res ){
	    				if( ! planUtils.switchToPlan("prologFailure").getGoon() ) break;
	    			}
	    			
	    		if( (guardVars = planUtils.evalTheGuard( " !?actiontodo(G,T)" )) != null ){
	    		{ String parg = "createAppplicationInterface(G,T)";
	    		parg = QActorUtils.substituteVars(guardVars,parg);
	    		/*
	    		  aar = solveGoal( parg , 0, "","" , "" );
	    		//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    		if( aar.getInterrupted() ){
	    			curPlanInExec   = "init";
	    			if( ! aar.getGoon() ) break;
	    		} 			
	    		if( aar.getResult().equals("failure")){
	    		if( ! switchToPlan("prologFailure").getGoon() ) break;
	    		}else if( ! aar.getGoon() ) break;
	    		}
	    		*/
	    		res = QActorUtils.solveGoal(this.pengine, parg);
//    			println("solveGoal  " + parg + " res=" + res);
	    		if( !res ){
    				if( ! planUtils.switchToPlan("prologFailure").getGoon() ) break;
    			}
	    		}
	    		}
	    		if( (guardVars = planUtils.evalTheGuard( " !?actiontodo(G,T)" )) != null ){
	    		temporaryStr = "actiontodo(G,T)";
	    		temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
	    		println( temporaryStr );  
	    		}
	    		if( ! planUtils.switchToPlan("waitEvents").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	    println( getName() + " ERROR " + e.getMessage() );
	    throw e;
	    }
	    }
	    public boolean reactionToUsercmd() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "reactionToUsercmd";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		temporaryStr = " \"reactionToUsercmd from the model\" ";
	    		println( temporaryStr );  
	    		returnValue = continueWork;  
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	    println( getName() + " ERROR " + e.getMessage() );
	    throw e;
	    }
	    }
	    public boolean waitEvents() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "waitEvents";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		temporaryStr = " \"WAITS\" ";
	    		println( temporaryStr );  
	    		//senseEvent
	    		timeoutval = 600000;
	    		aar = planUtils.senseEvents( timeoutval,"usercmd","continue",
	    					"" , "",ActionExecMode.synch );
	    		if( ! aar.getGoon() || aar.getTimeRemained() <= 0 ){
	    			println("			WARNING: sense timeout");
	    			addRule("tout(senseevent,"+getName()+")");
	    			//break;
	    		}
	    		planUtils.printCurrentEvent(false);
	    		if( planUtils.repeatPlan(0).getGoon() ) continue;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	    println( getName() + " ERROR " + e.getMessage() );
	    throw e;
	    }
	    }
	    public boolean prologFailure() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "prologFailure";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		temporaryStr = " \"failure in solving a Prolog goal\" ";
	    		println( temporaryStr );  
	    		returnValue = continueWork;  
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	    println( getName() + " ERROR " + e.getMessage() );
	    throw e;
	    }
	    }
	    protected void initSensorSystem(){
	    	//doing nothing in a QActor
	    }
	    
	 
		/* 
		* ------------------------------------------------------------
		* APPLICATION ACTIONS
		* ------------------------------------------------------------
		*/
		
		/* 
		* ------------------------------------------------------------
		* IACTIVITY
		* ------------------------------------------------------------
		*/
		    private String[] actions = new String[]{
		    	"println( STRING | TERM )", 
		    	"play( FILENAME ) ",
		"emit(EVID,EVCONTENT)  ",
		"move(MOVE,DURATION,ANGLE)  with MOVE=mf|mb|ml|mr|ms",
		"forward( DEST, MSGID, MSGCONTENTTERM)"
		    };
		    protected void doHelp(){
				println("  GOAL ");
				println("[ GUARD ], ACTION  ");
				println("[ GUARD ], ACTION, DURATION ");
				println("[ GUARD ], ACTION, DURATION, ENDEVENT ");
				println("[ GUARD ], ACTION, DURATION,'',E VENTS, PLANS ");
				println("Actions:");
				for( int i=0; i<actions.length; i++){
					println(" " + actions[i] );
				}
		    }
		@Override
		public void execAction(String cmd) {
			if( cmd.equals("HELP") ){
				doHelp();
				return;
			}
			if( cmd.equals("FIRE") ){
				try {
					this.emit("input", "alarm", "alarm(fire)");
				} catch (Exception e) {
					e.printStackTrace();
				}
				return;
			}
			String input = env.readln();
			//input = "\""+input+"\"";
			input = it.unibo.qactors.web.GuiUiKb.buildCorrectPrologString(input);
			//println("input=" + input);
			try {
				Term.createTerm(input);
//				String eventMsg=it.unibo.qactors.web.QActorHttpServer.inputToEventMsg(input);
//				//println("QActor eventMsg " + eventMsg);
//				platform.raiseEvent("input", "local_"+it.unibo.qactors.web.GuiUiKb.inputCmd, eventMsg);
	 		} catch (Exception e) {
		 		println("QActor input error " + e.getMessage());
			}
		}
	 	
		@Override
		public void execAction() {}
		@Override
		public void execAction(IIntent input) {}
		@Override
		public String execActionWithAnswer(String cmd) {return null;}
	  }
	
