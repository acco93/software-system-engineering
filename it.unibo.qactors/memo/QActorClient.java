/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.akka.action;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import alice.tuprolog.Var;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.akka.QActor;

public class QActorClient extends QActor{
	
	public QActorClient(String actorId, ActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, outEnvView);
 	}

//	@Override
//	public void preStart() {
//		try {
//			super.preStart();
//		} catch (Exception e) {
// 			e.printStackTrace();
//		}
//		
//	}

	/*
	 * 1) ask to play a short sound and waits until the sound  ends 
	 * 2) ask play a music in asynchronous  way
	 * 3) while the music is playing ask to play a message in synchronous way 
	 */
	protected void doJob() throws Exception {
 		String result ;
//		result = askMessageWaiting("qaplayer", "play", 15000);
//		println("	%%% "+getName() + " play result=" + result );
		result = askMessageWaiting("qaplayer", "playAsynchronous", 4000);
		println("	%%% "+getName() + " playAsynchronous result=" + result );
		result = askMessageWaiting("qaplayer", "playSynchronous", 5000);
		println("	%%% "+getName() + " playSynchronous result=" + result );
		
 		testRules();
		testGuards();
 	}
	
	protected void testRules() throws Exception{
 		addRule( "a(2)" );  
 		AsynchActionResult aar = solveGoal( "a(X)" , 0,  "" , "" ); //solve immediately
 		println("	%%% "+getName() + " testRules aar=" + aar.getResult() );
 		aar = solveGoal( "goalResult(X)" , 0,  "" , "" ); //solve immediately
 		println("	%%% "+getName() + " goalResult aar=" + aar.getResult() );
	}
	
	protected void testGuards() throws Exception{
		addRule( "soundFile(filename,2000)" );  
		List<Var> guardVars = QActorUtils.evalTheGuard( this, "soundFile(F,T)", QActorUtils.guardVolatile);
		if( guardVars != null ){
//			String fname = guardVars.get("F");
//			String time  = guardVars.get("T");
//			println("	%%% "+getName() + " guard F=" + fname + " T=" + time );
			Iterator<Var> iter = guardVars.iterator();
			while( iter.hasNext() ){
				Var curVar = iter.next();
				println("	%%% "+getName() + " curVar " + curVar.getName() + " = " + curVar.getTerm() );
			}
			
		}
	}

	@Override
	protected void handleQActorString(String msg) {
		println("	%%% "+ getName() + " RECEIVES String " + msg  );	
	}
	@Override
	protected void handleQActorEvent(IEventItem ev) {
		println(getName() + " RECEIVES EVENT " + ev.getDefaultRep() );		
	}

	@Override
	protected void handleQActorMessage(QActorMessage msg) {
		println(getName() + " RECEIVES QActorMessage " + msg.getDefaultRep() );	
	}

	@Override
	protected void handleTerminationMessage(ActorTerminationMessage msg) {
		println(getName() + " RECEIVES ActorTerminationMessage "  );	
		this.terminate();
	}
}
