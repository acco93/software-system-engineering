package it.unibo.qactors.akka.action.simple;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.AsynchActionGeneric;

public class ActionAsynchFibonacci extends AsynchActionGeneric<String>{
private String goalTodo ;
private int n = 0;
private String myresult;
 	public ActionAsynchFibonacci(String name, String goalTodo, boolean cancompensate,
			String terminationEvId, String answerEvId, IOutputEnvView outView ) throws Exception {
		super(name,  terminationEvId,  outView );
 		this.goalTodo = goalTodo;
//		println("%%% ActionFibonacci CREATED goalTodo " +  goalTodo  );
  	}
	/*
	 * This operation is called by ActionObservableGeneric.call 
  	 */
	@Override
	public void execTheAction() throws Exception {
	 try{	  
		 myresult =  fibonacci( goalTodo );
 		 //The real result is set by endOgAction() -> getResult() -> getApplicationResult
 		 println("%%% ActionFibonacci ENDS result=" +  myresult );
   	 }catch(Exception e){
		 println("%%% ActionFibonacci EXIT " +  e.getMessage() );
	 }
    } 	
	protected String fibonacci( String goalTodo ) throws Exception{
		Struct st = (Struct) Term.createTerm(goalTodo);
		n = Integer.parseInt( ""+st.getArg(0) );
 		return ""+fibonacci(n);
	}	
	protected long fibonacci( int n ) throws Exception{
 		if( n<0 || n==0 || n == 1 ) return 1;
		else return fibonacci(n-1) + fibonacci(n-2);
	}	 
	@Override
	public String getApplicationResult() throws Exception {
  		return "fibo("+n+",val("+this.myresult+"))";
	}
 }
