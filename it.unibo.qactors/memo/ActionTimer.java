package it.unibo.qactors.action;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorUtils;
import it.unibo.system.SituatedActiveObject;

/*
 * TODO: ActionTimer as an actor?
 */
public class ActionTimer extends SituatedActiveObject{
protected int execTime ;
protected String toutEventId;
protected IContactEventPlatform platform;
protected Thread myself;

	public ActionTimer( String name,  IOutputEnvView outEnvView, int execTime , String toutEventId ){
		super(outEnvView,name);
		this.execTime = execTime;
		this.toutEventId = toutEventId;
		myself = this;
	}
	@Override
	protected void doJob() throws Exception {
 		try {
// 			println("%%% ActionTimer " + getName() + " STARTS " +  execTime    );
 			Thread.sleep( execTime  );
 			String m = "tout("+execTime+")";
//  			println("%%% ActionTimer " + getName() + " ENDS and raiseEvent " +  toutEventId    );
			QActorUtils.raiseEvent(getName(), toutEventId, m );
 		} catch (Exception e) {
//  			println("%%% ActionTimer " + getName() + " doJob " +  execTime + " has been interrupted "  );
  		} 		
	}
	
	@Override
	protected void startWork() throws Exception {		
	}

	@Override
	protected void endWork() throws Exception { 		
	}

}
