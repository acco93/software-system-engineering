/*
 * This actors prints a sequence of messages on the standard output port.
 */
package it.unibo.qactors.platform;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.akka.QActor;

public class QActorMock extends QActor{

	public QActorMock(String actorId, ActorContext myCtx, IOutputEnvView outEnvView) {
		super(actorId, myCtx, outEnvView);
 	}

 	@Override
	protected void doJob() throws Exception {
 		for(int i=1; i<=1; i++){
 			autoMsg(i);
   		}
  	}	
	protected void autoMsg(int i) throws Exception{
		println(getName() + " sends " + i );
//		sendMsg("info", getName(), ActorContext.dispatch, "autoMsg" );
//		String msg = receiveMsg();
//		String senderId = myCtx.getMsgSenderActorId(msg);
//		println(getName() + " received " + msg + " from " + senderId );
		
	}

}
