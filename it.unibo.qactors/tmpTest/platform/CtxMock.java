package it.unibo.qactors.platform;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.InputStream;

import alice.tuprolog.Prolog;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorUtils;


public class CtxMock extends ActorContext{

	public CtxMock(String name, IOutputEnvView outEnvView, InputStream sysKbStream, InputStream sysRulesStream)
			throws Exception {
		super(name, outEnvView, sysKbStream, sysRulesStream);		 
	}
 	
/*
 * MAIN for a local demo 	
 */
	public static void main(String[] args) throws Exception {
		IBasicEnvAwt env = new EnvFrame("ctxMock", null, Color.cyan, Color.black);
		env.init();
		env.writeOnStatusBar("ctxMock" + " | working ... ", 14);
		InputStream sysKbStream    = new FileInputStream("mockJavaSystemKb.pl");
		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
		new CtxMock("ctxMock", env.getOutputEnvView(), sysKbStream, sysRulesStream ) ;
	}

}
 
