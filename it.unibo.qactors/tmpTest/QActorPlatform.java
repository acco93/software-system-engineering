package it.unibo.qactors.platform;

import java.util.Hashtable;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import it.unibo.contactEvent.interfaces.IContactEventPlatform;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.contactEvent.interfaces.ILocalTime;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;
import it.unibo.system.SituatedSysKb;

/*
 * A singleton that implements IContactEventPlatform
 * SHARED between actors
 */
public class QActorPlatform implements IContactEventPlatform{
	private static QActorPlatform myself = null; // To implement a singleton
  	private ActorContext ctx;
  	  
//  	protected Hashtable<String,ActorRef> eventLoopActorTable = new Hashtable<String,ActorRef>();
  	
	public static IContactEventPlatform getPlatform( ) throws Exception{
		 if( myself == null ){
			 throw new Exception("No qactor platform available");
		 }
		 else return myself;
	}
	public static  IContactEventPlatform getPlatform(ActorContext ctx){
		 if( myself == null ){
		 	myself  = new QActorPlatform(ctx);
		 }
		 return myself;
	}
	/*
	 * CONSTRUCTOR (private)
	 */
	private QActorPlatform(ActorContext ctx){
		this.ctx = ctx;
//		eventLoopActorTable = new Hashtable<String,QActor>();
}

	@Override
	public void activate(String subj) {
 		
	}

	@Override
	public void raiseEvent(String emitter, String evId, String evContent) {
//		try {
//			startEventLoopActor();
//			//Send a message to the context event loop actor
//			String eventLoopName = QActorUtils.getEventLoopActorName(ctx);
//			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx,eventLoopName);
//			if( asel != null ){
//				asel.tell("", getSelf());
//			}
//		} catch (Exception e) {
// 			e.printStackTrace();
//		}
	}

	@Override
	public ILocalTime getLocalTime() {
 		return null;
	}

	@Override
	public void registerForEvent(String subj, String evId) {
//		try {
//			startEventLoopActor();
//			IEventItem event = QActorUtils.buildEventItem(subj, evId);
////			getCtxEvlpActor().tell( event.getPrologRep(), getSelf()  );
//		} catch (Exception e) {
// 			e.printStackTrace();
//		} 
	}

	@Override
	public void unregisterForEvent(String subj, String ev) {
 		
	}

	@Override
	public void unregisterForAllEvents(String subj) {
 		
	}

/*
 * ------------------------------------------------------
 * LOCAL OPERATONS	
 * ------------------------------------------------------
 */
	/*
	 * Called by need (e,g, by registerForEvent , raiseEvent ... )
	 */
	protected synchronized void  startEventLoopActor() throws Exception{
			String eventLoopName = QActorUtils.getEventLoopActorName(ctx);
			ActorSelection asel = QActorUtils.getSelectionIfLocal(ctx,eventLoopName);
			if( asel == null ){
				QActorUtils.activateAkkaActor( ctx,"it.unibo.qactors.platform.EventLoopActor", eventLoopName, SituatedSysKb.standardOutEnvView);
			}
	}
 
	
	
}
