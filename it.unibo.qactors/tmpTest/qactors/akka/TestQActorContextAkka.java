package it.unibo.qactors.akka;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import it.unibo.qactors.ActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.mock.CtxMockAkka;
import it.unibo.qactors.akka.mock.QActorMockAkka;
import it.unibo.system.SituatedSysKb;

public class TestQActorContextAkka {
 
private String ctxName = "ctxMockAkka";
//	@Before
//	public void setUp(){
//		System.out.println("TestQActorContext setUp");
//		try {
//	  		InputStream sysKbStream    = new FileInputStream("mockAkkaSystemKb.pl");
//	 		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
//	 		ctx = new CtxMockAkka(ctxName, SituatedSysKb.standardOutEnvView, sysKbStream, sysRulesStream );
//		} catch (Exception e) {
//			fail("setUp " + e.getMessage() );
//		}
//	}
	@Test
	public void emptyKb() {
		try {
	  		InputStream sysKbStream    = new FileInputStream("emptySystemKb.pl");
	 		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
	 		ActorContext ctx = new CtxMockAkka(ctxName, SituatedSysKb.standardOutEnvView, sysKbStream, sysRulesStream );
	 		fail("emptyKbAkkaTest "   );
		} catch (Exception e) {
			System.out.println("            *** emptyKb " +  e.getMessage());
			assertTrue("emptyKbAkkaTest " , e.getMessage() != null );
		}
		
	}
	
	@Test
	public void actorContextTest() {
		try {
	  		InputStream sysKbStream    = new FileInputStream("mockAkkaSystemKb.pl");
	 		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
	 		ActorContext ctx = new CtxMockAkka(ctxName, SituatedSysKb.standardOutEnvView, sysKbStream, sysRulesStream );
			//System.out.println("            *** actorContextAkkaTest " +  ctx.getName());
	  		assertTrue("testActorContextName",  ctx.getName().equals( ctxName ) );
	 		assertTrue("testActorContextOutEnvView",  ctx.getOutputEnvView()==SituatedSysKb.standardOutEnvView );
		} catch (Exception e) {
			fail("actorContextAkkaTest " + e.getMessage() );
		}		
	}
	@Test
	public void actorTest() {
		try {
	  		InputStream sysKbStream    = new FileInputStream("mockAkkaSystemKb.pl");
	 		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
	 		ActorContext ctx = new CtxMockAkka(ctxName, SituatedSysKb.standardOutEnvView, sysKbStream, sysRulesStream );
 	 		new QActorMockAkka( "qa1",ctx,ctx.getOutputEnvView() );
//	 		QActorUtils.activateAkkaActorsInContext( getContext(),  ctx, ctx.getEngine(),  ctx.getOutputEnvView()  ) ;

//		    String configFile= ctx.getName()+".conf";
//		    Config config = ConfigFactory.parseFile(new File(configFile));
//	 	    //create an actor system with that config
//		    ActorSystem  system = ActorSystem.create( ctx.getName() , config );
//			system.actorOf( Props.create(  
//					SystemCreationActor.class, ctx.getName()+"Creator", ctx, ctx.getOutputEnvView()  ),  ctx.getName());

//			new SystemCreationActor("sys",ctx,ctx.getOutputEnvView());	//NOT ALLOWED
		} catch (Exception e) {
			System.out.println("actorContextAkkaTest " + e.getMessage() );
		}
		
	}

}
