package it.unibo.qactors.akka.mock;
 
import java.io.FileInputStream;
import java.io.InputStream;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.ActorContext;
import it.unibo.system.SituatedSysKb;


public class CtxMockAkka extends ActorContext{

	public CtxMockAkka(String name, IOutputEnvView outEnvView, InputStream sysKbStream, InputStream sysRulesStream)
			throws Exception {
		super(name, outEnvView, sysKbStream, sysRulesStream);		 
	}
	
/*
 * MAIN for a local demo 	
 */
	public static void main(String[] args) throws Exception {
  		InputStream sysKbStream    = new FileInputStream("mockAkkaSystemKb.pl");
 		InputStream sysRulesStream = new FileInputStream("sysRules.pl");
 		ActorContext ctx = new CtxMockAkka("ctxMockAkka", SituatedSysKb.standardOutEnvView, sysKbStream, sysRulesStream );
 		ctx.activateTheActors();
 	}




}
 
