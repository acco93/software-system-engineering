package it.unibo.qactors;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contactEvent.interfaces.IActorMessage;

public class TestQActorMessage {
	private String msgId       = "info";
	private String msgType     = "dispatch";
	private String msgSender   = "actorSender";
	private String msgReceiver = "actorReceiver";
	private String msgContent  = "hello(world)";
	private String msgNum = "1";		
	
	private IActorMessage workMsg;
	
	@Before
	public void setUp(){
		System.out.println("TestQActorMessage setUp");
		try {
			workMsg = new QActorMessage(msgId,msgType,msgSender,msgReceiver,msgContent,msgNum);
		} catch (Exception e) {
 			fail("setUp " + e.getMessage() );
		}
	}

	@Test
	public void testCreateArgs() {
 		assertTrue("testCreateArgs", 
 				workMsg.msgId().equals(msgId) &&
 				workMsg.msgType().equals(msgType) &&
 				workMsg.msgSender().equals(msgSender) &&
 				workMsg.msgReceiver().equals(msgReceiver) &&
 				workMsg.msgContent().equals(msgContent) &&
 				workMsg.msgNum().equals(msgNum)				
			);
	}

	@Test
	public void testCreateStruct() {
 		String msgStr =  
		"msg(A,B,C,D,E,F)".replace("A", msgId).replace("B", msgType).
			replace("C", msgSender).replace("D", msgReceiver).replace("E", msgContent).replace("F", msgNum );
		Term msgTerm = Term.createTerm(msgStr);
		assertTrue("testCreateStruct", msgTerm != null );
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgStr);
			assertTrue("testCreateStruct", 
					msg.msgId().equals(msgId) &&
					msg.msgType().equals(msgType) &&
					msg.msgSender().equals(msgSender) &&
					msg.msgReceiver().equals(msgReceiver) &&
					msg.msgContent().equals(msgContent) &&
					msg.msgNum().equals(msgNum)				
				);
		} catch (Exception e) {
			fail("testCreateStruct");
	 	}
	}

	@Test
	public void testCreateNoNumSyntax() {
		try{
			new QActorMessage(msgId,msgType,msgSender,msgReceiver,msgContent,"a");
			fail("testCreateNoNumSyntax");
		}catch(Exception e){
			assertTrue("testCreateNoNumSyntax", true);
		}
	}
	@Test
	public void testCreateNoPrologSyntax() {
		try{
			String msgStr =  "msg(info, dispatch, sender, receiver, a( , 1)";
 			new QActorMessage(msgStr);
			fail("testCreateNoPrologSyntax");
		}catch(Exception e){
			assertTrue("testCreateNoPrologSyntax", true);
		}
	}

	@Test
	public void testDefaultRep() {
		 String msgRep = workMsg.getDefaultRep();
		 Struct msgTerm = (Struct) Term.createTerm(msgRep);
 		 assertTrue("testDefaultRep", 
				 msgTerm.getArg(0).toString().equals(msgId) &&
				 msgTerm.getArg(1).toString().equals(msgType) &&
				 msgTerm.getArg(2).toString().equals(msgSender) &&
				 msgTerm.getArg(3).toString().equals(msgReceiver) &&
				 msgTerm.getArg(4).toString().equals(msgContent) &&
				 msgTerm.getArg(5).toString().equals(msgNum)
		 );
		 
	}
	@Test
	public void testDefaultRepMsgTypeNull() {
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgId,null,msgSender,msgReceiver,msgContent,msgNum);
			String msgRep = msg.getDefaultRep();
	  		 assertTrue("testDefaultRepMsgTypeNull", msgRep.equals("msg(none,none,none,none,none,0)")
			 );		
		} catch (Exception e) {
			fail("testDefaultRepMsgTypeNull");
 		}
	}
	@Test
	public void testContentNormalString() {
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgId,msgType,msgSender,msgReceiver,"hello world",msgNum);
			 String msgRep    = msg.getDefaultRep();
			 System.out.println("		*** testContentNormalString msgRep=" + msgRep);
			 Struct msgTerm   = (Struct) Term.createTerm(msgRep);
			 System.out.println("		*** " + msgTerm);
			 assertTrue("testContentNormalString", 
					 msg.msgContent().equals("hello world") 
					 &&  msgTerm.getArg(4).toString().equals("'hello world'")
	 		);
		} catch (Exception e) {
			fail("testContentNormalString");
		}
 	}
	@Test
	public void testContentQuotedString() {
		IActorMessage msg;
		try {
			msg = new QActorMessage(msgId,msgType,msgSender,msgReceiver,"'hello world'",msgNum);
			 String msgRep    = msg.getDefaultRep();
			 System.out.println("		*** testContentQuotedString msgRep=" + msgRep);
			 Struct msgTerm   = (Struct) Term.createTerm(msgRep);
			 System.out.println("		*** " + msgTerm);
			 assertTrue("testContentQuotedString", 
					 msg.msgContent().equals("'hello world'") 
					 &&  msgTerm.getArg(4).toString().equals("'hello world'")
	 		);
		} catch (Exception e) {
			fail("testContentQuotedString");
		}
 	}

}
