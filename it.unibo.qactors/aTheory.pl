/* =========================================================== 
aTheory.pl in project it.unibo.qactors
=============================================================== */
 data(sonar, 1, 10).
 data(sonar, 2, 20).
 data(sonar, 3, 30).
 data(sonar, 4, 40).
 
 validDistance( N,V ) :-  data(sonar, N, V), V>10, V<50.
 nearDistance(  N,X ) :-  validDistance( N,X ), X < 40.
 nears( D ) :-  findall( d( N,V ), nearDistance(N,V), D).

%% likes(jane,X).
%% character(ulysses, Z, king(ithaca, achaean)).
%% f(g(X, h(X, b)), Z).

initialize  :-   actorPrintln("initializing the aTheory ...").
:- initialization(initialize).


