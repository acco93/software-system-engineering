/*
===============================================================
exampleTheory.pl  
===============================================================
*/
 /*
 ----------------------------------
Show system configuration
 ----------------------------------
 */
showSystemConfiguration :-
	actorPrintln('	The system is composed of the following contexts'),
	getTheContexts(CTXS),
	showElements(CTXS),
	actorPrintln('	 and  of the following actors'),
	getTheActors(A),
	showElements(A).

showElements([]).
showElements([C|R]):-
	actorPrintln( C ),
	showElements(R).

 /*
 ----------------------------------
Find system configuration
 ----------------------------------
 */	
getTheContexts(CTXS) :-
	findall( context( CTX, HOST, PROTOCOL, PORT ), context( CTX, HOST, PROTOCOL, PORT ), CTXS).
getTheActors(ACTORS) :-
	findall( qactor( A, CTX ), qactor( A, CTX ), ACTORS).

 /*
 ----------------------------------
 Family relationship
 ----------------------------------
 */
 father( abraham, isaac ).
 father( haran,   lot ).
 father( haran,   milcah ).
 father( haran,   yiscah ).
 male(isaac).
 male(lot).
 female(milcah).
 female(yiscah).
  
 son(S,F):-father(F,S),male(S).
 daughter(D,F):- father(F,D),female(D).
 
 sons(SONS,F) :- findall( son( S,F ), son( S,F ), SONS).
 daughters(DS,F) :- findall( d( D,F ), daughter( D,F ), DS).
 
 /*
 ----------------------------------
 Indexed knowledege
 ----------------------------------
 */
 data(sonar, 1, 10).
 data(sonar, 2, 20).
 data(sonar, 3, 30).
 data(sonar, 4, 40).
 data(sonar,length,4).
  
 data(distance, 1, 100).
 data(distance, 2, 200).
 data(distance, 3, 300).
 data(distance,length,3).

 nextdata( Sensor, I , V):- 
 	data(Sensor,length,N),
  	value(I,V),
	inc(I),
 	V =< N.
 /*
 ----------------------------------
 Imperative
 ----------------------------------
 */ 
assign( I,V ):-
	( retract( value(I,_) ),!; true ),
	assert( value( I,V ) ).
inc(N):-
 	value(N,X),
 	Y is X + 1,
 	assign( N,Y ).
 
/*
------------------------------------------------------------
initialize
------------------------------------------------------------
*/
initialize  :-   actorPrintln("initializing the exampleTheory ...").
:- initialization(initialize).