/*
===============================================================
actionDemoTheory.pl
===============================================================
*/
setMaxActionTime(MaxT) :-
	retract( actiontodo( G, _ ) ),
	assert( actiontodo( G, MaxT ) ).

createAppplicationInterface(G,T)  :-
	%% actorPrintln( createAppplicationInterface(G,T) ),
	actorobj(Actor),
	%% actorPrintln( createAppplicationInterface(Actor) ),
	Actor <- createAppplicationInterface(G,T).
 
/*
------------------------------------------------------------
initialize
------------------------------------------------------------
*/
initialize  :-  
	actorobj(Actor),
	actorPrintln( loaded(actionDemoTheory, Actor) ).
 
:- initialization(initialize).