%====================================================================================
% Context ctx  SYSTEM-configuration: file it.unibo.ctx.testqa.pl 
%====================================================================================
context(ctx, "localhost",  "TCP", "9990" ).  		 
%%% -------------------------------------------
qactor( a , ctx, "it.unibo.a.MsgHandle_A"   ). %%store msgs 
qactor( a_ctrl , ctx, "it.unibo.a.A"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,ctx,"it.unibo.ctx.Evh","").  
%%% -------------------------------------------

