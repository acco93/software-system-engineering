/* Generated by AN DISI Unibo */ 
/*
This code is generated only ONCE
*/
package it.unibo.actor;
import javax.swing.JFrame;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Actor extends AbstractActor { 
	public Actor(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
		super(actorId, myCtx, outEnvView);
	}
	
	public void createUI(){
	//	System.out.println("qua");
		JFrame f = new JFrame();
		f.setSize(100, 100);
		f.setVisible(true);
	}
	
	public int halve(int value){
		return value/2;
	}
	
	public void javaDelay(int time){
		System.out.println("time: "+time);
		this.delay(time);
	}
}
