package it.unibo.firstDeliverable.step1.pc;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.bls.lowLevel.interfaces.IButton;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.firstDeliverable.step1.BCLS;
import it.unibo.is.interfaces.IBasicEnvAwt;

public class TestPCSystem {

	@Test
	public void testSystem(){
		try {
			IBasicEnvAwt env = new EnvFrame("BCLS");
			env.init();
			BCLS sys = BCLS.createTheSystem();
			IButton concreteButton = sys.getConcreteButton();
			ILed    concreteLed    = sys.getConcreteLed();
			
			/*
			 * Start the blinking action
			 */
			concreteButton.high();
			/*
			 * The led is off when the blinking action is completed
			 */
			assertTrue("testSystem",!concreteLed.isOn());

		} catch (Exception e) {
			fail("");
  		}
	}
}
