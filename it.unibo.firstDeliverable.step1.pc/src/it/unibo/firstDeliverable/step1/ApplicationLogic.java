package it.unibo.firstDeliverable.step1;

import java.util.Observable;

import it.unibo.bls.highLevel.interfaces.IBLSControl;
import it.unibo.bls.highLevel.interfaces.IDevLed;
import it.unibo.is.interfaces.IOutputEnvView;


public class ApplicationLogic implements IBLSControl {

	private IDevLed led;
	private IOutputEnvView out;
	private int time;
	private int TLIMIT;

	public ApplicationLogic(IOutputEnvView outEnvView, int TLIMIT) {
		time = 0;
		this.TLIMIT = TLIMIT;
		this.out = outEnvView;
	}
	
	@Override
	public void setLed(IDevLed led) {
		this.led = led;

	}

	@Override
	public void update(Observable o, Object arg) {
		while(time < TLIMIT){
			if(this.led.isOn()){
				out.addOutput("Turning off the led");
				this.led.turnOff();
			} else {
				out.addOutput("Turning on the led");
				this.led.turnOn();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				
			}
			time++;
		}
		/*
		 * Turn off the led when you'have finished to blink!
		 */
		this.led.turnOff();
		
	}

	

}
