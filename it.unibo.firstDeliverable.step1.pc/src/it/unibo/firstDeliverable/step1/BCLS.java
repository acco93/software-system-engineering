package it.unibo.firstDeliverable.step1;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.bls.highLevel.interfaces.IDevLed.LedColor;
import it.unibo.buttonLedSystem.BLSHLConfig;
import it.unibo.buttonLedSystem.gui.BLSGuiFactory;
import it.unibo.buttonLedSystem.proxy.DeviceLedPassiveProxy;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IOutputEnvView;

public class BCLS extends BLSHLConfig{

	public final static String hostAddress = "192.168.137.2";
	public final static int portNumber = 8055;
	public final static int TLIMIT = 10;
	
	
	public BCLS(IOutputEnvView outEnvView) throws Exception {
		
		super( outEnvView );

		// Button (concrete)
		String[] commands = new String[] { "Blink "+TLIMIT+" seconds" };
		
		this.concreteButton   = BLSGuiFactory.createGuiButton( "button",  outEnvView , commands);
		//Led (concrete)  
		this.concreteLed     = new DeviceLedPassiveProxy("ledGreen", LedColor.GREEN, hostAddress, portNumber, outEnvView);
		
		this.control         = new ApplicationLogic( outEnvView, TLIMIT );
		
		super.createTheLogicElements(concreteButton,concreteLed,control);
		
  	}

	public static BCLS createTheSystem() throws Exception {
		IBasicEnvAwt env = new EnvFrame("Button Control Led System");
		env.init();
		return new BCLS(env.getOutputEnvView());
	} 
	
	public static void main(String[] args) throws Exception {
		
		BCLS.createTheSystem();

	}
	
}
