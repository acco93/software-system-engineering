%====================================================================================
% Context ctxLed  SYSTEM-configuration: file it.unibo.ctxLed.buttonLedBasic.pl 
%====================================================================================
context(ctxcontrol, "192.168.137.1",  "TCP", "8010" ).  		 
context(ctxled, "192.168.137.2",  "TCP", "8015" ).  		 
%%% -------------------------------------------
qactor( control , ctxcontrol, "it.unibo.control.MsgHandle_Control"   ). %%store msgs 
qactor( control_ctrl , ctxcontrol, "it.unibo.control.Control"   ). %%control-driven 
qactor( led , ctxled, "it.unibo.led.MsgHandle_Led"   ). %%store msgs 
qactor( led_ctrl , ctxled, "it.unibo.led.Led"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

