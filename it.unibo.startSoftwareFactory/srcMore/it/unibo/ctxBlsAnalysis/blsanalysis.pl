%====================================================================================
% Context ctxBlsAnalysis  SYSTEM-configuration: file it.unibo.ctxBlsAnalysis.blsAnalysis.pl 
%====================================================================================
context(ctxblsanalysis, "localhost",  "TCP", "8037" ).  		 
%%% -------------------------------------------
qactor( blscontrol , ctxblsanalysis, "it.unibo.blscontrol.MsgHandle_Blscontrol"   ). %%store msgs 
qactor( blscontrol_ctrl , ctxblsanalysis, "it.unibo.blscontrol.Blscontrol"   ). %%control-driven 
qactor( ledcontrol , ctxblsanalysis, "it.unibo.ledcontrol.MsgHandle_Ledcontrol"   ). %%store msgs 
qactor( ledcontrol_ctrl , ctxblsanalysis, "it.unibo.ledcontrol.Ledcontrol"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

