%====================================================================================
% Context pingPongContext  SYSTEM-configuration: file it.unibo.pingPongContext.pingPong.pl 
%====================================================================================
context(pingpongcontext, "localhost",  "TCP", "8887" ).  		 
%%% -------------------------------------------
qactor( referee , pingpongcontext, "it.unibo.referee.MsgHandle_Referee"   ). %%store msgs 
qactor( referee_ctrl , pingpongcontext, "it.unibo.referee.Referee"   ). %%control-driven 
qactor( playera , pingpongcontext, "it.unibo.playera.MsgHandle_Playera"   ). %%store msgs 
qactor( playera_ctrl , pingpongcontext, "it.unibo.playera.Playera"   ). %%control-driven 
qactor( playerb , pingpongcontext, "it.unibo.playerb.MsgHandle_Playerb"   ). %%store msgs 
qactor( playerb_ctrl , pingpongcontext, "it.unibo.playerb.Playerb"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,pingpongcontext,"it.unibo.pingPongContext.Evh","stop").  
%%% -------------------------------------------

