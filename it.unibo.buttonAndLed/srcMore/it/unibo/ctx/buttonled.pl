%====================================================================================
% Context ctx  SYSTEM-configuration: file it.unibo.ctx.buttonLed.pl 
%====================================================================================
context(ctx, "localhost",  "TCP", "8889" ).  		 
%%% -------------------------------------------
qactor( userinterface , ctx, "it.unibo.userinterface.MsgHandle_Userinterface"   ). %%store msgs 
qactor( userinterface_ctrl , ctx, "it.unibo.userinterface.Userinterface"   ). %%control-driven 
qactor( ledcontrol , ctx, "it.unibo.ledcontrol.MsgHandle_Ledcontrol"   ). %%store msgs 
qactor( ledcontrol_ctrl , ctx, "it.unibo.ledcontrol.Ledcontrol"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

