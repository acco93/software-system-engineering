package it.unibo.led;

import java.awt.Color;
import java.awt.Frame;

public class LedMock extends Frame {

	private String name;
	private Color color;
	private boolean state;

	public LedMock(String name, Color color){
		this.name = name;
		this.color = color;
		this.state = false;
		this.setBackground(Color.black);
		this.setSize(100, 100);
		this.setVisible(true);
	}
	
	public void turnOn(){
		this.state = true;
		this.setBackground(color);
		System.out.println(this.name +" on");
	}
	
	public void turnOff(){
		this.state = false;
		this.setBackground(Color.black);
		System.out.println(this.name +" off");
	}
	
	public void toggle(){
		if(!this.state){
			this.turnOn();
		} else {
			this.turnOff();
		}
		
	}

	public boolean isOn() {
		return this.state;
	}
}
