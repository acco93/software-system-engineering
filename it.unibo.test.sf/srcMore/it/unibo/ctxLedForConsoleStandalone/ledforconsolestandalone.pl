%====================================================================================
% Context ctxLedForConsoleStandalone  SYSTEM-configuration: file it.unibo.ctxLedForConsoleStandalone.ledForConsoleStandalone.pl 
%====================================================================================
context(ctxledforconsolestandalone, "localhost",  "TCP", "8011" ).  		 
context(ctxconsolestandalone, "localhost",  "TCP", "8088" ).  		 
%%% -------------------------------------------
qactor( ledforstandalone , ctxledforconsolestandalone, "it.unibo.ledforstandalone.MsgHandle_Ledforstandalone"   ). %%store msgs 
qactor( ledforstandalone_ctrl , ctxledforconsolestandalone, "it.unibo.ledforstandalone.Ledforstandalone"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

