%====================================================================================
% Context ctxpong  SYSTEM-configuration: file it.unibo.ctxpong.exPingpong.pl 
%====================================================================================
context(ctxarbiter, "localhost",  "TCP", "8030" ).  		 
context(ctxping, "localhost",  "TCP", "8010" ).  		 
context(ctxpong, "localhost",  "TCP", "8020" ).  		 
%%% -------------------------------------------
qactor( arbiter , ctxarbiter, "it.unibo.arbiter.MsgHandle_Arbiter"   ). %%store msgs 
qactor( arbiter_ctrl , ctxarbiter, "it.unibo.arbiter.Arbiter"   ). %%control-driven 
qactor( ping , ctxping, "it.unibo.ping.MsgHandle_Ping"   ). %%store msgs 
qactor( ping_ctrl , ctxping, "it.unibo.ping.Ping"   ). %%control-driven 
qactor( pong , ctxpong, "it.unibo.pong.MsgHandle_Pong"   ). %%store msgs 
qactor( pong_ctrl , ctxpong, "it.unibo.pong.Pong"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,ctxpong,"it.unibo.ctxpong.Evh","arbiter,testing").  
%%% -------------------------------------------

