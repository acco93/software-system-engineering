%====================================================================================
% Context ctxLedStdAolone1  SYSTEM-configuration: file it.unibo.ctxLedStdAolone1.ledStdAlone2.pl 
%====================================================================================
context(ctxledstdaolone1, "localhost",  "TCP", "8012" ).  		 
context(ctxconsolestandalone, "localhost",  "TCP", "8088" ).  		 
%%% -------------------------------------------
qactor( ledstdalone2 , ctxledstdaolone1, "it.unibo.ledstdalone2.MsgHandle_Ledstdalone2"   ). %%store msgs 
qactor( ledstdalone2_ctrl , ctxledstdaolone1, "it.unibo.ledstdalone2.Ledstdalone2"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

