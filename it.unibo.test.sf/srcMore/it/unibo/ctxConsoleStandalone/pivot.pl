%====================================================================================
% Context ctxConsoleStandalone  SYSTEM-configuration: file it.unibo.ctxConsoleStandalone.pivot.pl 
%====================================================================================
context(ctxconsolestandalone, "localhost",  "TCP", "8088" ).  		 
%%% -------------------------------------------
qactor( pivot , ctxconsolestandalone, "it.unibo.pivot.MsgHandle_Pivot"   ). %%store msgs 
qactor( pivot_ctrl , ctxconsolestandalone, "it.unibo.pivot.Pivot"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,ctxconsolestandalone,"it.unibo.ctxConsoleStandalone.Evh","alarm").  
%%% -------------------------------------------

