/*
===============================================================
pivotTheory.pl
===============================================================
*/
register( NICKNAME , NAME, alreadyRegistered(NICKNAME) ):-
	retract( registered(NICKNAME,_) ),!.
register( NICKNAME, NAME, doneRegistered(NICKNAME,NAME) ):-
	actorPrintln( registered(NICKNAME,NAME) ),
	assert( registered(NICKNAME,NAME) ). 
 

getactor(NICKNAME, NAME):- 	 	
	registered(NICKNAME,NAME), 
	actorPrintln( getactor(NICKNAME,NAME) ),
	!.
getactor(NICKNAME,null).	

createActor(Name, Class, NewActor):-
	actorPrintln( createActor(Name, Class) ),
	actorobj(A),
	A <- getName returns CurActorName,
 	A <- getContext  returns Ctx,
	A <- getOutputEnvView returns View,
 	%% A <- activateAsNewInstance( Name ), 
	actorPrintln( createActor(CurActorName, Name, Ctx, View) ),
 	java_object(Class, [Name,Ctx,View], NewActor),
 	NewActor <-  getName returns NewActorName,
	actorPrintln( createdNewActor(NewActorName, NewActor) ).
	
/*
------------------------------------------------------------
initialize
------------------------------------------------------------
*/
initialize  :-  
	actorPrintln("pivotTheory started ...") .

:- initialization(initialize).