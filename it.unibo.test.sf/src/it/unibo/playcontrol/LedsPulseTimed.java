package it.unibo.playcontrol;

import java.util.concurrent.Callable;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.action.ActorTimedAction;
import it.unibo.qactors.akka.QActor;

public class LedsPulseTimed extends ActorTimedAction{
protected int dt;
protected ILed[] leds;
protected int indexLedOn ;

	public LedsPulseTimed(String name, QActor actor, QActorContext ctx, boolean cancompensate,
			String terminationEvId, String[] alarms, 
			IOutputEnvView outEnvView, int maxduration, int dt, ILed[] leds) throws Exception {
		super(name, actor, ctx, cancompensate, terminationEvId, alarms, outEnvView, maxduration);
		this.dt   = dt;
		this.leds = leds;
 	}
 	
	@Override
	protected Callable<String> getActionBodyAsCallable() {		
 		return new Callable<String>(){
			@Override
			public String call() throws Exception {
				boolean b = true;
		 		while(true){
		 			for(int i=0; i<leds.length;i++) {
		 				leds[i].turnOn();
		 				b = delay(dt);
		 				indexLedOn = i+1;
		 				if( ! b ) break;
		 				leds[i].turnOff();
		 				b = delay(dt);
 		 				if( ! b ) break;
		 			}
		 			if( ! b ) break;
				}
		 		return ""+indexLedOn;
			}	
 		};
	}

	@Override
	protected String getApplicationResult() {
 		return "actionDone(NAME,TIME)".replace("NAME", name).replace("TIME", ""+durationMillis);
	}

	protected int getIndexLedOn() {
		return indexLedOn;
	}
	protected boolean delay(int dt){
		try {
			Thread.sleep(dt);
			return true;
		} catch (InterruptedException e) {
  			println("LedsPulseTimed delay " + e.getMessage());
 			return false;
		}
	}
}
