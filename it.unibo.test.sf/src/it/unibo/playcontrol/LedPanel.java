package it.unibo.playcontrol;

import java.awt.Panel;
import it.unibo.bls.highLevel.interfaces.IDevLed.LedColor;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.buttonLedSystem.gui.components.DeviceLedGui;
import it.unibo.is.interfaces.IOutputEnvView;

public class LedPanel extends Panel{

	private static final long serialVersionUID = 1L;
	ILed l1,l2,l3,lflash;
	
	public LedPanel(IOutputEnvView outEnvView){
		try{
  			l1      = new DeviceLedGui("l1", outEnvView, this, LedColor.RED);
 			l2      = new DeviceLedGui("l2", outEnvView, this, LedColor.RED);
 			l3      = new DeviceLedGui("l3", outEnvView, this, LedColor.RED);
 			lflash  = new DeviceLedGui("lflash", outEnvView, this, LedColor.GREEN);
  			lflash.turnOff();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
