/* Generated by AN DISI Unibo */ 
/*
This code is generated only ONCE
*/
package it.unibo.ledmorse;
import java.util.Vector;

import it.unibo.bls.highLevel.interfaces.IDevLed.LedColor;
import it.unibo.buttonLed.components.DeviceLedImpl;
import it.unibo.buttonLedSystem.gui.components.DeviceLedGui;
import it.unibo.is.interfaces.IActivity;
import it.unibo.is.interfaces.IBasicEnvAwt;
import it.unibo.is.interfaces.IIntent;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Ledmorse extends AbstractLedmorse implements IActivity{ 
	protected  DeviceLedImpl ledImpl = null;	
	protected  MorseNaiveCompiler compiler;
	protected MorseNaiveInterpreter interpreter;
	protected IBasicEnvAwt myEnv;
	
	public Ledmorse(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
		super(actorId, myCtx, outEnvView);
		myEnv     = outEnvView.getEnv();
		compiler  = new MorseNaiveCompiler(outEnvView,pengine);
	}
  	public void createUserConsole(){
		try{
			myEnv.addInputPanel("SOS                        ");
			String[] cmds = new String[]{"START","STOP"};
 			myEnv.addCmdPanel("btns", cmds, this);
 			ledImpl = new DeviceLedGui("",outEnvView, LedColor.GREEN);  
 		}catch(Exception e){
			e.printStackTrace();
		}
	}
   	public  void evalFromString( String s ) throws Exception{
   		interpreter = MorseNaiveInterpreter.getInterpreter(outEnvView,pengine,ledImpl); //singleton
   		interpreter.evalFromString( s );
   	}
   	public void evalFromMorseString( String s ) throws Exception{
   		println("evalFromMorseString " + s );
   		interpreter = MorseNaiveInterpreter.getInterpreter(outEnvView,pengine,ledImpl); //singleton
   		interpreter.evalFromMorseString( s );
   	}
   	
   	public  Vector<String> compile( String s ) throws Exception{
   			return compiler.compileToList(s); 
   	}
   	public void interpret(  Object  res ) throws Exception{
   		println("interpret " + res.getClass().getName() );
   		interpreter = MorseNaiveInterpreter.getInterpreter(outEnvView,pengine,ledImpl); //singleton
   		interpreter.evalList( (Vector<String>) res );		
   	}
	@Override
	public void execAction(String cmd) {
		if( cmd.equals("START")){
			String str = this.myEnv.readln();
			String event = "usercmd(executeInput('"+str+"'))";
			emit("local_inputcmd", event);
		}else if( cmd.equals("STOP")){
 			String event = "alarm(stop)";
			emit("alarm", event);
			
		} 
	}
	@Override
	public void execAction() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void execAction(IIntent input) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String execActionWithAnswer(String cmd) {
		// TODO Auto-generated method stub
		return null;
	}
}
