package it.unibo.ledmorse;
import java.util.Iterator;
import java.util.Vector;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedPlainObject;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;

public class MorseNaiveInterpreter extends SituatedPlainObject{
	private static MorseNaiveInterpreter myself = null;
	protected Prolog pengine;
	protected  ILed led = null;	
/*
 * SINGLETON	
 */
	
	public static MorseNaiveInterpreter getInterpreter(IOutputEnvView outEnvView, Prolog pengine,ILed led){
		if( myself == null ){
			myself = new MorseNaiveInterpreter(outEnvView,pengine,led);
		}
		return myself;
		
	}
	public MorseNaiveInterpreter(IOutputEnvView outEnvView, Prolog pengine,ILed led){
		super(outEnvView); 
		this.pengine = pengine;
		this.led     = led;
	}


	public void evalFromMorseString(String s ) throws Exception{
 		try{
 			println("MorseNaiveInterpreter evalFromMorseString input   " + s);
 			String input = s.replace("'", "").replace("[", "").replace("]", "");
 			println("MorseNaiveInterpreter input   " + input);
 			String[] items = input.split(",");
 			for( int i=0;i<items.length;i++){
 				String cur = items[i];
 				String goal="timeTurnLed("+cur+",T,ONOFF).";
 				SolveInfo sol = pengine.solve(goal);
 				int time      = Integer.parseInt( ""+sol.getVarValue("T") );
 				boolean onoff = Boolean.parseBoolean(""+sol.getVarValue("ONOFF"));
 				turnLedMorseSym(  cur, time, onoff );							
 			}
			 ;
		}catch( Exception e ){
			println("input error " + s);
		}
	}	

	/*
	 * Calls the compiler and then evalList
	 */
	public void evalFromString(String s ) throws Exception{
		MorseNaiveCompiler compiler  = new MorseNaiveCompiler(outEnvView,pengine);
		try{
			Vector<String> morseList = compiler.compileToList(s);
			evalList(morseList);
		}catch( Exception e ){
			println("input error " + s);
		}
	}	
	public void evalList( Vector<String> morseSymList ) throws Exception{
// 		println("MorseNaiveInterpreter evalList " + morseSymList );
		Iterator<String> it = morseSymList.iterator();
		while( it.hasNext() ){
			String cur = it.next();
			println("MorseNaiveInterpreter evalList cur=" + cur );	
			String goal="timeTurnLed("+cur+",T,ONOFF).";
			SolveInfo sol = pengine.solve(goal);
			int time      = Integer.parseInt( ""+sol.getVarValue("T") );
			boolean onoff = Boolean.parseBoolean(""+sol.getVarValue("ONOFF"));
			turnLedMorseSym(  cur, time, onoff );			
		}
	}
	public void turnLedMorseSym( String morseSym, int v, boolean b){
//   		println("MorseNaiveInterpreter turnLedMorseSym " + morseSym + " " + v);
		try {
			if( b ) led.turnOn(); 
			else led.turnOff();
			Thread.sleep(v);;
		} catch (Exception e) {
 		}
	}
	
}
