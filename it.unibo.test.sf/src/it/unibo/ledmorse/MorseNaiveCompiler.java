package it.unibo.ledmorse;

import java.util.List;
import java.util.Vector;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedPlainObject;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;

public class MorseNaiveCompiler extends SituatedPlainObject{
protected Prolog pengine;

	public MorseNaiveCompiler( IOutputEnvView outEnvView, Prolog pengine){
		super(outEnvView);
		this.pengine = pengine;
	}
 
	public  String compile( String s ) throws Exception{
 		String res="";
  		println("MorseNaiveCompiler compile " + s  );
 		String goal="map(gapword,V).";
		SolveInfo sol = pengine.solve(goal);
		char[] sca = s.toCharArray();
		for( int i=0; i<sca.length; i ++ ){
 			println("" + sca[i] );
			goal="map('"+sca[i]+"',V).";
			sol = pengine.solve(goal);
			println("MorseNaiveCompiler compile sol " + sol  );
 			if( sol.isSuccess() ){
				res = res + sol.getVarValue("V"); //res=[ _ , _ , _ ...]
				res = res.replace("[", "").replace("]", "");
				if( i<sca.length - 1 ) res=res+",";
			}else throw new Exception("input wrong");		
		}
 		println("MorseNaiveCompiler compile res " + res  );
 		return res;
	}
	public   Vector<String> compileToList( String s ) throws Exception{
		Vector<String> out = new Vector<String>();
		char[] sca = s.toCharArray();
		for( int i=0; i<sca.length; i ++ ){
 			println("compileToList: " + sca[i]  );
 			String goal="map('"+sca[i]+"',V).";
			SolveInfo sol = pengine.solve(goal);
// 			println("MorseNaiveCompiler compileToList sol " + sol  );
 			if( sol.isSuccess() ){
 				Term t = sol.getVarValue("V"); //res=[ _ , _ , _ ...]
 				addToList(t,out);
 			}else{
 				println("compileToList: " + sca[i] + " input wrong " );
 				throw new Exception("input wrong");			
 			}
		}
//		println("MorseNaiveCompiler compileToList out " + out.size()  );
		return out;
	}

	public void addToList( Term t, Vector<String> out) throws Exception{
//  		println("MorseNaiveCompiler addToList " + t );
   		if( t.isList() ){
			while( ! t.isEmptyList() ){
				Term th = ((Struct) t).listHead() ;
				out.add(th.toString());
 				t = ((Struct) t).listTail();
			}
 		}				
	}
}
