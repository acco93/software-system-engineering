/*
===============================================================
morseTheory.pl
===============================================================
*/
map("A",[ dot, gap, dash , gap ] ).
map("B",[ dash, gap, dot, gap, dot, gap, dot , gap ] ).
map("C",[ dash, gap, dot, gap, dash, gap, dot , gap ] ).
map("D",[ dash, gap, dot, gap, dot , gap ] ).
map("E",[ dot , gap ] ).
map("F",[ dot, gap, dot, gap, dash, gap, dot , gap ] ).
map("G",[ dash, gap, dash, gap, dot , gap ] ).
map("H",[ dot, gap, dot, gap, dot, gap, dot , gap ] ).
map("I",[ dot, gap, dot , gap ] ).
map("J",[ dot, gap, dash, gap, dash, gap, dash , gap ] ).
map("K",[ dash, gap, dot, gap, dash , gap ] ).
map("L",[ dot, gap, dash, gap, dot, gap, dot , gap ] ).
map("M",[ dash, gap, dash , gap ] ).
map("N",[ dash, gap, dot , gap ] ).
map("O",[ dash, gap, dash, gap, dash , gap ] ).
map("P",[ dot, gap, dash, gap, dash, gap, dot , gap ] ).
map("Q",[ dash, gap, dash, gap, dot, gap, dash , gap ] ).
map("R",[ dot, gap, dash, gap, dot , gap ] ).
map("S",[ dot, gap, dot, gap, dot , gap ] ).
map("T",[ dash , gap ] ).
map("U",[ dot, gap, dot, gap, dash , gap ] ).
map("V",[ dot, gap, dot, gap, dot, gap, dash , gap ] ).
map("W",[ dot, gap, dash, gap, dash , gap ] ).
map("X",[ dash, gap, dot, gap, dot, gap, dash , gap ] ).
map("Y",[ dash, gap, dot, gap, dash, gap, dash , gap ] ).
map("Z",[ dash, gap, dash, gap, dot, gap, dot , gap ] ).
 
map("0",[ dash, gap, dash, gap, dash, gap, dash, gap, dash , gap ] ).
map("1",[ dot, gap, dash, gap, dash, gap, dash, gap, dash , gap ] ).
map("2",[ dot, gap, dot, gap, dash, gap, dash, gap, dash , gap ] ).
map("3",[ dot, gap, dot, gap, dot, gap, dash, gap, dash , gap ] ).
map("4",[ dot, gap, dot, gap, dot, gap, dot, gap, dash , gap ] ).
map("5",[ dot, gap, dot, gap, dot, gap, dot, gap, dot , gap ] ).
map("6",[ dash, gap, dot, gap, dot, gap, dot, gap, dot , gap ] ).
map("7",[ dash, gap, dash, gap, dot, gap, dot, gap, dot , gap ] ).
map("8",[ dash, gap, dash, gap, dash, gap, dot, gap, dot , gap ] ).
map("9",[ dash, gap, dash, gap, dash, gap, dash, gap, dot , gap ] ).

timeTurnLed( dot, 300, true ).
timeTurnLed( gap, 700, false ).
timeTurnLed( dash, 900, true ).

timeTurnLed( gapletter, 300, false ).
timeTurnLed( gapword, 800, false ).

/*
S could be a letter, a word or a sentence

compile( S,L,ok ):-
  	actorobj(A),
 	A <- compile( S ) returns L,
 	!. 
compile( _, _, ko ).
 
interpret( M ):-
  	actorobj(A),
 	A <- interpret( M ). 
*/
	



/*
------------------------------------------------------------
initialize
------------------------------------------------------------
*/
initmorse   :-  
	actorobj(Actor),
	actorPrintln( loaded(morseTheory, Actor) ).
 
:- initialization(initmorse).