%====================================================================================
% Context ctxPlanExample  SYSTEM-configuration: file it.unibo.ctxPlanExample.planexample.pl 
%====================================================================================
context(ctxplanexample, "localhost",  "TCP", "8022" ).  		 
%%% -------------------------------------------
qactor( alarmemitter , ctxplanexample, "it.unibo.alarmemitter.MsgHandle_Alarmemitter"   ). %%store msgs 
qactor( alarmemitter_ctrl , ctxplanexample, "it.unibo.alarmemitter.Alarmemitter"   ). %%control-driven 
qactor( planbehaviour , ctxplanexample, "it.unibo.planbehaviour.MsgHandle_Planbehaviour"   ). %%store msgs 
qactor( planbehaviour_ctrl , ctxplanexample, "it.unibo.planbehaviour.Planbehaviour"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,ctxplanexample,"it.unibo.ctxPlanExample.Evh","alarm,endplay").  
%%% -------------------------------------------

