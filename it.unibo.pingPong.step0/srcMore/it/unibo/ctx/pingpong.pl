%====================================================================================
% Context ctx  SYSTEM-configuration: file it.unibo.ctx.pingPong.pl 
%====================================================================================
context(ctx, "localhost",  "TCP", "8888" ).  		 
%%% -------------------------------------------
qactor( pingactor , ctx, "it.unibo.pingactor.MsgHandle_Pingactor"   ). %%store msgs 
qactor( pingactor_ctrl , ctx, "it.unibo.pingactor.Pingactor"   ). %%control-driven 
qactor( pongactor , ctx, "it.unibo.pongactor.MsgHandle_Pongactor"   ). %%store msgs 
qactor( pongactor_ctrl , ctx, "it.unibo.pongactor.Pongactor"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

