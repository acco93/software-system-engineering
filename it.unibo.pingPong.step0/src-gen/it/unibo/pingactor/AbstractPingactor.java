/* Generated by AN DISI Unibo */ 
package it.unibo.pingactor;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.ActionReceiveTimed;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.akka.QActor;


//REGENERATE AKKA: QActor instead QActorPlanned
public abstract class AbstractPingactor extends QActor { 
	protected AsynchActionResult aar = null;
	protected boolean actionResult = true;
	protected alice.tuprolog.SolveInfo sol;
	//protected IMsgQueue mysupport ;  //defined in QActor
	protected String planFilePath    = null;
	protected String terminationEvId = "default";
	protected String parg="";
	protected boolean bres=false;
	protected IActorAction  action;
	
			protected static IOutputEnvView setTheEnv(IOutputEnvView outEnvView ){
				return outEnvView;
			}
	
	
		public AbstractPingactor(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
			super(actorId, myCtx,  
			"./srcMore/it/unibo/pingactor/WorldTheory.pl",
			setTheEnv( outEnvView )  , "init");		
			this.planFilePath = "./srcMore/it/unibo/pingactor/plans.txt";
			//Plan interpretation is done in Prolog
			//if(planFilePath != null) planUtils.buildPlanTable(planFilePath);
	 	}
		@Override
		protected void doJob() throws Exception {
			String name  = getName().replace("_ctrl", "");
			mysupport = (IMsgQueue) QActorUtils.getQActor( name );
	 		initSensorSystem();
			boolean res = init();
			//println(getName() + " doJob " + res );
		} 
		/* 
		* ------------------------------------------------------------
		* PLANS
		* ------------------------------------------------------------
		*/
	    public boolean init() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "init";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		temporaryStr = " \"Hello from ping Actor\" ";
	    		println( temporaryStr );  
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine,"ping","ping", guardVars ).toString();
	    		sendMsg("ping","pongactor", QActorContext.dispatch, temporaryStr ); 
	    		if( ! planUtils.switchToPlan("play").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=init WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    public boolean play() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "play";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		//OnReceiveMsg 
	    			 aar  = planUtils.receiveMsg(mysupport,
	    			 "MSGID" , "dispatch" , 
	    			 "SENDER" , "RECEIVER" ,
	    			 "PAYLOAD" , "MSgNum", 50000, "" , "");	//could block
	    			 //println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    			 if( aar.getInterrupted() ){
	    			 	curPlanInExec   = "play";
	    			 	if( ! aar.getGoon() ) break;
	    			 } 			
	    			if( ! aar.getGoon() ||  aar.getResult().contains("receive(timeOut(") ){
	    				//System.out.println(" " + getName() + " no goon " + aar.getTimeRemained() + "/" +  50000);
	    				addRule("tout(receive,"+getName()+")");
	    			}
	    		//onMsg
	    		if( currentMessage.msgId().equals("pong") ){
	    			String parg = " \"ping\" ";
	    			parg =  updateVars( Term.createTerm("pong"), Term.createTerm("pong"), 
	    				    		  					Term.createTerm(currentMessage.msgContent()), parg);
	    				if( parg != null ) println( parg );  
	    		}temporaryStr = QActorUtils.unifyMsgContent(pengine,"ping","ping", guardVars ).toString();
	    		sendMsg("ping","pongactor", QActorContext.dispatch, temporaryStr ); 
	    		//delay
	    		aar = delayReactive(500,"" , "");
	    		if( aar.getInterrupted() ) curPlanInExec   = "play";
	    		if( ! aar.getGoon() ) break;
	    		if( planUtils.repeatPlan(0).getGoon() ) continue;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=play WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    protected void initSensorSystem(){
	    	//doing nothing in a QActor
	    }
	    
	 
		/* 
		* ------------------------------------------------------------
		* APPLICATION ACTIONS
		* ------------------------------------------------------------
		*/
		/* 
		* ------------------------------------------------------------
		* QUEUE  
		* ------------------------------------------------------------
		*/
		    protected void getMsgFromInputQueue(){
	//	    	println( " %%%% getMsgFromInputQueue" ); 
		    	QActorMessage msg = mysupport.getMsgFromQueue(); //blocking
	//	    	println( " %%%% getMsgFromInputQueue continues with " + msg );
		    	this.currentMessage = msg;
		    }
	  }
	
