package it.unibo.firstDeliverable.step1.raspberry;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;

import org.junit.Test;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.bls.highLevel.interfaces.IDevLed.LedColor;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.buttonLedSystem.proxy.DeviceLedPassiveProxy;
import it.unibo.firstDeliverable.step1.raspberry.DeviceLedPi4j;
import it.unibo.is.interfaces.IBasicEnvAwt;

public class TestPi4jLed {

	@Test
	public void testLed(){
		try {
		IBasicEnvAwt env = new EnvFrame("Test the proxy led", Color.white, Color.BLACK);
		env.init();
 		ILed led = new DeviceLedPi4j("led", env.getOutputEnvView() , LedColor.GREEN, 25 );
 		assertTrue("testLed", ! led.isOn());
 		led.doSwitch();
   		assertTrue("testLed",led.isOn());
		} catch (Exception e) {
			fail("");
  		}
	}	
	
}
