package it.unibo.firstDeliverable.step1.raspberry;

import java.io.FileInputStream;
import java.util.Properties;

import it.unibo.bls.highLevel.interfaces.IDevLed.LedColor;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.buttonLedSystem.proxy.DeviceLedServer;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedPlainObject;
import it.unibo.system.SituatedSysKb;

 
/*
 * =======================================================================
 *  This class defines a system with a remote Led
 * =======================================================================
 */
public class BLSRaspWithLedServerMain extends SituatedPlainObject{
protected int execTime = 7;

	public BLSRaspWithLedServerMain(IOutputEnvView outEnvView) {
		super(outEnvView);
		System.setProperty("TcpTrace","set");
   		println("================================================================");
  		println("The ButtonLed with the led over RaspberryPi");
  		println("Please start a remote ButtonControl");
  		println("(e.g. it.unibo.buttonLedSystem.proxy.example.BLSRemoteLed)");
  		println("================================================================");
   		try {
			initTheSystem();
		} catch (Exception e) {
 			e.printStackTrace();
		}
   	}	
  	protected void initTheSystem() throws Exception {
		try{
			Properties myProperties = new Properties();
			myProperties.load(new FileInputStream("config.properties"));
			String hostAddr = myProperties.getProperty("remoteHostaddress");
			println("STARTING with remoteHostaddress= " + hostAddr);	
// 	  		this.concreteButton = new DeviceButtonPi4J("btn",  outView ,   24 );
	 		ILed concreteLed    = new DeviceLedPi4j("led", outEnvView , LedColor.GREEN, 25 );
 	 		println("Please, execute it.unibo.buttonLedSystem.proxy.DeviceLedServer on 192.168.43.230");
 	 		DeviceLedServer ledServer = new DeviceLedServer( outEnvView, concreteLed );
 	 		ledServer.activate(SituatedSysKb.executorNumberOfCores);
 		}catch(Exception e){ 
			println("initConcreteDevices ERROR " + e.getMessage() );
		}
     }	
  	
 /*
 * -----------------------------------------
 * MAIN
 * -----------------------------------------
 */
	public static void main(String args[]) throws Exception {
 		new BLSRaspWithLedServerMain( SituatedSysKb.standardOutEnvView );
	}
}
