%====================================================================================
% Context ctxBls  SYSTEM-configuration: file it.unibo.ctxBls.blsAnalysis.pl 
%====================================================================================
context(ctxbls, "localhost",  "TCP", "8037" ).  		 
%%% -------------------------------------------
qactor( blscontrol , ctxbls, "it.unibo.blscontrol.MsgHandle_Blscontrol"   ). %%store msgs 
qactor( blscontrol_ctrl , ctxbls, "it.unibo.blscontrol.Blscontrol"   ). %%control-driven 
qactor( ledcontrol , ctxbls, "it.unibo.ledcontrol.MsgHandle_Ledcontrol"   ). %%store msgs 
qactor( ledcontrol_ctrl , ctxbls, "it.unibo.ledcontrol.Ledcontrol"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

